/****************************************************************************
 Copyright (c) 2012      Wenbin Wang
 
 http://geeksavetheworld.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "WrapperX.h"
#import <FacebookSDK/FacebookSDK.h>
/*#import "Chartboost.h"
#import "GameKitHelper.h"
#import <Parse/Parse.h>
#import "Const.h"*/
#import "Reachability.h"
#include "JacGameCenter.h"
/*#import <Accounts/Accounts.h>*/


// ChartboostDelegateBridge
/*@interface ChartboostDelegateBridge : NSObject<ChartboostDelegate>

@end

static ChartboostDelegateBridge* s_pDelegateBridge = nil;*/

static WrapperX* s_pWrapperX = NULL;

//static GameKitHelper *obj = [[GameKitHelper alloc] init];

WrapperX* WrapperX::sharedWrapperX()
{
    if (s_pWrapperX == NULL)
    {
        s_pWrapperX = new WrapperX();
        /*s_pDelegateBridge = [[ChartboostDelegateBridge alloc] init];
        [[Chartboost sharedChartboost] setDelegate:s_pDelegateBridge];*/
    }
    return s_pWrapperX;
}

void WrapperX::purgeWrapperX()
{
    CC_SAFE_DELETE(s_pWrapperX);
    //[s_pDelegateBridge dealloc];
}

/*
void WrapperX::setAppId(const char * appId)
{
    Chartboost *cb = [Chartboost sharedChartboost];
    cb.appId = [NSString stringWithUTF8String:appId];
}

void WrapperX::setAppSignature(const char * appSignature)
{
    Chartboost *cb = [Chartboost sharedChartboost];
    cb.appSignature = [NSString stringWithUTF8String:appSignature];
}

void WrapperX::startSession()
{
    Chartboost *cb = [Chartboost sharedChartboost];
    [cb startSession];
}

void WrapperX::cacheInterstitial(const char* location)
{
    Chartboost *cb = [Chartboost sharedChartboost];
    if (location) {
        [cb cacheInterstitial : [NSString stringWithUTF8String:location]];
    } else {
        [cb cacheInterstitial];
    }
}

void WrapperX::showInterstitial(const char* location)
{
    Chartboost *cb = [Chartboost sharedChartboost];
    if (location) {
        [cb showInterstitial : [NSString stringWithUTF8String:location]];
    } else {
        [cb showInterstitial];
    }
}

bool WrapperX::hasCachedInterstitial(const char* location)
{
    Chartboost *cb = [Chartboost sharedChartboost];
    if (location) {
        return [cb hasCachedInterstitial : [NSString stringWithUTF8String:location]];
    } else {
        return [cb hasCachedInterstitial];
    }
}

void WrapperX::cacheMoreApps()
{
    Chartboost *cb = [Chartboost sharedChartboost];
    [cb cacheMoreApps];
}

void WrapperX::showMoreApps()
{
    Chartboost *cb = [Chartboost sharedChartboost];
    [cb showMoreApps];
}*/

/*
 * Chartboost Delegate Methods
 *
 */
/*@implementation ChartboostDelegateBridge

- (BOOL)shouldRequestInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->shouldRequestInterstitial([location UTF8String]);
    }

    return YES;
}

- (BOOL)shouldDisplayInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->shouldDisplayInterstitial([location UTF8String]);
    }

    return YES;
}

- (void)didCacheInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didCacheInterstitial([location UTF8String]);
    }
}

- (void)didFailToLoadInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didFailToLoadInterstitial([location UTF8String]);
    }
}

- (void)didDismissInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didDismissInterstitial([location UTF8String]);
    }
}

- (void)didCloseInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didCloseInterstitial([location UTF8String]);
    }
}

- (void)didClickInterstitial:(NSString *)location {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didClickInterstitial([location UTF8String]);
    }
}

- (BOOL)shouldDisplayLoadingViewForMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->shouldDisplayLoadingViewForMoreApps();
    }
    
    return YES;
}

- (BOOL)shouldDisplayMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->shouldDisplayMoreApps();
    }
    
    return YES;
}

- (void)didCacheMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didCacheMoreApps();
    }
}

- (void)didFailToLoadMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didFailToLoadMoreApps();
    }
}

- (void)didDismissMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didDismissMoreApps();
    }
}

- (void)didCloseMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didCloseMoreApps();
    }
}

- (void)didClickMoreApps {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        delegate->didClickMoreApps();
    }
}

- (BOOL)shouldRequestInterstitialsInFirstSession {
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->shouldRequestInterstitialsInFirstSession();
    }
    
    return YES;
}

-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
            return delegate->voteDone();
        }
    }
}

@end*/


/*void WrapperX::checkFbLogin()
{
    FBSession *session = [[FBSession alloc] init];
    
    if (session.state == FBSessionStateCreatedTokenLoaded)
    {
        // even though we had a cached token, we need to login to make the session usable
        [session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
            // we recurse here, in order to update buttons and labels
            //[self updateView];
            if (session.isOpen) {
                // login is integrated with the send button -- so if open, we send
                NSLog(@"Sessione aperta");
                
                [FBSession setActiveSession:session];
                
                [[FBRequest requestForMe]
                 startWithCompletionHandler:
                 ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
                 {
                     // if login fails for any reason, we alert
                     if (error)
                     {
                         NSLog(@"Error: %@", error);
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"attenzione"                                                                                          message:@"autenticazione non riuscita" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                         [alert release];
                         //LoadMatchesScene::get_instance()->goBack();
                         if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                             return delegate->goBack();
                         }
                     }
                     // Did everything come back okay with no errors?
                     //else if (!error && result)
                     else
                     {
                         NSLog(@"CheckFbLogin %@", result.id);
                         const char* cStringId = [result.id UTF8String];
                         const char* cStringName = [result.first_name UTF8String];
                         const char* cStringSurname = [result.last_name UTF8String];
                         const char* cStringEmail = [[result objectForKey:@"email"] UTF8String];
                         const char* cStringGender = [[result objectForKey:@"gender"] UTF8String];
                         const char* cStringLocale = [[result objectForKey:@"locale"] UTF8String];

                         if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                             return delegate->handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale);
                         }
                     }
                 }
                 ];
            }
         }];
    }
    else
    {
        [session closeAndClearTokenInformation];
        
        if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
            //WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate();
            return delegate->handleFb("-1", "", "", "", "", "");
        }

    }
    
    
    
}*/

void WrapperX::fbLogin()
{
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [[FBRequest requestForMe]
         startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
         {
             // if login fails for any reason, we alert
             if (error)
             {
                 NSLog(@"Error: %@", error);
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"attenzione"                                                                                          message:@"autenticazione non riuscita" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
                 [alert release];
                 //LoadMatchesScene::get_instance()->goBack();
                 if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                     return delegate->goBack();
                 }
             }
             // Did everything come back okay with no errors?
             else if (!error && result)
             {
                 NSLog(@"CheckFbLogin %@", result.id);
                 const char* cStringId = [result.id UTF8String];
                 const char* cStringName = [result.first_name UTF8String];
                 const char* cStringSurname = [result.last_name UTF8String];
                 const char* cStringEmail = [[result objectForKey:@"email"] UTF8String];
                 const char* cStringGender = [[result objectForKey:@"gender"] UTF8String];
                 const char* cStringLocale = [[result objectForKey:@"locale"] UTF8String];
                 
                 const char *access_token = [[[FBSession activeSession] accessToken] UTF8String];
                 //LoadMatchesScene::get_instance()->handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale);
                 if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                     //return delegate->handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale, access_token);
                     return JacGameCenter::handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale, access_token);
                 }
             }
         }
         ];
        
    }
    else
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",
                                nil];
        
        // Attempt to open the session. If the session is not open, show the user the Facebook login UX
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            
            // Did something go wrong during login? I.e. did the user cancel?
            
            if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
                
                // If so, just send them round the loop again
                [[FBSession activeSession] closeAndClearTokenInformation];
                [FBSession setActiveSession:nil];
                FBSession* session = [[FBSession alloc] init];
                [FBSession setActiveSession: session];
            }
            else {
                [[FBRequest requestForMe]
                 startWithCompletionHandler:
                 ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
                 {
                     // if login fails for any reason, we alert
                     if (error)
                     {
                         NSLog(@"Error: %@", error);
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"attenzione"                                                                                          message:@"autenticazione non riuscita" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alert show];
                         [alert release];
                         //LoadMatchesScene::get_instance()->goBack();
                         if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                             return delegate->goBack();
                         }
                     }
                     // Did everything come back okay with no errors?
                     else if (!error && result)
                     {
                         NSLog(@"CheckFbLogin %@", result.id);
                         const char* cStringId = [result.id UTF8String];
                         const char* cStringName = [result.first_name UTF8String];
                         const char* cStringSurname = [result.last_name UTF8String];
                         const char* cStringEmail = [[result objectForKey:@"email"] UTF8String];
                         const char* cStringGender = [[result objectForKey:@"gender"] UTF8String];
                         const char* cStringLocale = [[result objectForKey:@"locale"] UTF8String];
                         const char *access_token = [[[FBSession activeSession] accessToken] UTF8String];
                         //LoadMatchesScene::get_instance()->handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale);
                         if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
                            // return delegate->handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale, access_token);
                             return JacGameCenter::handleFb(cStringId, cStringName, cStringSurname, cStringEmail, cStringGender, cStringLocale, access_token);
                         }
                     }
                 }
                 ];
            }
        }];
    }
    
}

void WrapperX::fbResync()
{
    ACAccountStore *accountStore;
    ACAccountType *accountTypeFb;
    
    if((accountStore = [[ACAccountStore alloc] init]) && (accountTypeFb = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook]) )
    {
        NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFb];
        id account;
        if(fbAccounts && [fbAccounts count] > 0 && (account = [fbAccounts objectAtIndex:0]))
        {
            [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
                if(error)
                {
                    
                }
            }];
        }
    }
}

void WrapperX::shareFb()
{
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        // This function will invoke the Feed Dialog to post to a user's Timeline and News Feed
        // It will attemnt to use the Facebook Native Share dialog
        // If that's not supported we'll fall back to the web based dialog.
        NSString *linkURL = @"http://www.lk-lab.com";
        NSString *pictureURL = @"http://www.localnomad.com/en/blog/wp-content/uploads/2013/04/0333rdst-eighthave.jpg";
        
        // Prepare the native share dialog parameters
        FBShareDialogParams *shareParams = [[FBShareDialogParams alloc] init];
        shareParams.link = [NSURL URLWithString:linkURL];
        shareParams.name = @"Adventure Time Rider";
        shareParams.caption= @"caption";
        shareParams.picture= [NSURL URLWithString:pictureURL];
        shareParams.description = @"desc";
        
        if ([FBDialogs canPresentShareDialogWithParams:shareParams]){
            
            [FBDialogs presentShareDialogWithParams:shareParams
                                        clientState:nil
                                            handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                                if(error) {
                                                    NSLog(@"Error publishing story.");
                                                } else if (results[@"completionGesture"] && [results[@"completionGesture"] isEqualToString:@"cancel"]) {
                                                    NSLog(@"User canceled story publishing.");
                                                } else {
                                                    NSLog(@"Story published.");
                                                }
                                            }];
            
        } else {
            
            // Prepare the web dialog parameters
            NSDictionary *params = @{
                                     @"name" : shareParams.name,
                                     @"caption" : shareParams.caption,
                                     @"description" : shareParams.description,
                                     @"picture" : pictureURL,
                                     @"link" : linkURL
                                     };
            
            // Invoke the dialog
            [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                                   parameters:params
                                                      handler:
             ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                 if (error) {
                     NSLog(@"Error publishing story.");
                 } else {
                     if (result == FBWebDialogResultDialogNotCompleted) {
                         NSLog(@"User canceled story publishing.");
                     } else {
                         NSLog(@"Story published.");
                     }
                 }}];
        }
    }
    else
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"email",
                                nil];
        
        // Attempt to open the session. If the session is not open, show the user the Facebook login UX
        [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:true completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            
            // Did something go wrong during login? I.e. did the user cancel?
            
            if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateClosed || status == FBSessionStateCreatedOpening) {
                
                // If so, just send them round the loop again
                [[FBSession activeSession] closeAndClearTokenInformation];
                [FBSession setActiveSession:nil];
                FBSession* session = [[FBSession alloc] init];
                [FBSession setActiveSession: session];
            }
            else {
                shareFb();
            }
        }];
    }
    
}

//void WrapperX::shareFbWin(std::string nome, std::string uid, std::string lang)
void WrapperX::shareFbWin(const char* nome, const char* uid, const char* lang)
{
    if (FBSession.activeSession.isOpen)
    {
        NSArray *permissions = [NSArray arrayWithObject:@"publish_stream"];
        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            // if login fails for any reason, we alert
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                // if otherwise we check to see if the session is open, an alternative to
                // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                // property of the session object; the macros are useful, however, for more
                // detailed state checking for FBSession objects
            } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                
                NSString *desc;
                
                if(!strcmp(lang,"it"))
                    desc = [NSString stringWithFormat:@"me/_zap_app:win_it?profile=%s", uid];
                else
                    desc = [NSString stringWithFormat:@"me/_zap_app:win_en?profile=%s", uid];
                
                FBRequest* newAction = [[FBRequest alloc]initForPostWithSession:[FBSession activeSession] graphPath:desc graphObject:nil];
                
                FBRequestConnection* conn = [[FBRequestConnection alloc] init];
                
                [conn addRequest:newAction completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    if(error) {
                        NSLog(@"Sending OG Story Failed: %@", result[@"id"]);
                        return;
                    }
                    
                    NSLog(@"OG action ID: %@", result[@"id"]);
                }];
                [conn start];
                
            }
        }];
        
    }
}

//void WrapperX::inviteFb(std::string lang)
void WrapperX::inviteFb(const char* lang)
{
    NSString *desc;
    
    if(!strcmp(lang,"it"))
    {
        desc = @"Sfida i tuoi amici in avvincenti partite e dimostra la tua capacita' di riflessi, velocita' e concentrazione!";
    }
    else
    {
        desc = @"challenge your friend in engaging games and show your mental alertness, speed and focus";
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys: nil];
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil message:desc title:@"ZapTap" parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        if(error)
        {
            
        }
    } ];
}
/*
void WrapperX::publishAutomaticFeedLogin(const char* lang)
{
    if (FBSession.activeSession.isOpen)
    {
        NSArray *permissions = [NSArray arrayWithObject:@"publish_stream"];
        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            // if login fails for any reason, we alert
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:error.localizedDescription
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                // if otherwise we check to see if the session is open, an alternative to
                // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                // property of the session object; the macros are useful, however, for more
                // detailed state checking for FBSession objects
            } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                
                NSString *cap;
                NSString *desc;
                
                if(!strcmp(lang,"it"))
                {
                    cap = @"La sfida dei colori";
                    desc = @"Sfida i tuoi amici in avvincenti partite e dimostra la tua capacita' di riflessi, velocita' e concentrazione!";
                }
                else
                {
                    cap = @"The colors challenge";
                    desc = @"challenge your friend in engaging games and show your mental alertness, speed and focus";
                }
                
                
                //NSLog(@"%@", [NSString stringWithFormat:@"%@MY_%@%@.png",FEED_FB_LINK, codeTeam1, codeTeam2]);
                NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               APP_FB_LINK, @"link",
                                               FEED_FB_LINK, @"picture",
                                               @"ZapTap", @"name",
                                               cap, @"caption",
                                               desc, @"description",
                                               nil];
                
                //self.uid = [delegate returnUid];
                
                [FBRequestConnection startWithGraphPath:@"me/feed" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    if(error)
                        NSLog(@"Error %@", error);
                    else
                        NSLog(@"Preeeeeeeso");
                }];
            }
        }];
    }
}


void WrapperX::registerPushNotifications(int id_user)
{
    NSString *str = [NSString stringWithFormat:@"zp_%i", id_user];
    // When users indicate they are Giants fans, we subscribe them to that channel.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation addUniqueObject:str forKey:@"channels"];
    [currentInstallation saveInBackground];
}

void WrapperX::sendPushNotifications(const char* name, int id_user, const char* type, const char* lang)
{
    NSLog(@"Invio notifica");
    NSString *str = [NSString stringWithFormat:@"zp_%i", id_user];
    NSString *message;
    
    //Versus o reminder
    if(!strcmp(type,"versus"))
    {
        if(!strcmp(lang,"it"))
            message = [NSString stringWithFormat:@"%s ti ha appena sfidato!", name];
        else
            message = [NSString stringWithFormat:@"%s has just challenged you!", name];
    }
    else if(!strcmp(type,"reminder"))
    {
        if(!strcmp(lang,"it"))
            message = [NSString stringWithFormat:@"%s ti sta aspettando, è il tuo turno", name];
        else
            message = [NSString stringWithFormat:@"%s is waiting for you", name];
    }
    else if(!strcmp(type, "match"))
    {
        if(!strcmp(lang,"it"))
            message = [NSString stringWithFormat:@"Hai vinto contro %s, complimenti!", name];
        else
            message = [NSString stringWithFormat:@"You won against %s, congratulations!", name];
    }
    
    // Send a notification to all devices subscribed to the "Giants" channel.
    PFPush *push = [[PFPush alloc] init];
    [push setChannel:str];
    [push setMessage:message];
    [push sendPushInBackground];
    
}

//-------------------------------------

void WrapperX::initGameCenter()
{
    NSLog(@"initGameCenter");
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if(![localPlayer isAuthenticated])
    {
        [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
            if(localPlayer.authenticated)
            {
                
            }
            else
            {
                
            }
        }];
    }
}

void WrapperX::showLeaderBoard()
{
    NSLog(@"showLeaderBoard");
    //static GameKitHelper *obj = [[GameKitHelper alloc] init];
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if([localPlayer isAuthenticated])
    {
        [obj showLeaderboard];
    }
    else
    {
        [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
            if(localPlayer.authenticated)
            {
                [obj showLeaderboard];
            }
            else
            {
                
            }
        }];
    }
}

void WrapperX::savePointsLeaderBoard(int points)
{
    //NSLog(@"savePointsLeaderBoard");
    //static GameKitHelper *obj = [[GameKitHelper alloc] init];
    
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    if([localPlayer isAuthenticated])
    {
        [obj submitScore:points category:@"grp.zaptap"];
    }
    else
    {
        [localPlayer authenticateWithCompletionHandler:^(NSError *error) {
            if(localPlayer.authenticated)
            {
                [obj submitScore:points category:@"grp.zaptap"];
            }
            else
            {
                
            }
        }];
    }
    
}

//-----------------------------
void WrapperX::requestVote(const char* lang)
{
    NSString *title;
    NSString *desc;
    NSString *cancel;
    
    if(!strcmp(lang,"it"))
    {
        title = @"Vota ZapTap!";
        desc = @"Se ti piace ZapTap puoi votarlo. I tuoi commenti ci saranno utili per migliorarlo";
        cancel = @"Annulla";
    }
    else
    {
        title = @"Vote ZapTap!";
        desc = @"If you like Zap Tap,  you can vote it. Your comments will be useful in order to improve ZapTap";
        cancel = @"cancel";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:desc
                                                   delegate:s_pDelegateBridge
                                          cancelButtonTitle:cancel
                                          otherButtonTitles:@"OK", nil];
    [alert show];
    [alert release];
}

//-----------------------------
void WrapperX::openUrl(const char *url)
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%s", url]]];
}

//-----------------------------*/
bool WrapperX::checkInternet()
{
    Reachability *r = [Reachability reachabilityWithHostName:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}
void WrapperX::test()
{
    bool param = true;
    if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {
        return delegate->testCallback(param);
    }
}