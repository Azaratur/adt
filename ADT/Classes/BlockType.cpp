//
//  CircleBody.cpp
//  Gumball2
//
//  Created by Johnny Picciafuochi on 06/08/2013.
//
//

#include "BlockType.h"

BlockType::BlockType() {
}

void BlockType::Init(BlockReader::structBlock* bl, CCParallaxNodeExtras* node, float posx, vector<Sprite*> sp, int world, int currentDiff){
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        arrayObstacle.at(i)->Destroy();
    }
    arrayObstacle.clear();
    int diff = 50*currentDiff;
    int d = diff;
    for (int i=0; i<bl->obstacle.size(); i++)
    {
        BlockReader::structObstacle ob = bl->obstacle.at(i);
        ObstacleType* o = new ObstacleType ();
        o->Init(ob, node, posx+d, sp, world);
        d+=diff;
        o->setVisible(true);
        arrayObstacle.push_back(o);
    }
    Hidden = false;
}

void BlockType::Destroy()
{
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        arrayObstacle.at(i)->Destroy();
    }
    arrayObstacle.clear();
}

void BlockType::Pause()
{
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        arrayObstacle.at(i)->Pause();
    }
}

void BlockType::UnPause()
{
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        arrayObstacle.at(i)->UnPause();
    }
}

void BlockType::Hide()
{
    Hidden = true;
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        arrayObstacle.at(i)->setVisible(false);
    }
}

void BlockType::Update(CCPoint s, float radius, Player* ship, bool Update, bool p, bool r, float dt)
{
    if (Hidden || p || r)
        return;
    for (int i=0; i<arrayObstacle.size(); i++)
    {
        if (arrayObstacle.at(i)->Update(s,radius, ship, Update, dt))
            return;
    }
}