//
//  BlockReader.h
//  AdventureTime
//
//  Created by Artificium on 17/09/13.
//
//

#ifndef __AdventureTime__BlockReader__
#define __AdventureTime__BlockReader__

#include "cocos2d.h"

USING_NS_CC;

using namespace std;

class BlockReader
{
public:
    BlockReader();
    ~BlockReader();
    
    void Init(char c [] );
    
    struct structObstacle
    {
        string name;
        int posX;
        int posY;
        int level;
        float rotation;
        bool isMoving;
        bool bonus;

    };
    int blockNumber;
    struct structBlock
    {
        int Number;
        int Width;
        vector<structObstacle> obstacle;
    };
    
    struct structCircle
    {
        int posX;
        int posY;
        int range;
    };

    struct structSpriteCollision
    {
        string Name;
        int Width;
        vector<structCircle> circle;
    };
    
    vector<structBlock*> ContainerBlock;
    vector<structObstacle> arrayBlock;
    
    vector<structSpriteCollision*> ContainerSprite;
    vector<structCircle> arrayCircle;
    
    vector<string> explode( const string &delimiter, const string &str);
    
    int Width;
    string SpriteName;
    
};
#endif /* defined(__Gumball2__CircleBody__) */


