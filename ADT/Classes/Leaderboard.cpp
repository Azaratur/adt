#include "JacGameCenter.h"
#include "Leaderboard.h"
#include "MenuScene.h"

#include "CCSlidingLayer.h"

using namespace cocos2d;

static Leaderboard * myScene = NULL;

CCScene* Leaderboard::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    Leaderboard *layer = Leaderboard::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool Leaderboard::init()
{

    if ( !CCLayer::init() )
    {
        
        return false;
    }
    
    myScene = this;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("leaderboard_texture.plist", "leaderboard_texture.png");
    
    CCSprite *bg = CCSprite::createWithSpriteFrameName("Set_Leaderboard.jpg");
    bg->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(bg, 1);
    
    CCSprite *btnExit = CCSprite::createWithSpriteFrameName("Back.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(btnExit, btnExit, this, menu_selector(Leaderboard::backToMenu));
    backItem->setPosition( ccp(90, 90) );
    menuBack = CCMenu::create(backItem, NULL);
    menuBack->setPosition( CCPointZero );
    this->addChild(menuBack, 2);
    
    bgLoad = CCSprite::create();
    bgLoad->setTextureRect(CCRectMake(0, 0, size.width, size.height));
    bgLoad->setPosition(ccp(size.width/2, size.height/2));
    bgLoad->setColor(ccBLACK);
    bgLoad->setOpacity(180);
    this->addChild(bgLoad, 10);
    Loading = CCSprite::createWithSpriteFrameName("Ico_Loading.png");
    Loading->setPosition(ccp(size.width/2, size.height/2));
    Loading->runAction( CCRepeatForever::create(CCRotateBy::create(0.01f, 3)));
    bgLoad->addChild(Loading, 10);
    
    CCSprite *btnUp = CCSprite::createWithSpriteFrameName("Button_Up.png");
    CCMenuItemSprite *upItem = CCMenuItemSprite::create(btnUp, btnUp, this, menu_selector(Leaderboard::MoveUp));
    upItem->setPosition( ccp(-90, -90) );
    menuUp = CCMenu::create(upItem, NULL);
    menuUp->setAnchorPoint(ccp(1, 0));
    menuUp->setPosition( ccp(size.width, size.height) );
    this->addChild(menuUp, 2);
    menuUp->setVisible(false);
    
    CCSprite *btnDown = CCSprite::createWithSpriteFrameName("Button_DOWN.png");
    CCMenuItemSprite *downItem = CCMenuItemSprite::create(btnDown, btnDown, this, menu_selector(Leaderboard::MoveDown));
    downItem->setPosition( ccp(-90, 90) );
    menuDown = CCMenu::create(downItem, NULL);
    menuDown->setAnchorPoint(ccp(1, 1));
    menuDown->setPosition( ccp(size.width, 0) );
    this->addChild(menuDown, 2);
    menuDown->setVisible(false);
    StartPos = 0;
    
    //TODO mettere bg
    
    CCSprite *btnWorld = CCSprite::createWithSpriteFrameName("Tab_OFF.png");
    CCSprite *btnWorldPress = CCSprite::createWithSpriteFrameName("Tab_ON.png");
    CCLabelBMFont *labelWorld = CCLabelBMFont::create("World", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    CCLabelBMFont *labelWorldPress = CCLabelBMFont::create("World", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    labelWorld->setPosition(ccp(btnWorld->getContentSize().width/2, btnWorld->getContentSize().height/2-11));
    labelWorldPress->setPosition(ccp(btnWorldPress->getContentSize().width/2, btnWorldPress->getContentSize().height/2-11));
    btnWorld->addChild(labelWorld);
    btnWorldPress->addChild(labelWorldPress);
    worldItem = CCMenuItemSprite::create(btnWorldPress, btnWorldPress, btnWorld, this, menu_selector(Leaderboard::WorldScore));
    worldItem->setPosition( ccp(size.width / 2-btnWorld->getContentSize().width/2, size.height-btnWorld->getContentSize().height) );
    
    CCSprite *btnLocale = CCSprite::createWithSpriteFrameName("Tab_OFF.png");
    CCSprite *btnLocalePress = CCSprite::createWithSpriteFrameName("Tab_ON.png");
    CCLabelBMFont *labelLocale = CCLabelBMFont::create("Locale", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    CCLabelBMFont *labelLocalePress = CCLabelBMFont::create("Locale", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    labelLocale->setPosition(ccp(btnWorld->getContentSize().width/2, btnWorld->getContentSize().height/2-11));
    labelLocalePress->setPosition(ccp(btnWorldPress->getContentSize().width/2, btnWorldPress->getContentSize().height/2-11));
    btnLocale->addChild(labelLocale);
    btnLocalePress->addChild(labelLocalePress);
    localeItem = CCMenuItemSprite::create(btnLocalePress, btnLocalePress, btnLocale, this, menu_selector(Leaderboard::WorldScore));
    localeItem->setPosition( ccp(size.width / 2+btnLocale->getContentSize().width/2, size.height-btnLocale->getContentSize().height) );

    pMainMenu = CCMenu::create(worldItem, localeItem, NULL);
    pMainMenu->setPosition( CCPointZero );
    this->addChild(pMainMenu, 2);
    
    WorldScore();
    return true;
}

void Leaderboard::MoveUp(CCObject* pSender)
{

}

void Leaderboard::MoveDown(CCObject* pSender)
{
    
}

void Leaderboard::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void Leaderboard::FriendScore()
{
    if (!WorldSelected)
        return;
    WorldSelected = false;
    bgLoad->setVisible(true);
    worldItem->setEnabled(true);
    localeItem->setEnabled(false);
    CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(Leaderboard::RefreshData), "HTTPGET", NULL);
    JacGameCenter::getGameSession();
}

void Leaderboard::WorldScore()
{
    if (WorldSelected)
        return;
    bgLoad->setVisible(true);
    worldItem->setEnabled(false);
    localeItem->setEnabled(true);
    WorldSelected = true;
    CCNotificationCenter::sharedNotificationCenter()->addObserver(this, callfuncO_selector(Leaderboard::RefreshData), "HTTPGET", NULL);
    JacGameCenter::getGameSession();
}

void Leaderboard::RefreshData()
{
    bgLoad->setVisible(false);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    vector<string> vec = JacGameCenter::GetDoc();
    if (vec.size() == 0)
        return;
    int starty = size.height-localeItem->getContentSize().height-80;
    stringstream ss;
    for (int i=0; i<vec.size(); i+=2)
    {
        ss << (StartPos+(i/2)+1);
        string str = ss.str();
        string s = str+") "+vec.at(i);
        CCLabelBMFont *name = CCLabelBMFont::create(s.c_str(), "shop_font.fnt", 300, kCCTextAlignmentCenter);
        name->setAnchorPoint(ccp(0, 0.5));
        name->setPosition((size.width/2)-(500-137), starty);
        this->addChild(name,4);
        
        CCLabelBMFont *score = CCLabelBMFont::create(vec.at(i+1).c_str(), "shop_font.fnt", 80, kCCTextAlignmentCenter);
        score->setAnchorPoint(ccp(1, 0.5));
        score->setPosition((size.width/2)+(500-137), starty);
        this->addChild(score,4);
        starty-=60;
    }
    if (StartPos>0)
        menuUp->setVisible(true);
    else
        menuUp->setVisible(false);
    if (vec.size() == 20)
        menuDown->setVisible(true);
    else
        menuDown->setVisible(false);
    CCNotificationCenter::sharedNotificationCenter()->removeObserver(this, "HTTPGET");
}

void Leaderboard::onExit()
{
    this->removeAllChildrenWithCleanup(true);
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("achiev_texture.plist");
}

