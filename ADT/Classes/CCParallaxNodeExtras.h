//
//  CCParallaxNodeExtras.h
//  Gumball2
//
//  Created by Johnny Picciafuochi on 25/07/2013.
//
//

#ifndef __Gumball2__CCParallaxNodeExtras__
#define __Gumball2__CCParallaxNodeExtras__

#include "cocos2d.h"

USING_NS_CC;

class CCParallaxNodeExtras : public CCParallaxNode {
    
    public :
    
    CCParallaxNodeExtras();
    
    static CCParallaxNodeExtras * node();
    void incrementOffset(CCPoint offset, CCNode* node);
    void setRealOffset(CCPoint offset, CCNode* node);
    CCPoint getOffset(CCNode* node);
} ;

#endif
