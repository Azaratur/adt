//
//  CreditsScene.cpp
//  AdventureTime
//
//  Created by Artificium on 26/09/13.
//
//

#include "HelpScene.h"
#include "MenuScene.h"
#include "CCSlidingLayer.h"
#include "CCLocalizedString.h"

CCScene* HelpScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelpScene *layer = HelpScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelpScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayerColor::initWithColor(ccc4(0, 0, 120, 255)))
    {
        
        return false;
    }
    // ask director the window size
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    this->setColor(ccWHITE);
    
    //CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("main.plist", "main.png");
    
    // create and initialize a label
    CCLabelTTF* backLabel = CCLabelTTF::create("back", "Thonburi", 48);
    backLabel->setColor(ccBLACK);
    CCMenuItemLabel *backItem = CCMenuItemLabel::create(backLabel, this, menu_selector(HelpScene::backToMenu));
    backItem->setPosition( ccp(backLabel->getDimensions().width+100, size.height-40) );
    
    // create menu, it's an autorelease object
    CCMenu *pMenu = CCMenu::create(backItem, NULL);
    pMenu->setPosition( CCPointZero );
    this->addChild(pMenu, 11);
    
    CCLabelTTF *text = CCLabelTTF::create(CCLocalizedString("t1001", "test"), "Thonburi", 24, CCSizeMake(600, 0), kCCTextAlignmentLeft);
    text->setColor(ccBLACK);
    text->setPosition(ccp(350, 0));
    
    CCLayerColor *layer = CCLayerColor::create();
    layer->setContentSize( CCSizeMake( size.width, 2000));
    layer->setPosition( ccp(0, 0) );
    
    layer->addChild(text, 1);
    
    CCSlidingLayer *sl = CCSlidingLayer::create(Vertically, CCSizeMake(0, layer->getContentSize().height), CCRectMake(0, 0, size.width, layer->getContentSize().height/2), ccc4(0,0,0,0), "match");
    sl->addChildWithSize(layer, layer->getContentSize(), kAlignmentCenter);
    
    this->addChild(sl, 10);
    
    return true;
}

void HelpScene::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void HelpScene::onExit()
{
    this->removeAllChildrenWithCleanup(true);
}