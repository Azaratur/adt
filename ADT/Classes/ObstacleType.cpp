//
//  CircleBody.cpp
//  Gumball2
//
//  Created by Johnny Picciafuochi on 06/08/2013.
//
//

#include "ObstacleType.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"
#include <stdlib.h>

ObstacleType::ObstacleType()
{
    LaserRed = false;
    LaserRot = false;
    MissileLocking = true;
    Initialized = false;
    Splashed = false;
    ToogleMissile = 0;
    Hidden = false;
    SRot = 0;
    Paused = false;
    Collided = false;
    AudioStarted = false;
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    statusAudio = userDefault->getBoolForKey("audio_disable");

}

ObstacleType::~ObstacleType()
{
    sprite->release();
}

void ObstacleType::Init(BlockReader::structObstacle so, CCParallaxNodeExtras* n, float posx, vector<Sprite*> sp, int world)
{
    CCPoint bgSpeed = ccp(0.4, 0.4);
    CCArray *animFrames = CCArray::create();
    node = n;
    char *a=new char[so.name.size()+1];
    a[so.name.size()]=0;
    memcpy(a,so.name.c_str(),so.name.size());
    Sprite* spr;
    string test = so.name;

    if (world == 2 && so.name.compare("ButterflyBlue_Frame1.png") == 0)
        test = "ButterflyRed_Frame1.png";
    else if (world == 2 && so.name.compare("ButterflyYellow_Frame1.png") == 0)
        test = "ButterflyViolet_Frame1.png";
    else if (world == 3 && so.name.compare("ButterflyYellow_Frame1.png") == 0)
        test = "ButterflyGreen_Frame1.png";
    else if (world == 2 && so.name.compare("Laser_Rotate_Frame1.png") == 0)
        test = "Laser_Rotate_World2_Frame1.png";
    else if (world == 3 && so.name.compare("Laser_Rotate_Frame1.png") == 0)
        test = "Laser_Rotate_World3_Frame1.png";
    else if (world == 2 && so.name.compare("Laser_Static_Frame1.png") == 0)
        test = "Laser_Static_World2_Frame1.png";
    else if (world == 3 && so.name.compare("Laser_Static_Frame1.png") == 0)
        test = "Laser_Static_World3_Frame1.png";
    for (int i=0; i<sp.size(); i++)
    {
        if (test.compare(sp.at(i)->Name) == 0)
        {
            spr = sp.at(i);
            break;
        }
    }
    
    sprite = CCSprite::createWithSpriteFrameName(a);
    if (so.name.compare("ButterflyBlue_Frame1.png") ==0 || so.name.compare("ButterflyYellow_Frame1.png") == 0)
    {
        ButterFlyX = so.posX+posx;
        ButterFlyY = 768-so.posY-sprite->getContentSize().height/2;
        type = BUTTERFLY;
        spriteAnim.clear();
        Collision.clear();
        for (int i=0; i<spr->Frame.size(); i++)
        {
            FrameADT f = spr->Frame.at(i);
            char *a=new char[f.Name.size()+1];
            a[f.Name.size()]=0;
            memcpy(a,f.Name.c_str(),f.Name.size());
            CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
            spriteAnim.push_back(sprf);
            animFrames->addObject(sprf);
            Collision.push_back(f.arrayCollision);
        }
            
        for (int i=spriteAnim.size()-2; i>=1; i--)
        {
            CCSpriteFrame* sprf = spriteAnim.at(i);
            spriteAnim.push_back(sprf);
        }
        CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
        CCAnimate *animate = CCAnimate::create(animation);
        HaveActions = true;
        sprite->runAction(CCRepeatForever::create(animate));
    }
    if (so.name.compare("Wood.png") ==0 || so.name.compare("Box.png") ==0 || so.name.compare("BoxGroup.png") ==0 || so.name.compare("Rock_Small.png") ==0 || so.name.compare("Rock_Big.png") ==0)
    {
        type = WOOD;
        spriteAnim.clear();
        Collision.clear();
        FrameADT f = spr->Frame.at(0);
        HaveActions = false;
        Collision.push_back(f.arrayCollision);
    }
    if (so.name.compare("Laser_Rotate_Frame1.png") ==0)
    {
        type = LASER;
        spriteAnim.clear();
        Collision.clear();
        for (int i=0; i<spr->Frame.size(); i++)
        {
            FrameADT f = spr->Frame.at(i);
            char *a=new char[f.Name.size()+1];
            a[f.Name.size()]=0;
            memcpy(a,f.Name.c_str(),f.Name.size());
            CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
            spriteAnim.push_back(sprf);
            animFrames->addObject(sprf);
            Collision.push_back(f.arrayCollision);
        }
        CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
        CCAnimate *animate = CCAnimate::create(animation);
        HaveActions = true;
        sprite->runAction(CCRepeatForever::create(animate));
    }
    if (so.name.compare("FireWolf_Frame1.png") ==0)
    {
        type = FIREWOLF;
        spriteAnim.clear();
        Collision.clear();
        for (int i=0; i<spr->Frame.size(); i++)
        {
            FrameADT f = spr->Frame.at(i);
            char *a=new char[f.Name.size()+1];
            a[f.Name.size()]=0;
            memcpy(a,f.Name.c_str(),f.Name.size());
            CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
            spriteAnim.push_back(sprf);
            animFrames->addObject(sprf);
            Collision.push_back(f.arrayCollision);
        }
        
        for (int i=spriteAnim.size()-2; i>=1; i--)
        {
            CCSpriteFrame* sprf = spriteAnim.at(i);
            spriteAnim.push_back(sprf);
        }
        
        CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.2f);
        CCAnimate *animate = CCAnimate::create(animation);
        HaveActions = true;
        sprite->runAction(CCRepeatForever::create(animate));
        
    }
    if (so.name.compare("Laser_Static_Frame1.png") ==0)
    {
        type = LASER;
        spriteAnim.clear();
        Collision.clear();
        for (int i=0; i<spr->Frame.size(); i++)
        {
            FrameADT f = spr->Frame.at(i);
            char *a=new char[f.Name.size()+1];
            a[f.Name.size()]=0;
            memcpy(a,f.Name.c_str(),f.Name.size());
            CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
            spriteAnim.push_back(sprf);
            animFrames->addObject(sprf);
            Collision.push_back(f.arrayCollision);
        }
        CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
        CCAnimate *animate = CCAnimate::create(animation);
        HaveActions = true;
        sprite->runAction(CCRepeatForever::create(animate));
    }
    if (so.name.compare("Ball.png") ==0)
    {
    	int b = randomValueBetween(0,5);
    	if (b<=1)
    	{
			type = BONUS;
			spriteAnim.clear();
			Collision.clear();
			vector<FrameADT::dimStruct> arrayCollision;
			FrameADT::dimStruct d;
			d.x = 0;//sprite->getContentSize().width/2;
			d.y = 0;//sprite->getContentSize().height/2;
			d.radius = sprite->getContentSize().width/2;
			node->setRealOffset(ccp(so.posX+posx, 768-so.posY-sprite->getContentSize().height/2), sprite);
			arrayCollision.push_back(d);
			Collision.push_back(arrayCollision);
			
			string str;
			string strBig;
            CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
            bool shield = userDefault->getBoolForKey("SHIELD_UNLOCKED");
            bool wings = userDefault->getBoolForKey("WINGS_UNLOCKED");
            int tot = 3;
            if (shield)
                tot++;
            if (wings)
                tot++;
            int t = randomValueBetween(0,tot);
			switch (t)
			{
				case 0:
					str = "OnBall_AllBonus.png";
					strBig = "Stripe_AllBonus.png";
					break;
				case 1:
					str = "OnBall_Atomic.png";
					strBig = "Stripe_Atomic.png";
					break;
				case 2:
					str = "OnBall_Stabilizer.png";
					strBig = "Stripe_Stabilizer.png";
					break;
                case 3:
					str = "OnBall_Shield.png";
					strBig = "Stripe_Shield.png";
					break;
				case 4:
					str = "OnBall_Wings.png";
					strBig = "Stripe_Wings.png";
					break;
			}
			bt = new BonusType(t, strBig);
			char *a=new char[str.size()+1];
			a[str.size()]=0;
			memcpy(a,str.c_str(),str.size());
			spriteSmokeSmall = CCSprite::createWithSpriteFrameName(a);
			str = "UnderBall.png";
			a=new char[str.size()+1];
			a[str.size()]=0;
			memcpy(a,str.c_str(),str.size());
			spriteRot = CCSprite::createWithSpriteFrameName(a);
			CCRotateBy *rotate = CCRotateBy::create(0.8f, 360.0f);
            HaveActions = true;
			spriteRot->runAction(CCRepeatForever::create(rotate));
			node->addChild(spriteSmokeSmall, 1, bgSpeed, ccp(-1500, 0));
			node->addChild(spriteRot, 1, bgSpeed, ccp(-1500, 0));
			node->setRealOffset(ccp(so.posX+posx, 768-so.posY-sprite->getContentSize().height/2), spriteSmokeSmall);
			node->setRealOffset(ccp(so.posX+posx, 768-so.posY-sprite->getContentSize().height/2), spriteRot);
			ButterFlyX = so.posX+posx;
			ButterFlyY = 768-so.posY-sprite->getContentSize().height/2;
    	}
    	else 
    		type = -1;
    }
    if (type == -1)
    {
        HaveActions = false;
        sprite->removeAllChildrenWithCleanup(true);
    }
    else
    {
        sprite->setRotation(so.rotation);
        sprite->setVisible(false);
        SRot = so.rotation;
        node->addChild(sprite, 1, bgSpeed, ccp(-1500, 0));
        Initialized = true;
        LaserRot = so.isMoving;
        startTime = GetMilliCount();
        //node->setRealOffset(ccp(so.posX+posx+sprite->getContentSize().width/2, 640-so.posY), sprite);
        node->setRealOffset(ccp(so.posX+posx, 768-so.posY-sprite->getContentSize().height/2), sprite);
    }
    
}

void ObstacleType::Destroy()
{
	if (type != -1 && type != BONUS)
		MainScene::addEnemyPassed();
    if (type == MISSILE || type == FLYINGENEMY || type == HUGELASER || type == MISSILEUPDOWN || type == FLYINGENEMYONLYDOWN)
    {
        
        sprite->removeAllChildrenWithCleanup(true);
        BatchNode->removeChild(sprite, true);
        if (type == MISSILE || type == MISSILEUPDOWN)
        {
            spriteRot->removeAllChildrenWithCleanup(true);
            spriteSmokeSmall->removeAllChildrenWithCleanup(true);
            spriteSmokeMedium->removeAllChildrenWithCleanup(true);
            spriteSmokeBig->removeAllChildrenWithCleanup(true);
            BatchNode->removeChild(spriteRot, true);
            BatchNode->removeChild(spriteSmokeSmall, true);
            BatchNode->removeChild(spriteSmokeMedium, true);
            BatchNode->removeChild(spriteSmokeBig, true);
        }
    }
    else if (type != -1)
    {
        if (type == LASER)
        {
            if (!statusAudio)
            {
                CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(Audionum);
                AudioStarted = false;
            }
        }
        if (type == BONUS)
        {
            spriteRot->removeAllChildrenWithCleanup(true);
            spriteSmokeSmall->removeAllChildrenWithCleanup(true);
            node->removeChild(spriteRot, true);
            node->removeChild(spriteSmokeSmall, true);
        }
        sprite->removeAllChildrenWithCleanup(true);
        node->removeChild(sprite, true);
    }
    Collision.clear();
}

void ObstacleType::InitFlyingEnemy (char c[], CCSpriteBatchNode* n, vector<Sprite*> sp)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    type = FLYINGENEMY;
    BatchNode = n;
    sprite = CCSprite::createWithSpriteFrameName(c);
    sprAnim = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Splash1_Frame1.png");
    BatchNode->addChild(sprite, 30);
    range = randomValueBetween(winSize.height/10, winSize.height/5);
    MissileY = randomValueBetween(0, winSize.height);
    sprite->setPosition(ccp(winSize.width-sprite->getContentSize().width,MissileY));
    Collision.clear();
    Sprite* spr;
    string str(c);
    for (int i=0; i<sp.size(); i++)
    {
        if (str.compare(sp.at(i)->Name) == 0)
        {
            spr = sp.at(i);
            break;
        }
    }
    spriteAnim.clear();
    CCArray *animFrames = CCArray::create();
    for (int i=0; i<spr->Frame.size(); i++)
    {
        FrameADT f = spr->Frame.at(i);
        char *a=new char[f.Name.size()+1];
        a[f.Name.size()]=0;
        memcpy(a,f.Name.c_str(),f.Name.size());
        CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
        spriteAnim.push_back(sprf);
        animFrames->addObject(sprf);
        Collision.push_back(f.arrayCollision);
    }
    CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
    CCAnimate *animate = CCAnimate::create(animation);
    HaveActions = true;
    sprite->runAction(CCRepeatForever::create(animate));
    Initialized = true;
    startTime = GetMilliCount();
}

void ObstacleType::InitFlyingOnlyDownEnemy (char c[], CCSpriteBatchNode* n, vector<Sprite*> sp)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    type = FLYINGENEMYONLYDOWN;
    BatchNode = n;
    sprite = CCSprite::createWithSpriteFrameName(c);
    sprAnim = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Splash2_Frame1.png");
    BatchNode->addChild(sprite, 30);
    range = randomValueBetween(winSize.height/10, winSize.height/5);
    MissileY = randomValueBetween(0, winSize.height);
    sprite->setPosition(ccp(winSize.width-sprite->getContentSize().width,MissileY));
    Collision.clear();
    Sprite* spr;
    string str(c);
    for (int i=0; i<sp.size(); i++)
    {
        if (str.compare(sp.at(i)->Name) == 0)
        {
            spr = sp.at(i);
            break;
        }
    }
    spriteAnim.clear();
    CCArray *animFrames = CCArray::create();
    for (int i=0; i<spr->Frame.size(); i++)
    {
        FrameADT f = spr->Frame.at(i);
        char *a=new char[f.Name.size()+1];
        a[f.Name.size()]=0;
        memcpy(a,f.Name.c_str(),f.Name.size());
        CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
        spriteAnim.push_back(sprf);
        animFrames->addObject(sprf);
        Collision.push_back(f.arrayCollision);
    }
    CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
    CCAnimate *animate = CCAnimate::create(animation);
    HaveActions = true;
    sprite->runAction(CCRepeatForever::create(animate));
    Initialized = true;
    startTime = GetMilliCount();
}

void ObstacleType::InitMissile(char c [], char a [], char s [], CCSpriteBatchNode* n, bool updown, vector<Sprite*> sp)
{
    BatchNode = n;
    if (!updown)
        type = MISSILE;
    else
        type = MISSILEUPDOWN;
    sprite = CCSprite::createWithSpriteFrameName(c);
    spriteRot = CCSprite::createWithSpriteFrameName(a);
    spriteSmokeSmall = CCSprite::createWithSpriteFrameName(s);
    spriteSmokeMedium = CCSprite::createWithSpriteFrameName(s);
    spriteSmokeBig = CCSprite::createWithSpriteFrameName(s);
    BatchNode->addChild(sprite);
    BatchNode->addChild(spriteRot);
    BatchNode->addChild(spriteSmokeSmall);
    BatchNode->addChild(spriteSmokeMedium);
    BatchNode->addChild(spriteSmokeBig);
    spriteSmokeSmall->setScale(0.5f);
    spriteSmokeBig->setScale(1.5f);
    CCBlink *blink = CCBlink::create(2, 3);
    HaveActions = true;
    spriteRot->runAction(blink);
    sprite->setVisible(false);
    spriteSmokeSmall->setVisible(false);
    spriteSmokeMedium->setVisible(false);
    spriteSmokeBig->setVisible(false);
    spriteRot->setVisible(false);
    Collision.clear();
    Sprite* spr;
    string str(c);
    for (int i=0; i<sp.size(); i++)
    {
        if (str.compare(sp.at(i)->Name) == 0)
        {
            spr = sp.at(i);
            break;
        }
    }
    FrameADT f = spr->Frame.at(0);
    Collision.push_back(f.arrayCollision);
    MissileY = -9999;
    Initialized = true;
    startTime = GetMilliCount();
}

void ObstacleType::InitHugeLaser(char c [], CCSpriteBatchNode* n, float y) {
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    BatchNode = n;
    type = HUGELASER;
    sprite = CCSprite::createWithSpriteFrameName(c);
    spriteRot = CCSprite::createWithSpriteFrameName(c);
    spriteRot->setRotation(180.0f);
    BatchNode->addChild(sprite);
    BatchNode->addChild(spriteRot);
    sprite->setPosition(ccp(0,y));
    centerRadius = 15;
    spriteRot->setPosition(ccp(winSize.width,y));
    Initialized = true;
}

void ObstacleType::setVisible (bool b)
{
	if (type != -1)
		sprite->setVisible(b);
    if (type == MISSILE || type == FLYINGENEMY || type == HUGELASER || type == MISSILEUPDOWN || type == FLYINGENEMYONLYDOWN)
    {
        if (type == MISSILE || type == MISSILEUPDOWN)
        {
            Hidden = true;
            spriteRot->setVisible(b);
            spriteSmokeSmall->setVisible(b);
            spriteSmokeMedium->setVisible(b);
            spriteSmokeBig->setVisible(b);
        }
    }
}

void ObstacleType::Pause()
{
    pauseTime = GetMilliSpan(startTime);
    if (type == MISSILE || type == MISSILEUPDOWN)
    {
        spriteRot->pauseSchedulerAndActions();
        CCLog("pauseTime: %i", pauseTime);
    }
    Paused = true;
    if (HaveActions)
        sprite->pauseSchedulerAndActions();
}

void ObstacleType::UnPause()
{
    startTime = GetMilliCount()-pauseTime;
    if (type == MISSILE || type == MISSILEUPDOWN)
    {
        spriteRot->resumeSchedulerAndActions();
        CCLog("UnPause pauseTime: %i %i", GetMilliCount(), pauseTime);
    }
    Paused = false;
    if (HaveActions)
        sprite->resumeSchedulerAndActions();
}

bool ObstacleType::Update(CCPoint sP, float radius, Player* ship, bool Update, float dt)
{
    if (!Initialized || Paused)
        return false;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    float radiusTop = MAX(sprite->getContentSize().width, sprite->getContentSize().height)/2;
    CCPoint PosTop;
    CCPoint PosBot;

    if (type == HUGELASER)
    {
        PosTop = BatchNode->convertToWorldSpace(sprite->getPosition());
        PosBot = BatchNode->convertToWorldSpace(spriteRot->getPosition());
    }
    else if (type == FLYINGENEMY || type == FLYINGENEMYONLYDOWN || type == MISSILE || type == MISSILEUPDOWN)
    {
        PosTop = BatchNode->convertToWorldSpace(sprite->getPosition());
    }
    else
    {
        PosTop = node->convertToWorldSpace(sprite->getPosition());
    }

    if (type == LASER)
    {
        if (!AudioStarted && PosTop.x<winSize.width && PosTop.x>0)
        {
            if (!statusAudio)
            {
                AudioStarted = true;
                Audionum = CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("laser_fire_lp.mp3", true);
            }
        }
        if (AudioStarted && PosTop.x<0)
        {
            if (!statusAudio)
            {
                CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(Audionum);
                AudioStarted = false;
            }
        }
        float s = sinf(SRot*(M_PI/180.0))*(-1);
        float c = cosf(SRot*(M_PI/180.0))*(-1);
        if (Update)
        {
            sprite->setRotation(SRot);
        }
        vector<FrameADT::dimStruct> array;
        for (int i=0; i<spriteAnim.size(); i++)
        {
            if (sprite->isFrameDisplayed(spriteAnim.at(i)))
            {
                array = Collision.at(i);
                break;
            }
        }
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            if (isIntersectingCircle (sP, ccp(PosTop.x+((d.y*s)-(d.x*c)), PosTop.y+((d.y*c)+(d.x*s))), radius, d.radius) && sprite->isVisible() && !ship->Dying)
            {
                if (!statusAudio)
                {
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("player_bones.mp3");
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(Audionum);
                }
                sprite->setVisible(false);
                ship->Hitted();
            }
        }
        if (LaserRot && Update && !ship->Dying)
            SRot+=166*dt;
    }
    else if (type == BUTTERFLY)
    {
        if (Update)
        {
            float s = sinf(SRot*(M_PI/180.0));
            node->setRealOffset(ccp(ButterFlyX, ButterFlyY+s*20), sprite);
            SRot+=500*dt;
        }
        vector<FrameADT::dimStruct> array;
        for (int i=0; i<spriteAnim.size(); i++)
        {
            if (sprite->isFrameDisplayed(spriteAnim.at(i)))
            {
                array = Collision.at(i);
                break;
            }
        }
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
            {
                if (!statusAudio)
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("player_hurt_1.mp3");
                sprite->setVisible(false);
                ship->Hitted();
            }
        }

    }
    else if (type == WOOD)
    {
        vector<FrameADT::dimStruct> array = Collision.at(0);
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
            {
                if (!statusAudio)
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("player_hurt_1.mp3");
                sprite->setVisible(false);
                ship->Hitted();
            }
        }
    }
    else if (type == FIREWOLF)
    {
        vector<FrameADT::dimStruct> array;
        for (int i=0; i<spriteAnim.size(); i++)
        {
            if (sprite->isFrameDisplayed(spriteAnim.at(i)))
            {
                array = Collision.at(i);
                break;
            }
        }
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
            {
                if (!statusAudio)
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("player_hurt_1.mp3");
                sprite->setVisible(false);
                ship->Hitted();
            }
        }
    }
    else if (type == BONUS)
    {
        if (Update)
        {
            float s = sinf(SRot*(M_PI/180.0));
            float c = cosf(SRot*(M_PI/180.0));
            node->setRealOffset(ccp(ButterFlyX+c*5, ButterFlyY+s*5), spriteRot);
            SRot+=500*dt;
        }
        vector<FrameADT::dimStruct> array = Collision.at(0);
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            bool skipIt = false;
            int count = 0;
            for (BonusType* bonust : ship->spriteBonus)
            {
                switch (bonust->Type)
                {
                    case BonusType::ALLBONUS:
                        skipIt = true;
                        break;
                    case BonusType::ATOMIC:
                    case BonusType::SHIELD:
                    case BonusType::WINGS:
                    case BonusType::STABILIZER:
                        if (bt->Type == BonusType::ALLBONUS)
                            count++;
                        if (bt->Type == bonust->Type)
                            skipIt = true;
                        break;
                }
            }
            if (count == 4 && bt->Type == BonusType::ALLBONUS)
                skipIt = true;

            if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
            {
                sprite->setVisible(false);
                spriteRot->setVisible(false);
                spriteSmokeSmall->setVisible(false);
                if (!skipIt)
                {
                    ship->AddBonus(bt);
                    MainScene::StopAll(true);
                }
                return true;
            }
        }
    }
    else if (type == FLYINGENEMY || type == FLYINGENEMYONLYDOWN)
    {
        float s = sinf(SRot*(M_PI/180.0));
        if (Update)
        {
            if (Collided)
                sprite->setPosition(ship->getPosition());
            else
                sprite->setPosition(ccp(sprite->getPosition().x-FLYINGENEMYSPEED*dt,MissileY+(range*s)));
            SRot+=300*dt;
        }
        
        vector<FrameADT::dimStruct> array;
        for (int i=0; i<spriteAnim.size(); i++)
        {
            if (sprite->isFrameDisplayed(spriteAnim.at(i)))
            {
                array = Collision.at(i);
                break;
            }
        }
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            if (isIntersectingCircle (sP, ccp((PosTop.x-FLYINGENEMYSPEED*dt)+d.x, (MissileY+(range*s))-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying && !Splashed)
            {
                Collided = true;
                sprite->setScale(0.75);
                CCArray *animFrames = CCArray::create();
                animFrames->addObject(sprAnim);
                CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
                CCAnimate *animate = CCAnimate::create(animation);
                sprite->stopAllActions();
                CCDelayTime *delay = CCDelayTime::create(0.1);
                
                
                CCAction *a = CCScaleTo::create(0.1, 1.30f,1.30f);
                CCSpawn *spwn = CCSpawn::create(animate, a, NULL);
                CCFiniteTimeAction* actionDone = CCCallFuncN::create(BatchNode, callfuncN_selector(ObstacleType::cleanAnimation));
                CCAction *s = CCSequence::create(spwn, delay, actionDone, NULL);
                
                sprite->runAction(s);
                Splashed = true;
                if (type == FLYINGENEMY)
                    ship->HittedFromFlyEnemy();
                else
                    ship->HittedFromFlyEnemyOnlyDown();
                if (!statusAudio)
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("fruit_splat_4.mp3");
            }
        }
    }
    else if (type == MISSILE || type == MISSILEUPDOWN)
    {
        if (MissileLocking && Update)
        {
            spriteRot->setVisible(true);
            float sy = ship->getPosition().y;
            if (GetMilliSpan(startTime)<=MISSILETIME)
            {
                if (MissileY == -9999)
                    MissileY = sy;
                else if (abs((int)(MissileY-sy))<8)
                    MissileY = sy;
                else if (MissileY < sy)
                    MissileY += 8;
                else if (MissileY>sy)
                    MissileY -= 8;
            }
            spriteRot->setPosition(ccp(winSize.width-spriteRot->getContentSize().width,MissileY));
            if (GetMilliSpan(startTime)>MISSILETIME+MISSILEWARNING)
            {
                MissileLocking = false;
                AudioStarted = false;
                sprite->setVisible(true);
            }
            else if (GetMilliSpan(startTime)>MISSILETIME && !AudioStarted)
            {
                sprite->stopAllActions();
                if (!statusAudio)
                {
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("missile_warning.mp3");
                    AudioStarted = true;
                }
            }
            sprite->setPosition(ccp(winSize.width+sprite->getContentSize().width,MissileY));
        }
        else if (!MissileLocking)
        {
            //CCLog("NOT MissileLocking %i", type);
            spriteRot->setVisible(false);
            if (!AudioStarted && !statusAudio)
            {
                AudioStarted = true;
                CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("missile_launch.mp3");
            }
            if (type == MISSILEUPDOWN)
            {
                if (Update)
                {
                    float s = sinf(SRot*(M_PI/180.0));
                    sprite->setPosition(ccp(sprite->getPosition().x-MISSILESPEEDUPDOWN*dt,MissileY+(60*s)));
                    spriteSmokeSmall->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) +10 ,MissileY+(60*s)));
                    spriteSmokeMedium->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) +20 ,MissileY+(60*s)));
                    spriteSmokeBig->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) +30 ,MissileY+(60*s)));
                    if (ToogleMissile != -9999)
                        ToogleMissile++;
                    if (ToogleMissile>30)
                        ToogleMissile = 0;
                    if (ToogleMissile < 0 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 10 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(true);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 20 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(true);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 30 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(true);
                    }
                    SRot+=400*dt;
                }
                PosTop = BatchNode->convertToWorldSpace(sprite->getPosition());
                radiusTop = sprite->getContentSize().height/2;
                vector<FrameADT::dimStruct> array = Collision.at(0);
                for (int i=0; i<array.size(); i++)
                {
                    FrameADT::dimStruct d = array.at(i);
                    if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
                    {
                        if (!statusAudio)
                            CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("rocket_explode_1.mp3");
                        sprite->setVisible(false);
                        ship->Hitted();
                        ToogleMissile = -9999;
                    }
                }
            }
            else
            {
                if (Update)
                {
                    sprite->setPosition(ccp(sprite->getPosition().x-MISSILESPEED*dt,MissileY));
                    spriteSmokeSmall->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) + 10 ,MissileY));
                    spriteSmokeMedium->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) + 20 ,MissileY));
                    spriteSmokeBig->setPosition(ccp(sprite->getPosition().x+(sprite->getContentSize().width/2) + 30 ,MissileY));
                    if (ToogleMissile != -9999)
                        ToogleMissile++;
                    if (ToogleMissile>30)
                        ToogleMissile = 0;
                    if (ToogleMissile < 0 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 10 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(true);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 20 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(true);
                        spriteSmokeBig->setVisible(false);
                    }
                    else if (ToogleMissile <= 30 && !Hidden)
                    {
                        spriteSmokeSmall->setVisible(false);
                        spriteSmokeMedium->setVisible(false);
                        spriteSmokeBig->setVisible(true);
                    }
                }
                PosTop = BatchNode->convertToWorldSpace(sprite->getPosition());
                radiusTop = sprite->getContentSize().height/2;
                vector<FrameADT::dimStruct> array = Collision.at(0);
                for (int i=0; i<array.size(); i++)
                {
                    FrameADT::dimStruct d = array.at(i);
                    if (isIntersectingCircle (sP, ccp(PosTop.x+d.x, PosTop.y-d.y), radius, d.radius) && sprite->isVisible() && !ship->Dying)
                    {
                        if (!statusAudio)
                            CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("rocket_explode_1.mp3");
                        sprite->setVisible(false);
                        ship->Hitted();
                        ToogleMissile = -9999;
                    }
                }
            }
        }
    }
    else if (type == HUGELASER)
    {

        if (isIntersectingCircle (sP, PosTop, radius, radiusTop) && sprite->isVisible())
        {
            sprite->setVisible(false);
            spriteRot->setVisible(false);
            ship->Hitted();
        }
        if (isIntersectingCircle (sP, PosBot, radius, radiusTop) && spriteRot->isVisible())
        {
            spriteRot->setVisible(false);
            sprite->setVisible(false);
            ship->Hitted();
        }
        if (isIntersectingRect (sP, radius, ccp(PosTop.x+radiusTop, PosTop.y+centerRadius), ccp (PosBot.x-radiusTop,  PosTop.y-centerRadius)) && spriteRot->isVisible())
        {
            spriteRot->setVisible(false);
            sprite->setVisible(false);
            ship->Hitted();
        }
    }
    return false;
}

void ObstacleType::cleanAnimation(cocos2d::CCSprite *pSender)
{
    pSender->setVisible(false);
}

bool ObstacleType::MissileIsOut ()
{
    return (sprite->getPosition().x<-sprite->getContentSize().width);
}

bool ObstacleType::isIntersectingCircle(CCPoint p1, CCPoint p2, float r1, float r2)
{
    float distance = ((MAX(p2.x, p1.x) - MIN(p2.x, p1.x))*(MAX(p2.x, p1.x) - MIN(p2.x, p1.x))) + (((MAX(p2.y, p1.y) - MIN(p2.y, p1.y))) *((MAX(p2.y, p1.y) - MIN(p2.y, p1.y))));
    return distance<= (r1+r2)*(r1+r2);
}

bool ObstacleType::isIntersectingRect(CCPoint p1, float r1, CCPoint pTL, CCPoint pBR)
{
    CCPoint circleDistance;
    circleDistance.x = abs((int)(p1.x - ((pTL.x+pBR.x)/2)));
    circleDistance.y = abs((int)(p1.y - ((pTL.y+pBR.y)/2)));
    float width = abs((int)(pBR.x-pTL.x));
    float height = abs((int)(pBR.y-pTL.y));
    if (circleDistance.x > (width/2 + r1)) { return false; }
    if (circleDistance.y > (height/2 + r1)) { return false; }
    
    if (circleDistance.x <= (width/2)) { return true; }
    if (circleDistance.y <= (height/2)) { return true; }
    
    float cornerDistance_sq = ((circleDistance.x - width/2)*(circleDistance.x - width/2)) +
    ((circleDistance.y - height/2) * (circleDistance.y - height/2));
    
    return (cornerDistance_sq <= (r1*r1));
}

int ObstacleType::GetMilliCount()
{
    timeb tb;
    ftime( &tb );
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int ObstacleType::randomValueBetween(int low, int high)
{
    srand ( time(NULL) );
    int r = arc4random() % (high-low);
    return r+low;
}

int ObstacleType::GetMilliSpan( int nTimeStart )
{
    int nSpan = GetMilliCount() - nTimeStart;
    if ( nSpan < 0 )
        nSpan += 0x100000 * 1000;
    return nSpan;
}