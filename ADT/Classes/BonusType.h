//
//  BonusType.h
//  ADT
//
//  Created by Johnny Picciafuochi on 07/02/2014.
//
//

#ifndef __ADT__BonusType__
#define __ADT__BonusType__

#include <iostream>

#include "cocos2d.h"
using namespace std;
USING_NS_CC;
class BonusType : public CCObject
{
public:
      enum Bonust{
        ALLBONUS,
        ATOMIC,
        STABILIZER,
        SHIELD,
        WINGS
    };
    bool Removing;
    float dx;
    float dy;
    int Type;
    float force;
    float time;
    string name;
    CCSprite* sprite;
    CCSprite* spr;
    CCSprite* bg;
    int rot;
    BonusType(int i, string name);
    void StartEnterAnim (cocos2d::CCSpriteBatchNode * _batchNode);
    void AddBonus(cocos2d::CCObject *pSender);
    cocos2d::CCSpriteBatchNode * node;
};

#endif /* defined(__ADT__BonusType__) */
