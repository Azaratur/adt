#include "MenuScene.h"
#include "MainScene.h"
#include "StoreScene.h"
#include "Leaderboard.h"
#include "Achievements.h"
#include "OptionsScene.h"
#include "CreditsScene.h"
#include "IntroScene.h"
#include "SimpleAudioEngine.h"
#include "JacGameCenter.h"

#include <cstdio>

#define kAnimationTime 2.0

using namespace cocos2d;

CCScene* MenuScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    MenuScene *layer = MenuScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    this->setKeypadEnabled(true);
    this->setTouchEnabled(true);
    // ask director the window size
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    /*CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("main.plist", "main.png");
     CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("world_texture.plist", "world_texture.png");
     CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("world_texture2.plist", "world_texture2.png");*/
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("fall_bounce_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("bones_bounce_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("missile_launch.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("gem_pickup.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("missile_warning.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("rocket_explode_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("laser_fire_lp.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("player_bones.mp3");
    
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("player_hurt_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("fruit_splat_4.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("run_dirt_left_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("run_dirt_left_2.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("run_dirt_right_1.mp3");
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("run_dirt_right_2.mp3");
    
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("menu_texture.plist", "menu_texture.png");
    bg = CCSprite::createWithSpriteFrameName("Set.jpg");
    
    if (size.width>1024)
    {
        bg->setScale(1.5);
        // bg->setContentSize(ccp(bg->getContentSize().width*1.5, bg->getContentSize().height*1.5));
    }
    bg->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(bg, 1);
    
    
    sprCloud = CCSprite::createWithSpriteFrameName("Cloud.png");
    posx = (size.width/2)-((size.width/2)-sprCloud->getContentSize().width/2);
    sprCloud->setPosition(ccp(posx, size.height-(sprCloud->getContentSize().height/2)));
    this->addChild(sprCloud, 2);
    
    sprCloud2 = CCSprite::createWithSpriteFrameName("Cloud.png");
    sprCloud2->setPosition(ccp(sprCloud->getContentSize().width+posx-1, size.height-(sprCloud->getContentSize().height/2)));
    this->addChild(sprCloud2, 2);
    
    sprCloud3 = CCSprite::createWithSpriteFrameName("Cloud.png");
    sprCloud3->setPosition(ccp((sprCloud->getContentSize().width*2)+posx-1, size.height-(sprCloud->getContentSize().height/2)));
    this->addChild(sprCloud3, 2);
    
    CCSprite *logo = CCSprite::createWithSpriteFrameName("Logo.png");
    logo->setPosition(ccp(size.width/2, size.height-(logo->getContentSize().height/2)-20));
    this->addChild(logo, 3);
    
    WrapperX::sharedWrapperX()->setDelegate(this);
    
    /////////////////////////////
    // 3. add your codes below...
    
    // add a label shows "Hello World"
    
    // create and initialize a label
    CCSprite *btnPlay = CCSprite::createWithSpriteFrameName("Button_PLAY.png");
    CCLabelBMFont *labelPlay = CCLabelBMFont::create("PLAY", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    labelPlay->setPosition(ccp(btnPlay->getContentSize().width/2, btnPlay->getContentSize().height/2-11));
    btnPlay->addChild(labelPlay);
    CCMenuItemSprite *playItem = CCMenuItemSprite::create(btnPlay, btnPlay, this, menu_selector(MenuScene::gotoGame));
    playItem->setPosition( ccp(size.width / 2, (size.height/2)+50) );
    
    CCSprite *btnShop = CCSprite::createWithSpriteFrameName("Button_SHOP.png");
    CCLabelBMFont *labelShop = CCLabelBMFont::create("SHOP", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    labelShop->setPosition(ccp(btnShop->getContentSize().width/2, btnShop->getContentSize().height/2-11));
    btnShop->addChild(labelShop);
    CCMenuItemSprite *shopItem = CCMenuItemSprite::create(btnShop, btnShop, this, menu_selector(MenuScene::gotoStore));
    shopItem->setPosition( ccp(playItem->getPositionX(), playItem->getPositionY()-150) );
    
    CCSprite *btnHelp = CCSprite::createWithSpriteFrameName("Button_HELP.png");
    CCMenuItemSprite *helpItem = CCMenuItemSprite::create(btnHelp, btnHelp, this, menu_selector(MenuScene::gotoHelp));
    helpItem->setPosition( ccp(size.width - btnHelp->getContentSize().width+40, 90) );
    
    CCSprite *btnOptions = CCSprite::createWithSpriteFrameName("Button_OPTIONS.png");
    CCMenuItemSprite *optionsItem = CCMenuItemSprite::create(btnOptions, btnOptions, this, menu_selector(MenuScene::gotoOptions));
    optionsItem->setPosition( ccp(helpItem->getPositionX()-160, 90) );
    
    CCSprite *btnChart = CCSprite::createWithSpriteFrameName("Button_LEADERBOARD.png");
    CCMenuItemSprite *chartItem = CCMenuItemSprite::create(btnChart, btnChart, this, menu_selector(MenuScene::gotoChart));
    chartItem->setPosition( ccp(optionsItem->getPositionX()-160, 90) );
    
    CCSprite *btnAchie = CCSprite::createWithSpriteFrameName("Button_CUPS.png");
    CCMenuItemSprite *AchieItem = CCMenuItemSprite::create(btnAchie, btnAchie, this, menu_selector(MenuScene::gotoAchie));
    AchieItem->setPosition( ccp(chartItem->getPositionX()-160, 90) );
    
    CCSprite *btnExit = CCSprite::createWithSpriteFrameName("Button_EXIT.png");
    CCMenuItemSprite *exitItem = CCMenuItemSprite::create(btnExit, btnExit, this, menu_selector(MenuScene::exitGame));
    exitItem->setPosition( ccp(90, 90) );
    
    pMainMenu = CCMenu::create(playItem, helpItem, optionsItem, shopItem, chartItem, exitItem, AchieItem, NULL);
    pMainMenu->setPosition( CCPointZero );
    this->addChild(pMainMenu, 2);
    
    CCDelayTime *delay = CCDelayTime::create(1.5f);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MenuScene::moveCamera));
    bg->runAction(CCSequence::create(delay, call, NULL));
    posx = ((size.width/2)-sprCloud->getContentSize().width/2);
    sp1 = 0;
    sp2 = 1;
    sp3 = 2;
    CCLabelTTF* notforsale = CCLabelTTF::create("Not for resale", "Thonburi", 32);
    notforsale->setColor(ccBLACK);
    notforsale->setPosition(ccp(100, 700));
    this->addChild(notforsale, 1000);
    this->scheduleUpdate();
    
    /*CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
     bool skipIntro = userDefault->getBoolForKey("skip_intro");
     
     if(!skipIntro)
     {
        this->inputAge();
        CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
        pMainMenu->setEnabled(false);
        userDefault->setBoolForKey("skip_intro", true);
        userDefault->flush();
     }*/
    this->inputAge();
    pMainMenu->setEnabled(false);
    
    
    if (WrapperX::sharedWrapperX()->checkInternet())
    {
        CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
        int idUser = userDefault->getIntegerForKey("id_user", 0);
        string m = userDefault->getStringForKey("METER_MUST_UPLOAD", "");
        if (idUser!= 0 && m.compare("") != 0)
        {
            CCLog("Utente loggato %i", idUser);
            JacGameCenter::saveGameSession(m);
            //this->saveGameSession(m);
        }
    }
    
    

    return true;
}

void MenuScene::inputAge()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    bgAge = CCSprite::create();
    bgAge->setTextureRect(CCRectMake(0, 0, winSize.width, winSize.height));
    bgAge->setPosition(ccp(winSize.width/2, winSize.height/2));
    bgAge->setColor(ccBLACK);
    bgAge->setOpacity(180);
    this->addChild(bgAge, 101);
    
    ageField = CCTextFieldTTF::textFieldWithPlaceHolder("Age", "shop_font.fnt", 140);
    ageField->setPosition(ccp(winSize.width/2, winSize.height/2+200));
    ageField->setColorSpaceHolder(ccc3(0,0,0));
    ageField->setColor(ccc3(0,0,0));
    
    this->addChild(ageField, 102);
    
    CCSprite *btnOk = CCSprite::createWithSpriteFrameName("Button_PLAY.png");
    CCLabelBMFont *labelOk = CCLabelBMFont::create("OK", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    labelOk->setPosition(ccp(btnOk->getContentSize().width/2, btnOk->getContentSize().height/2-11));
    btnOk->addChild(labelOk);
    CCMenuItemSprite *okItem = CCMenuItemSprite::create(btnOk, btnOk, this, menu_selector(MenuScene::closeInputAge));
    okItem->setPosition( ccp(winSize.width / 2, (winSize.height/2)-100) );
    
    pMenuAge = CCMenu::create(okItem, NULL);
    pMenuAge->setPosition( CCPointZero );
    this->addChild(pMenuAge, 103);
    
}

void MenuScene::closeInputAge(cocos2d::CCObject *pSender)
{
    //CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
    this->setTouchEnabled(false);
    this->removeChild(pMenuAge, true);
    this->removeChild(bgAge, true);
    ageField->detachWithIME();
    this->removeChild(ageField, true);
    pMainMenu->setEnabled(true);
    
    CCLog("Valore immesso %s", ageField->getString());
}

/*bool MenuScene::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent){
    //CCLog("ccTouchBegan");
    
    CCPoint touchLocation = pTouch->getLocation();
    CCRect boundingBoxAge = ageField->boundingBox();
    
    if(boundingBoxAge.containsPoint(touchLocation))
    {
        CCLog("Preeeeeso Age");
        ageField->attachWithIME();
    }
    
    return true;
}*/

void MenuScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCSetIterator it = touches->begin();
    CCTouch* touch;
    
    CCPoint pt;
    for( int iTouchCount = 0; iTouchCount < touches->count(); iTouchCount++ )
    {
        touch = (CCTouch*)(*it);
        //pt = touch->getLocationInView();
        pt = touch->getLocation();
        printf( "ccTouchesBegan id:%i %i,%i\n", touch->getID(), (int)pt.x, (int)pt.y );
        CCRect boundingBoxAge = ageField->boundingBox();

        if(boundingBoxAge.containsPoint(pt))
        {
            CCLog("Preeeeeso Age");
            ageField->attachWithIME();
        }
        
        
        it++;
    }
}

void MenuScene::update(float dt)
{
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    sprCloud->setPosition(ccp((((sprCloud->getContentSize().width-1)*sp1)+size.width/2)-posx, size.height-(sprCloud->getContentSize().height/2)));
    sprCloud2->setPosition(ccp((((sprCloud->getContentSize().width-1)*sp2)+size.width/2)-posx, size.height-(sprCloud->getContentSize().height/2)));
    sprCloud3->setPosition(ccp((((sprCloud->getContentSize().width-1)*sp3)+size.width/2)-posx, size.height-(sprCloud->getContentSize().height/2)));
    if (sprCloud->getPosition().x<=-size.width/2)
        sp1+=3;
    if (sprCloud2->getPosition().x<=-size.width/2)
        sp2+=3;
    if (sprCloud3->getPosition().x<=-size.width/2)
        sp3+=3;
    posx+=1;
}

void MenuScene::moveCamera()
{
    CCPoint p;
    
    srand ( time(NULL) );
    int random = rand()%8+1;
    
    int randomOffset = rand()%5+1;
    
    randomOffset*=10;
    
    //CCLog("Random: %i", random);
    switch (random) {
        case 1:
            p = CCPointMake(randomOffset, 0);
            break;
        case 2:
            p = CCPointMake(randomOffset, randomOffset);
            break;
        case 3:
            p = CCPointMake(0, randomOffset);
            break;
        case 4:
            p = CCPointMake(-randomOffset, randomOffset);
            break;
        case 5:
            p = CCPointMake(-randomOffset, 0);
            break;
        case 6:
            p = CCPointMake(-randomOffset, -randomOffset);
            break;
        case 7:
            p = CCPointMake(0, -randomOffset);
            break;
        case 8:
            p = CCPointMake(randomOffset, -randomOffset);
            break;
        default:
            break;
    }
    
    CCMoveBy *move = CCMoveBy::create(2.0, p);
    CCDelayTime *delay = CCDelayTime::create(0.5f);
    CCMoveBy *move2 = CCMoveBy::create(2.0, ccp(-p.x, -p.y));
    CCDelayTime *delay2 = CCDelayTime::create(0.3f);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MenuScene::moveCamera));
    bg->runAction(CCSequence::create(move, delay, move2, delay2, call, NULL));
}

void MenuScene::fadeOutFirstSprite(CCObject *pSender)
{
    //CCSprite *sprTemp = (CCSprite*)pSender;
    CCLog("fadeOutFirstSprite");
    CCFadeOut *fadeOut = CCFadeOut::create(0.5);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MenuScene::fadeInSecondSprite));
    firstSprite->runAction(CCSequence::create(fadeOut, call, NULL));
}

void MenuScene::fadeInSecondSprite(CCObject *pSender)
{
    CCLog("fadeInSecondSprite");
    secondSprite = CCSprite::create();
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    for(int i = 0; i < 4; i++)
        _raw[i] = 255;
    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    secondSprite->initWithTexture(tex, CCRectMake(0, 0, size.width, size.height));
    secondSprite->setPosition(ccp(size.width/2, size.height/2));
    secondSprite->setColor(ccRED);
    this->addChild(secondSprite, 102);
    
    this->removeChild(firstSprite, true);
    
    CCDelayTime *delay = CCDelayTime::create(kAnimationTime);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MenuScene::fadeOutSecondSprite));
    this->runAction(CCSequence::create(delay, call, NULL));
}

void MenuScene::fadeOutSecondSprite(CCObject* pSender)
{
    CCLog("fadeOutSecondSprite");
    CCFadeOut *fadeOut = CCFadeOut::create(0.5);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MenuScene::displayMenu));
    secondSprite->runAction(CCSequence::create(fadeOut, call, NULL));
}

void MenuScene::displayMenu(cocos2d::CCObject *pSender)
{
    CCLog("Displaymenu");
    this->removeChild(secondSprite, true);
    this->removeChild(bgIntro, true);
}

void MenuScene::gotoGame(CCObject* pSender)
{
    CCScene *pScene = MainScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::gotoAchie(CCObject* pSender)
{
    CCScene *pScene = Achievements::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::gotoChart(CCObject* pSender)
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    int idUser = userDefault->getIntegerForKey("id_user");
    if (idUser == 0)
    {
        //   WrapperX::sharedWrapperX()->setDelegate(this);
        WrapperX::sharedWrapperX()->fbLogin();
    }
    else
    {
        CCLog("Utente loggato %i", idUser);
        CCScene *pScene = Leaderboard::scene();
        CCDirector::sharedDirector()->replaceScene(pScene);
    }
    /*CCScene *pScene = MainScene::scene();
     CCDirector::sharedDirector()->replaceScene(pScene);*/
}

void MenuScene::gotoStore(CCObject* pSender)
{
    CCScene *pScene = StoreScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::gotoOptions(CCObject* pSender)
{
    WrapperX::sharedWrapperX()->shareFb();
    //CCScene *pScene = OptionsScene::scene();
    //CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::gotoCredits(CCObject *pSender)
{
    CCScene *pScene = CreditsScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::gotoHelp(CCObject *pSender)
{
    CCScene *pScene = IntroScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void MenuScene::exitGame(CCObject *pSender)
{
    CCDirector::sharedDirector()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MenuScene::onExit()
{
    CCLog("OnExit");
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("menu_texture.plist");
    this->removeAllChildrenWithCleanup(true);
    this->setKeypadEnabled(false);
}

void MenuScene::keyBackClicked()
{
	CCLog("testCallback");
    CCDirector::sharedDirector()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MenuScene::testCallback(bool param)
{
	CCLog("testCallback");
	this->removeAllChildrenWithCleanup(true);
	CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
	CCTextureCache::sharedTextureCache()->removeUnusedTextures();
	CCDirector::sharedDirector()->purgeCachedData();
}
