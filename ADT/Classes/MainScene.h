#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Player.h"
#include "BlockType.h"
#include "CCParallaxNodeExtras.h"
#include "BlockReader.h"
#include "Gems.h"
#include "Sprite.h"
USING_NS_CC;

class MainScene : public cocos2d::CCLayer
{
    
private:
    struct bonusStruct
    {
        int index;
        string desc;
        string sprName;
    };
    
    static const int BONUSMETER1        = 3000;
    static const int BONUSMETER2        = 5000;
    static const int BONUSMETER3        = 10000;
    
    static const int BONUSPASSED1       = 150;
    static const int BONUSPASSED2       = 300;
    static const int BONUSPASSED3       = 600;
    
    static const int BONUSBONUS1		= 5;
    static const int BONUSBONUS2		= 10;
    static const int BONUSBONUS3		= 20;
    
    static const int BONUSGEMS1			= 200;
    static const int BONUSGEMS2			= 600;
    static const int BONUSGEMS3			= 1000;
    
    cocos2d::CCSpriteBatchNode * ItemNode;
    cocos2d::CCSpriteBatchNode * WorldNode;
    
    bool achievements [12];
    
    int currentDiff;
    
    Player *player;
    Gems* gems;
    BlockReader *bl;
    BlockType* Block;
	
    CCParallaxNodeExtras *_backgroundNode;

    CCSprite *BonusBg;
    CCSprite *GameOverBg;
    
    CCSprite *GreenScroll1;
    CCSprite *GreenScroll2;
    CCSprite *YellowScroll1;
    CCSprite *YellowScroll2;
    CCSprite *OrangeScroll1;
    CCSprite *OrangeScroll2;
    CCSprite *Current1;
    CCSprite *Current2;
    CCSprite *ChangeLevel;
    CCSprite *CurrentBg;
    CCSprite *_pivot;
    CCSprite* cards [8];
    CCSprite* bgEnd;

    CCMenu *pCardMenu;
    int nextGems;
    float NextMovLevel;
    bool CardCreated;
    bool MoveLevel;
    bool SkipLevel;
    bool ForcePause;
    bool RemovingAll;
    bool statusAudio;
    int mov;
    int oldMov;
    int AttachNumber;
    int MissileNumber;
    bool FallSound;
    
    int DeathY;
    int ScreenX;
    int AnimationFallY;
    int AnimationFallX;
    bool AnimationUp;

    vector<Sprite*> SpriteList;
    vector<bonusStruct> bonusArray;
    vector<ObstacleType*> arrayObstacle;
//    static const int TIME_NEXT_HUGE_LASER = 12000;
    static const int TIME_NEXT_MISSILE = 20000;
    static const int TIME_BETWEEN_MISSILE = 1000;
    static const int TIME_BETWEEN_ATTACH = 300;
    static const int TIME_NEXT_ATTACH = 15000;
    long LastTimeMissile;
    long LastTimeAttach;
    /*bool HugeLaserActive;
    bool HugeLaserWaiting;
    int LastTimeHugeLaser;*/
    
    void update(float dt);
    void showCards();
    void showBonusDesc(CCObject *pSender);
    float improvingSpeed;
    void selectCard(CCObject *pSender);
    void UpdateBG();
    void UpdatePivot();
    void CreateGem(float xPosition);
    void CreateNormalLaser (float xPosition);
    int randomValueBetween(int low, int high);
    void setInvisible(CCNode * node);
    float getTimeTick();
    bool isIntersectingRect(CCRect r1, CCRect r2);
    bool isIntersectingCircle(CCPoint p1, CCPoint p2, float r1, float r2);
    //void CreateHugeLaser();
    int GetMilliCount();
    int GetMilliSpan( int nTimeStart );
    bool isAlreadyDead;
    
    void gotoMenu(CCObject *pSender);
    void pauseGame(CCObject *pSender);
    void continueGame(CCObject *pSender);
    void Revive ();
    void onExit();
    CCSprite *bgPause;
    CCMenu *pMainPauseMenu;
    CCMenu *pPauseMenu;    
    
    CCLabelBMFont *titleMetersLabel;
    CCLabelBMFont *metersLabel;
    CCLabelBMFont *titleGemsLabel;
    CCLabelBMFont *gemsLabel;
    CCLabelTTF *descBonus;
    
    float meters;
    float changelvl;
    int ChangeNow;
    
    CCSprite *sprBonus;
    void showResults();
    bool animDead;
    void finnDeathAnimFly();
    void fastMoveLevel();
    CCSprite *finnDeath;
    CCSprite *finnDeath1;
    CCSprite *jackDeath;
    CCSprite *jackDeath1;
    bool bouncing;
    CCSprite* jackShadow;
    CCSprite* finnShadow;

    
    void keyBackClicked();
    
    void showResultVisible(CCObject *pSender);
    void CheckCollisionsAndUpdate(float dt);
    void HandleMissileAttachment ();
    void UpdateGemsMEters();
    void MoveBG (float dt);
    void PlayerIsDying();
    bool checkAchievements();
    
public:
    virtual bool init();
    static cocos2d::CCScene* scene();
    static void RemoveAll ();
    void menuCloseCallback(CCObject* pSender);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void draw();
    static void StopAll (bool pause);
    static void addEnemyPassed();
    static void ResumeAll ();
    static void AddNewChild (CCSprite* sp, int pos);
    static void RemoveChild (CCSprite* sp);
    CREATE_FUNC(MainScene);
};

#endif // __HELLOWORLD_SCENE_H__
