//
//  IntroScene.h
//  ADT
//
//  Created by Johnny Picciafuochi on 06/02/2014.
//
//

#ifndef __ADT__IntroScene__
#define __ADT__IntroScene__

#include <iostream>
#include "cocos2d.h"
#include "common.h"
#include "CCLocalizedString.h"

using namespace cocos2d;


USING_NS_CC;

class IntroScene : public cocos2d::CCLayer
{
    CCLabelBMFont *intro1l;
    CCLabelBMFont *intro2l;
    CCLabelBMFont *intro3l;
    CCLabelBMFont *tut1l;
    CCLabelBMFont *tut2l;
    CCLabelBMFont *tut3l;
    CCSprite* Intro1;
    CCSprite* Intro2;
    CCSprite* Intro3;
    CCSprite* Tutorial1;
    CCSprite* Tutorial2;
    CCSprite* Tutorial3;
    
    CCLabelBMFont *card1l;
    CCLabelBMFont *card2l;
    CCLabelBMFont *card3l;
    CCLabelBMFont *card4l;
    CCLabelBMFont *card5l;
    CCLabelBMFont *card6l;
    CCLabelBMFont *card7l;
    CCSprite* Card1;
    CCSprite* Card2;
    CCSprite* Card3;
    CCSprite* Card4;
    CCSprite* Card5;
    CCSprite* Card6;
    CCSprite* Card7;
    
    
    int nTouch;

public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    void onExit();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(IntroScene);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    
};

#endif /* defined(__ADT__IntroScene__) */

