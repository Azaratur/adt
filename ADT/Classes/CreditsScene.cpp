//
//  CreditsScene.cpp
//  AdventureTime
//
//  Created by Artificium on 26/09/13.
//
//

#include "CreditsScene.h"
#include "MenuScene.h"

CCScene* CreditsScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    CreditsScene *layer = CreditsScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool CreditsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        
        return false;
    }
    // ask director the window size
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    //CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("main.plist", "main.png");
    
    // create and initialize a label
    CCLabelTTF* backLabel = CCLabelTTF::create("back", "Thonburi", 48);
    CCMenuItemLabel *backItem = CCMenuItemLabel::create(backLabel, this, menu_selector(CreditsScene::backToMenu));
    backItem->setPosition( ccp(backLabel->getDimensions().width+100, size.height-40) );
    
    // create menu, it's an autorelease object
    CCMenu *pMenu = CCMenu::create(backItem, NULL);
    pMenu->setPosition( CCPointZero );
    this->addChild(pMenu, 1);
    
    return true;
}

void CreditsScene::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void CreditsScene::onExit()
{
    this->removeAllChildrenWithCleanup(true);
}