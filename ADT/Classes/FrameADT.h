//
//  Frame.h
//  ADT
//
//  Created by Johnny Picciafuochi on 26/11/2013.
//
//

#ifndef __ADT__Frame__
#define __ADT__Frame__

#include <iostream>
#include "cocos2d.h"
#include "BlockReader.h"
using namespace std;
USING_NS_CC;
class FrameADT
{
public:
    struct dimStruct
    {
        int x;
        int y;
        int radius;
    };
    int OriginalWidth;
    int ActualWidth;
    vector<dimStruct> arrayCollision;
    string Name;
    FrameADT(BlockReader::structSpriteCollision* s);
};

#endif /* defined(__ADT__Frame__) */
