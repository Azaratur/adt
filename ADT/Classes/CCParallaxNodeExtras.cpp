//
//  CCParallaxNodeExtras.cpp
//  Gumball2
//
//  Created by Johnny Picciafuochi on 25/07/2013.
//
//
#include "CCParallaxNodeExtras.h"

class CCPointObject  : CCObject {
    CC_SYNTHESIZE(CCPoint, m_tRatio, Ratio)
    CC_SYNTHESIZE(CCPoint, m_tOffset, Offset)
    CC_SYNTHESIZE(CCNode *, m_pChild, Child)
};

CCParallaxNodeExtras::CCParallaxNodeExtras() {
    CCParallaxNode();
}

CCParallaxNodeExtras * CCParallaxNodeExtras::node() {
    return new CCParallaxNodeExtras();
}

void CCParallaxNodeExtras::incrementOffset(CCPoint offset,CCNode* node){
    for( unsigned int i = 0; i < m_pParallaxArray->num; i++) {
        CCPointObject *point = (CCPointObject *)m_pParallaxArray->arr[i];
        CCNode * curNode = point->getChild();
        if( curNode->isEqual(node) ) {
            point->setOffset( ccpAdd(point->getOffset(), offset) );
            break;
        }
    }
}

CCPoint CCParallaxNodeExtras::getOffset (CCNode* node)
{
    for( unsigned int i = 0; i < m_pParallaxArray->num; i++) {
        CCPointObject *point = (CCPointObject *)m_pParallaxArray->arr[i];
        CCNode * curNode = point->getChild();
        if( curNode->isEqual(node) ) {
            return point->getOffset();
        }
    }
    return ccp(0,0);
}

void CCParallaxNodeExtras::setRealOffset(CCPoint offset,CCNode* node){
    for( unsigned int i = 0; i < m_pParallaxArray->num; i++) {
        CCPointObject *point = (CCPointObject *)m_pParallaxArray->arr[i];
        CCNode * curNode = point->getChild();
        if( curNode->isEqual(node) ) {
            point->setOffset( offset );
            break;
        }
    }
}