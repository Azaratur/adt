#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include "cocos2d.h"
#include "common.h"
#include "WrapperX.h"

using namespace cocos2d;

class MenuScene : public cocos2d::CCLayer, public WrapperXDelegate
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(MenuScene);
    
    void onExit();
    
    void gotoGame(CCObject* pSender);
    void gotoStore(CCObject* pSender);
    void gotoAchie(CCObject* pSender);
    void gotoOptions(CCObject* pSender);
    void gotoCredits(CCObject* pSender);
    void gotoHelp(CCObject* pSender);
    void gotoChart(CCObject* pSender);
    void exitGame(CCObject* pSender);

    void inputAge();
    void closeInputAge(CCObject* pSender);
    void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    
    
    void moveCamera();
    void restoreCamera();
    
    CCMenu *pMainMenu;
    
    //Sprite and func for intro
    CCSprite *bgIntro;
    CCSprite *firstSprite;
    CCSprite *secondSprite;
    
    CCSprite *bgAge;
    cocos2d::CCTextFieldTTF * ageField;
    CCMenu *pMenuAge;
    
    CCSprite *bg;
    CCSprite *sprCloud;
    CCSprite *sprCloud2;
    CCSprite *sprCloud3;
    float posx;
    int sp1;
    int sp2;
    int sp3;
    
    void fadeOutFirstSprite(CCObject *pSender);
    void fadeInSecondSprite(CCObject *pSender);
    void fadeOutSecondSprite(CCObject* pSender);
    void displayMenu(CCObject* pSender);
    void keyBackClicked();
    void testCallback(bool param);
    void update(float dt);
};

#endif
