//
//  Sprite.cpp
//  ADT
//
//  Created by Johnny Picciafuochi on 26/11/2013.
//
//

#include "Sprite.h"
#include <functional>
#include <algorithm>
using namespace cocos2d;

Sprite::Sprite (string n )
{
    Name = n;
    Frame.clear();
}

bool compareSort( FrameADT f1, FrameADT f2 )
{
    return strcasecmp( f1.Name.c_str(), f2.Name.c_str() ) <= 0;
}

void Sprite::addFrame(FrameADT f)
{
    Frame.push_back(f);
}

void Sprite::Sort ()
{
    sort (Frame.begin(), Frame.end(), compareSort);
}

