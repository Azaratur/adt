//
//  Player.h
//  AdventureTime
//
//  Created by Johnny Picciafuochi on 19/09/2013.
//
//

#ifndef __AdventureTime__Player__
#define __AdventureTime__Player__

#include <iostream>
#include <sys/timeb.h>
#include "cocos2d.h"
#include "Sprite.h"
#include "BonusType.h"

using namespace std;
USING_NS_CC;
class Player : public CCObject
{
public:
	static const int SHIELDCOUNT = 8000;
    static const int WINGCOUNT = 666;
    
    int MAXTTACHED;
    int FALL_TIME;
    int RISE_TIME;
    enum AnimationState { ANIM_FLY, ANIM_UP, ANIM_DOWN};
    struct attachStruct
    {
        int x;
        int y;
        bool attached;
        int rot;
        bool isRemoving;
        float force;
        int dx;
        int dy;
        CCSprite* sp;
    };
    int enemyPassed;
    int bonusTaken;
    cocos2d::CCSpriteBatchNode * node;
    static void ActiveEffect (BonusType* bt);
    void AddBonus(BonusType* bt);
    vector<BonusType*> BonusVec;
    int ActualFeet;
    int FeeTime;
    bool Paused;
    bool statusAudio;
    void Pause();
    void UnPause();
    bool Pressed;
    int Animation;
    int canBeHitted;
    vector<attachStruct*> BlackAttach;
    vector<attachStruct*> PinkAttach;
    Player(Sprite* f, Sprite* u, Sprite* d, cocos2d::CCSpriteBatchNode * _batchNode);
    int attachedEnemy;
    int attachedEnemyOnlyDown;
    int Gems;
    int Distance;
    CCSprite* ship;
    CCSprite* GetSprite ();
    CCSprite* shadow;
    void setPosition(CCPoint p);
    void AddGems (int i);
    void Update(float dt);
    int GetMilliCount();
    int GetMilliSpan( int nTimeStart );
    bool isAccelerating;
    float acceleration;
    CCPoint getPosition();
    void runAction( CCAction* action);
    bool isVisible();
    float Clicktime;
    float Falltime;
    float Risetime;
    int amountOfClick;
    void touchBegan();
    float getTimeTick();
    void touchEnded();
    void Hitted();
    void HittedFromFlyEnemy ();
    void HittedFromFlyEnemyOnlyDown ();
    bool IsUpgraded;
    bool IMMORTAL;
    bool Dying;
    bool Stabilized;
    bool Shielded;
    bool Bombed;
    bool Winged;
    int WingedPerc;
    long StartShiled;
    long StartWing;
    
    void RemoveShield();
    
    void shipOnAir();
    void shipGoesUp();
    void shipGoesDown();
    void Bomb();
    void RemoveExp();
    float randomValueBetween(float low, float high);
    vector<BonusType*>spriteBonus;
    CCSprite* explosion;

    
    vector<vector<FrameADT::dimStruct> > CollisionFly;
    vector<CCSpriteFrame*>spriteAnimFly;
    
    vector<vector<FrameADT::dimStruct> > CollisionDown;
    vector<vector<FrameADT::dimStruct> > CollisionUp;
    vector<CCSpriteFrame*>spriteAnimDown;
    vector<CCSpriteFrame*>spriteAnimUp;
    float sqrt( float a );
};

#endif /* defined(__AdventureTime__Player__) */
