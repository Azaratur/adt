//
//  AdventureTimeAppDelegate.cpp
//  AdventureTime
//
//  Created by Artificium on 07/08/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//

#include "AppDelegate.h"

#include "cocos2d.h"
//#include "MainScene.h"
#include "SimpleAudioEngine.h"
#include "LogoScene.h"

USING_NS_CC;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
}
static cocos2d::CCSize designResolutionSize = cocos2d::CCSizeMake(1024, 768);
bool AppDelegate::applicationDidFinishLaunching()
{
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
    
    CCEGLView *pEGLView = CCEGLView::sharedOpenGLView();
   // pEGLView->setDesignResolutionSize(1024, 768, kResolutionShowAll);
    CCSize frameSize = pEGLView->getFrameSize();
    pEGLView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionFixedHeight);
    //pDirector->setContentScaleFactor(designResolutionSize.height/frameSize.height);
    pDirector->setDisplayStats(false);
    
    pDirector->setAnimationInterval(1.0 / 60);
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    
    pDirector->runWithScene(LogoScene::scene());
    
    CCDirector::sharedDirector()->resume();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect("POL-two-fat-gangsters-short.mp3");
    bool statusAudio = userDefault->getBoolForKey("audio_disable");
    if (!statusAudio)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("POL-two-fat-gangsters-short.mp3", true);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0.5f);
    }
    return true;
}

void AppDelegate::applicationDidEnterBackground()
{
    CCDirector::sharedDirector()->stopAnimation();
    CCDirector::sharedDirector()->pause();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseAllEffects();
}

void AppDelegate::applicationWillEnterForeground()
{
    CCDirector::sharedDirector()->resume();
    CCDirector::sharedDirector()->startAnimation();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeAllEffects();
}
