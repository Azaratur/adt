#ifndef __Gumball2__BlockType__
#define __Gumball2__BlockType__


#include "cocos2d.h"
#include "ObstacleType.h"
#include "Player.h"
#include "BlockReader.h"
#include "Sprite.h"

USING_NS_CC;
class BlockType
{
    public:
    BlockType();
    ~BlockType();

    bool Hidden;
    void Update(CCPoint s, float radius, Player* ship, bool Update, bool p, bool r, float dt);
    void Init(BlockReader::structBlock* bl, CCParallaxNodeExtras* node, float posx, vector<Sprite*> sp, int world, int currentDiff);

    vector<ObstacleType*> arrayObstacle;
    void Pause();
    void UnPause();
    void Hide();
    void Destroy();
};
#endif /* defined(__Gumball2__CircleBody__) */
