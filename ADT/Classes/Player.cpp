//
//  Player.cpp
//  AdventureTime
//
//  Created by Johnny Picciafuochi on 19/09/2013.
//
//

#include "Player.h"
#include "SimpleAudioEngine.h"
#include "MainScene.h"
#include <ctime>
using namespace cocos2d;
static Player* mainP;

Player::Player (Sprite* f, Sprite* u, Sprite* d, cocos2d::CCSpriteBatchNode * _batchNode)
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    statusAudio = userDefault->getBoolForKey("audio_disable");
    mainP = this;
    node = _batchNode;
    enemyPassed = 0;
    bonusTaken = 0;
	MAXTTACHED = 15;
	FALL_TIME = 200;
	RISE_TIME = 200;
    char *a=new char[f->Name.size()+1];
    a[f->Name.size()]=0;
    memcpy(a,f->Name.c_str(),f->Name.size());
    ship =CCSprite::createWithSpriteFrameName(a);
    shadow =CCSprite::createWithSpriteFrameName("Shadow.png");
    Animation = 0;
    Risetime = 0;
    IsUpgraded = false;
    Paused = false;
    Dying = false;
    Stabilized = false;
    Shielded = false;
    Bombed = false;
    Winged = false;
    Gems = 0;
    IMMORTAL = false;
    //ship->setOpacity(60);
    isAccelerating = false;
    acceleration = 0;
    attachedEnemy = 0;
    amountOfClick = 0;
    attachedEnemyOnlyDown = 0;
    Clicktime = 0;
    Falltime = 0;
    canBeHitted = -1;
    
    _batchNode->addChild(shadow, 1);    
    _batchNode->addChild(ship, 1);
    Pressed = false;

    BlackAttach.clear();
    PinkAttach.clear();
    for (int i=0; i<MAXTTACHED; i++)
    {
        attachStruct* b = new attachStruct();
        attachStruct* p = new attachStruct();
        if (i%2==0)
        {
            b->sp=CCSprite::createWithSpriteFrameName("Attach1_Frame1.png");
            p->sp=CCSprite::createWithSpriteFrameName("Attach2_Frame1.png");
        }
        else
        {
            b->sp=CCSprite::createWithSpriteFrameName("Attach1_Frame2.png");
            p->sp=CCSprite::createWithSpriteFrameName("Attach2_Frame2.png");
        }
        b->attached=false;
        p->attached=false;
        b->isRemoving=false;
        p->isRemoving=false;
        b->x = -8888;
        _batchNode->addChild(b->sp, 1);
        _batchNode->addChild(p->sp, 1);
        b->sp->setVisible(false);
        p->sp->setVisible(false);
        BlackAttach.push_back(b);
        PinkAttach.push_back(p);
    }
    
           
    /*for (int i=spriteAnimRun.size()-2; i>=1; i--)
    {
        CCSpriteFrame* sprf = spriteAnimRun.at(i);
        spriteAnimRun.push_back(sprf);
    }*/
    
    CollisionFly.clear();
    for (int i=0; i<f->Frame.size(); i++)
    {
        FrameADT fr = f->Frame.at(i);
        char *a=new char[fr.Name.size()+1];
        a[fr.Name.size()]=0;
        memcpy(a,fr.Name.c_str(),fr.Name.size());
        CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
        spriteAnimFly.push_back(sprf);
        CollisionFly.push_back(fr.arrayCollision);
    }
    
    /*for (int i=spriteAnimFly.size()-2; i>=1; i--)
    {
        CCSpriteFrame* sprf = spriteAnimFly.at(i);
        spriteAnimFly.push_back(sprf);
    }*/
    
    CollisionUp.clear();
    for (int i=0; i<u->Frame.size(); i++)
    {
        FrameADT fr = u->Frame.at(i);
        char *a=new char[fr.Name.size()+1];
        a[fr.Name.size()]=0;
        memcpy(a,fr.Name.c_str(),fr.Name.size());
        CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
        spriteAnimUp.push_back(sprf);
        CollisionUp.push_back(fr.arrayCollision);
    }
    
    CollisionDown.clear();
    for (int i=0; i<d->Frame.size(); i++)
    {
        FrameADT fr = d->Frame.at(i);
        char *a=new char[fr.Name.size()+1];
        a[fr.Name.size()]=0;
        memcpy(a,fr.Name.c_str(),fr.Name.size());
        CCSpriteFrame* sprf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(a);
        spriteAnimDown.push_back(sprf);
        CollisionDown.push_back(fr.arrayCollision);
    }

 }

void Player::AddGems (int i)
{
    Gems+=i;
}

void Player::setPosition(CCPoint p)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    ship->setPosition(p);
    shadow->setPosition(ccp(p.x-100, winSize.height-20));
}

CCSprite* Player::GetSprite ()
{
    return ship;
}

CCPoint Player::getPosition()
{
    return ship->getPosition();
}

void Player::runAction( CCAction* action)
{
    ship->runAction(action);
}

bool Player::isVisible()
{
    return ship->isVisible();
}


void Player::HittedFromFlyEnemy ()
{
    if (Shielded)
        return;
    if (Bombed)
    {
        Bomb();
        return;
    }
    int atc = 0;
    int ran = 0;
    if (attachedEnemyOnlyDown+attachedEnemy < MAXTTACHED)
    {
        if (attachedEnemyOnlyDown+attachedEnemy+5 <= MAXTTACHED)
        {
            attachedEnemy+=5;
            atc = 5;
            ran = randomValueBetween(1,5);
        }
        else
        {
            atc = MAXTTACHED-(attachedEnemyOnlyDown+attachedEnemy);
            attachedEnemy+=atc;
            if (atc>1)
                ran = randomValueBetween(1,atc);
        }
    }
    else
        return;
    for (int i=0; i<MAXTTACHED; i++)
    {
        attachStruct* b = BlackAttach.at(i);
        if (b->attached)
            continue;
        else
        {
            vector<FrameADT::dimStruct> array;
            if (Animation == ANIM_UP)
            {
                for (int i=0; i<spriteAnimUp.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimUp.at(i)))
                    {
                        array = CollisionUp.at(i);
                        break;
                    }
                }
            }
            else if (Animation == ANIM_DOWN)
            {
                for (int i=0; i<spriteAnimDown.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimDown.at(i)))
                    {
                        array = CollisionDown.at(i);
                        break;
                    }
                }
            }
            else
            {
                for (int i=0; i<spriteAnimFly.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimFly.at(i)))
                    {
                        array = CollisionFly.at(i);
                        break;
                    }
                }
            }
            b->attached = true;
            int k = randomValueBetween(0, array.size());
            FrameADT::dimStruct d = array.at(k);
            b->x = d.x;
            b->y = d.y;
            b->sp->setVisible(true); 
            atc--;
            ran--;
            if (ran>=0)
            {
                attachedEnemy--;
                CCRotateBy *rotate = CCRotateBy::create(0.8f, 360.0f);
                b->isRemoving= true;
                b->force = 0;
                b->sp->runAction(CCRepeatForever::create(rotate));
            }
            else
            {
                b->sp->setScale(1.50f);
                CCFiniteTimeAction *a = CCScaleTo::create(0.5, 1.00f,1.00f);
                CCFiniteTimeAction *f = CCFadeIn::create(0.3);
                CCSpawn *spwn = CCSpawn::create(a, f, NULL);
                b->sp->runAction(spwn);
            }
            if (atc == 0)
                break;
        }
    }
}

void Player::HittedFromFlyEnemyOnlyDown ()
{
    if (Shielded)
        return;
    if (Bombed)
    {
        Bomb();
        return;
    }
    
    int atc = 0;
    int ran = 0;
    if (attachedEnemyOnlyDown+attachedEnemy < MAXTTACHED)
    {
        if (attachedEnemyOnlyDown+attachedEnemy+5 <= MAXTTACHED)
        {
            attachedEnemyOnlyDown+=5;
            atc = 5;
            ran = randomValueBetween(1,5);
        }
        else
        {
            atc = MAXTTACHED-(attachedEnemyOnlyDown+attachedEnemy);
            attachedEnemyOnlyDown+=atc;
            if (atc>1)
                ran = randomValueBetween(1,atc);
        }
    }
    else
        return;
    
    for (int i=0; i<MAXTTACHED; i++)
    {
        attachStruct* b = PinkAttach.at(i);
        if (b->attached)
            continue;
        else
        {
            vector<FrameADT::dimStruct> array;
            if (Animation == ANIM_UP)
            {
                for (int i=0; i<spriteAnimUp.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimUp.at(i)))
                    {
                        array = CollisionUp.at(i);
                        break;
                    }
                }
            }
            else if (Animation == ANIM_DOWN)
            {
                for (int i=0; i<spriteAnimDown.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimDown.at(i)))
                    {
                        array = CollisionDown.at(i);
                        break;
                    }
                }
            }
            else
            {
                for (int i=0; i<spriteAnimFly.size(); i++)
                {
                    if (ship->isFrameDisplayed(spriteAnimFly.at(i)))
                    {
                        array = CollisionFly.at(i);
                        break;
                    }
                }
            }
            b->attached = true;
            int k = randomValueBetween(0, array.size());
            FrameADT::dimStruct d = array.at(k);
            b->x = d.x;
            b->y = d.y;
            b->sp->setVisible(true);
            atc--;
            ran--;
            if (ran>=0)
            {
                attachedEnemyOnlyDown--;
                CCRotateBy *rotate = CCRotateBy::create(0.8f, 360.0f);
                b->isRemoving= true;
                b->force = 0;
                b->sp->runAction(CCRepeatForever::create(rotate));
            }
            else
            {
                b->sp->setScale(1.50f);
                CCFiniteTimeAction *a = CCScaleTo::create(0.5, 1.00f,1.00f);
                CCFiniteTimeAction *f = CCFadeIn::create(0.3);
                CCSpawn *spwn = CCSpawn::create(a, f, NULL);
                b->sp->runAction(spwn);
            }
            if (atc == 0)
                break;
        }
    }
}

void Player::Hitted()
{
    if (Shielded)
        return;
    if (Bombed)
    {
        Bomb();
        return;
    }
    bool NeedRemove = false;
    for (BonusType* bonust : mainP->spriteBonus)
    {
        if (!bonust->Removing)
            NeedRemove = true;
    }
    if (NeedRemove)
    {
        if(spriteBonus.at(0)->Type == BonusType::STABILIZER)
        {
            Stabilized = false;
        }
        else if(spriteBonus.at(0)->Type == BonusType::WINGS)
        {
            Winged = false;
        }
        spriteBonus.at(0)->Removing = true;
        
        return;
    }
    if (IMMORTAL)
    {
        canBeHitted--;
        if (canBeHitted == 0)
            IMMORTAL = false;
        return;
    }
    
    if (IsUpgraded)
    {
        //TODO lose upgrade
    }
    else
    {
        for (attachStruct* b : PinkAttach)
        {
            b->sp->stopAllActions();
            b->sp->setVisible(false);
            b->attached = false;
            b->isRemoving = false;
        }
        
        for (attachStruct* b : BlackAttach)
        {
            b->sp->stopAllActions();
            b->sp->setVisible(false);
            b->attached = false;
            b->isRemoving = false;
        }
   
        Dying = true;
        isAccelerating = false;
    }
}

void Player::AddBonus(BonusType* bt)
{
    bonusTaken++;
    bt->StartEnterAnim(node);
    BonusVec.push_back(bt);
}

void Player::ActiveEffect (BonusType* bt)
{
    CCArray *animFrames;
    CCSpriteFrame* sprf1;
    CCSpriteFrame* sprf2;
    CCAnimation *animation;
    CCAnimate *animate;
    MainScene::ResumeAll();
    if (bt->Type == BonusType::ALLBONUS)
    {
        bool atomic     = false;
        bool shield     = false;
        bool stabilizer = false;
        bool wings      = false;
        for (BonusType* bonust : mainP->spriteBonus)
        {
            switch (bonust->Type)
            {
                case BonusType::ATOMIC:
                    atomic = true;
                    break;
                case BonusType::SHIELD:
                    shield = true;
                    break;
                case BonusType::STABILIZER:
                    stabilizer = true;
                    break;
                case BonusType::WINGS:
                    wings = true;
                    break;
            }
        }
        CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
        if (!shield)
            shield = !userDefault->getBoolForKey("SHIELD_UNLOCKED");
        if (!wings)
            wings = !userDefault->getBoolForKey("WINGS_UNLOCKED");
        if (!atomic)
        {
            mainP->Bombed = true;
            BonusType* bt1 = new BonusType(BonusType::ATOMIC, "");
            bt1->spr =CCSprite::createWithSpriteFrameName("OnBall_Atomic.png");
            mainP->node->addChild(bt1->spr, 1);
            bt1->rot = 90;
            bt1->spr->setPosition(mainP->ship->getPosition());
            mainP->spriteBonus.push_back(bt1);
        }
        if (!shield)
        {
            mainP->StartShiled = mainP->GetMilliCount();
            mainP->Shielded = true;
            BonusType* bt2 = new BonusType(BonusType::SHIELD, "");
            bt2->spr =CCSprite::createWithSpriteFrameName("OnJake_Shield.png");
            mainP->node->addChild(bt2->spr, 1);
            bt2->spr->setPosition(mainP->ship->getPosition());
            mainP->spriteBonus.push_back(bt2);
        }
        if (!stabilizer)
        {
            mainP->Stabilized = true;
            BonusType* bt3 = new BonusType(BonusType::STABILIZER, "");
            bt3->spr =CCSprite::createWithSpriteFrameName("OnJake_Stabilizer_Frame1.png");
            mainP->node->addChild(bt3->spr, 1);
            bt3->spr->setPosition(mainP->ship->getPosition());
            bt3->rot = 0;
            mainP->spriteBonus.push_back(bt3);
            animFrames = CCArray::create();
            sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame2.png");
            sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame1.png");
            animFrames->addObject(sprf1);
            animFrames->addObject(sprf2);
            animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
            animate = CCAnimate::create(animation);
            bt3->spr->runAction(CCRepeatForever::create(animate));
        }
        if (!wings)
        {
            mainP->Winged = true;
            bt->Type = BonusType::WINGS;
            bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Wing_Frame1.png");
            mainP->node->addChild(bt->spr, 1);
            bt->spr->setPosition(mainP->ship->getPosition());
            mainP->spriteBonus.push_back(bt);
            animFrames = CCArray::create();
            sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame2.png");
            animFrames->addObject(sprf1);
            sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame1.png");
            animFrames->addObject(sprf2);
            animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
            animate = CCAnimate::create(animation);
            bt->spr->runAction(CCRepeatForever::create(animate));
        }
    }
    else
    {
        switch (bt->Type)
        {
            case BonusType::ATOMIC:
                mainP->Bombed = true;
                bt->spr =CCSprite::createWithSpriteFrameName("OnBall_Atomic.png");
                bt->spr->setPosition(mainP->ship->getPosition());
                bt->rot = 90;
                break;
            case BonusType::SHIELD:
                mainP->StartShiled = mainP->GetMilliCount();
                mainP->Shielded = true;
                bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Shield.png");
                bt->spr->setPosition(mainP->ship->getPosition());
                break;
            case BonusType::STABILIZER:
                mainP->Stabilized = true;
                bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Stabilizer_Frame1.png");
                bt->spr->setPosition(mainP->ship->getPosition());
                bt->rot = 0;
                animFrames = CCArray::create();
                sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame2.png");
                sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame1.png");
                animFrames->addObject(sprf1);
                animFrames->addObject(sprf2);
                animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
                animate = CCAnimate::create(animation);
                bt->spr->runAction(CCRepeatForever::create(animate));
                break;
            case BonusType::WINGS:
                mainP->Winged = true;
                mainP->WingedPerc = 30;
                mainP->StartWing = mainP->GetMilliCount();
                bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Wing_Frame1.png");
                bt->spr->setPosition(mainP->ship->getPosition());
                animFrames = CCArray::create();
                sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame2.png");
                animFrames->addObject(sprf1);
                sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame1.png");
                animFrames->addObject(sprf2);
                animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
                animate = CCAnimate::create(animation);
                bt->spr->runAction(CCRepeatForever::create(animate));
                break;
        }
        mainP->node->addChild(bt->spr, 1);
        mainP->spriteBonus.push_back(bt);
    }
}

void Player::RemoveExp()
{
    explosion->removeAllChildrenWithCleanup(true);
    node->removeChild(explosion, true);
}

void Player::Bomb()
{
    Bombed = false;
    MainScene::RemoveAll();
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    explosion = CCSprite::createWithSpriteFrameName("OnJake_AtomicExplosion.png");
    CCScaleTo* scale = CCScaleTo::create(0.5, 2.0);
    node->addChild(explosion, 1000);
    explosion->setPosition(ccp(winSize.width/2, winSize.height/2));
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(Player::RemoveExp));
    explosion->runAction(CCSequence::create(scale, call, NULL));

    CCRotateBy *rotate = CCRotateBy::create(0.8f, 360.0f);
    for (BonusType* b : spriteBonus)
    {
        switch (b->Type)
        {
            case BonusType::ATOMIC:
                b->Removing= true;
                b->force = 0;
                b->spr->runAction(CCRepeatForever::create(rotate));
                break;
        }
    }
}

void Player::RemoveShield()
{
    for (BonusType* b : spriteBonus)
    {
        if (b->Type == BonusType::SHIELD)
        {
            b->spr->stopAllActions();
            b->spr->setVisible(false);
            b->Removing = true;
            //b->Removed = true;
            //b->spr->removeAllChildrenWithCleanup(true);
            //node->removeChild(b->spr, true);
        }
    }
}

void Player::Update(float dt)
{
    if (Paused)
        return;
    vector<BonusType*> tempspriteBonus;
    for (BonusType* b : spriteBonus)
    {
        if (b->Type == BonusType::SHIELD)
        {
            if (Shielded && GetMilliCount()-StartShiled > SHIELDCOUNT )
            {
                Shielded = false;
                CCFadeOut *moveOut = CCFadeOut::create(0.5);
                CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(Player::RemoveShield));
                b->spr->runAction(CCSequence::create(moveOut, call, NULL));
            }
        }
        int f = 0;
        if (b->Removing && b->Type != BonusType::SHIELD)
        {
            if (b->force == 0)
            {
                b->dx = b->spr->getPosition().x;
                b->dy = b->spr->getPosition().y;
            }
            f=b->force;
            b->force += 150*dt;
            int px = b->dx-f;
            int py = b->dy-f;

            b->spr->setPosition(ccp(px, py));
            if (b->spr->getPosition().x<-b->spr->getContentSize().width || b->spr->getPosition().y+b->spr->getContentSize().height < 0)
            {
                b->spr->stopAllActions();
                b->spr->setVisible(false);
                b->Removing = false;
                b->spr->removeAllChildrenWithCleanup(true);
                node->removeChild(b->spr, true);
            }
            else
                tempspriteBonus.push_back(b);
        }
        else if (b->Type == BonusType::SHIELD && b->Removing)
        {
            b->spr->removeAllChildrenWithCleanup(true);
            node->removeChild(b->spr, true);
        }
        else if (b->Type != BonusType::SHIELD && b->Type != BonusType::WINGS)
        {
            float scale = 2/(3-cosf(2*b->time));
            float x = scale * cosf(b->time);
            float y = scale * sinf(2*b->time) / 2;
            if (b->rot == 90)
            {
                float temp = y;
                y = x;
                x = temp;
            }
            x=x*100;
            y=y*100;
            b->spr->setPosition(ccp(ship->getPosition().x+x, ship->getPosition().y+y));
            b->time+=2.33*dt;
            tempspriteBonus.push_back(b);
        }
        else if (b->Type == BonusType::WINGS)
        {
            if (Winged && GetMilliCount()-StartWing > WINGCOUNT )
            {
                StartWing = GetMilliCount();
                WingedPerc--;
                if (WingedPerc<0)
                    WingedPerc = 0;
            }
            b->spr->setPosition(ccp(ship->getPosition().x-50, ship->getPosition().y+50));
            tempspriteBonus.push_back(b);
        }
        else if (b->Type == BonusType::SHIELD)
        {
            b->spr->setPosition(ship->getPosition());
            tempspriteBonus.push_back(b);
        }
        else if (!b->Removing)
            tempspriteBonus.push_back(b);
    }
    spriteBonus = tempspriteBonus;

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    int accelerate = Stabilized ? 50 : 30;
    if (!isAccelerating)
    {
        if (acceleration>(MAXTTACHED+5)-((attachedEnemy+attachedEnemyOnlyDown)))
            acceleration = (MAXTTACHED+5)-((attachedEnemy+attachedEnemyOnlyDown));
        acceleration -= (30-((attachedEnemy+attachedEnemyOnlyDown)))*dt;
        if (acceleration < - ((MAXTTACHED+5)-((attachedEnemy+attachedEnemyOnlyDown))))
            acceleration = -((MAXTTACHED+5)-((attachedEnemy+attachedEnemyOnlyDown)));
    }
    else
    {
        if (acceleration<-5)
            acceleration = -5;
        acceleration += (accelerate-((attachedEnemy)))*dt;
        if (acceleration >  (MAXTTACHED+10)-((attachedEnemy)))
            acceleration = (MAXTTACHED+10)-((attachedEnemy));
    }
    if (ship->getPosition().y+acceleration < (ship->getContentSize().height)/2)
    {
        acceleration = 0;
        ship->setPosition(ccp( 50+ship->getContentSize().width/2, (ship->getContentSize().height/2)));
    }
    else if (ship->getPosition().y+((ship->getContentSize().height)/2)+acceleration>winSize.height && isAccelerating)
    {
        acceleration = 0;
        ship->setPosition(ccp(50+ship->getContentSize().width/2, winSize.height-(ship->getContentSize().height)/2));
    }
    else if (ship->getPosition().y+((ship->getContentSize().height)/2)+acceleration>winSize.height && acceleration >0)
    {
        acceleration = 0;
        ship->setPosition(ccp(50+ship->getContentSize().width/2, winSize.height-(ship->getContentSize().height)/2));
    }
    else
        ship->setPosition(ccp(50+ship->getContentSize().width/2, ship->getPosition().y+acceleration));
    for (int i=0; i<MAXTTACHED; i++)
    {
        attachStruct* b = BlackAttach.at(i);
        if (!b->attached)
            continue;
        int f = 0;
        if (b->isRemoving)
        {
            if (b->force == 0)
            {
                b->dx = ship->getPosition().x;
                b->dy = ship->getPosition().y;
            }
            f=b->force;
            b->force += 56*dt;
        }
        float px = ship->getPosition().x;
        float py = ship->getPosition().y;

        if (f !=0 )
        {
            px = b->dx-f;
            py = b->dy-f;
        }
        b->sp->setPosition(ccp(b->x+px, py-b->y));
        if (b->isRemoving)
        {
            if (b->sp->getPosition().x<-b->sp->getContentSize().width || b->sp->getPosition().y+b->sp->getContentSize().height < 0)
            {
                b->sp->stopAllActions();
                b->sp->setVisible(false);
                b->attached = false;
                b->isRemoving = false;
            }
        }
        b->sp->setVisible(ship->isVisible());
    }
    for (int i=0; i<MAXTTACHED; i++)
    {
        attachStruct* b = PinkAttach.at(i);
        if (!b->attached)
            continue;
        int f = 0;
        if (b->isRemoving)
        {
            if (b->force == 0)
            {
                b->dx = ship->getPosition().x;
                b->dy = ship->getPosition().y;
            }
            f=b->force;
            b->force += 56*dt;
        }
        float px = ship->getPosition().x;
        float py = ship->getPosition().y;
        if (f !=0 )
        {
            px = b->dx-f;
            py = b->dy-f;
        }
        b->sp->setPosition(ccp(b->x+px, py-b->y));
        if (b->isRemoving)
        {
            if (b->sp->getPosition().x<-b->sp->getContentSize().width)
            {
                b->sp->stopAllActions();
                b->sp->setVisible(false);
                b->attached = false;
                b->isRemoving = false;
            }
        }
        b->sp->setVisible(ship->isVisible());
    }

    if (Pressed && Animation == ANIM_DOWN)
        shipOnAir();
    else if (Pressed && (getTimeTick()-Risetime)>RISE_TIME && ship->getPosition().y<winSize.height-ship->getContentSize().height/2)
        shipGoesUp();
    else if (!Pressed && (getTimeTick()-Falltime)>FALL_TIME && ship->getPosition().y>ship->getContentSize().height/2)
        this->shipGoesDown();
    else
        this->shipOnAir();
    if(!Dying)
    {
        if(ship->getPositionY()<=winSize.height/2)
        {
            shadow->setVisible(true);
            
            float scaleOrig = ship->getPositionY()/(winSize.height/2);
            float scale = 1-scaleOrig+0.25;
            
            float opacity = 255*scale;
            
            shadow->setOpacity(opacity);
            shadow->setScale(scale);
        }
        else
        {
            shadow->setVisible(false);
        }
    }
    else
    {
        shadow->setVisible(false);
    }
}

void Player::touchBegan()
{
    Pressed = true;
    Risetime = getTimeTick();
}

void Player::Pause()
{
    Paused = true;
    ship->pauseSchedulerAndActions();
}

void Player::UnPause()
{
    Paused = false;
    ship->resumeSchedulerAndActions();
}


void Player::touchEnded()
{
    Pressed = false;
    if ((getTimeTick()-Clicktime)<300)
    {
        Clicktime = 0;
        amountOfClick++;
    }
    else
        amountOfClick = 0;
    if (amountOfClick >=3)
    {
        amountOfClick = 0;
        attachStruct* b = NULL;
        if (attachedEnemy>0 && attachedEnemyOnlyDown>0)
        {
            int r = (int)randomValueBetween(0, 2);
            
            if (r == 0)
            {
                for (int i=0; i<MAXTTACHED; i++)
                {
                    attachStruct* t = BlackAttach.at(i);
                    if (!t->attached || t->isRemoving)
                        continue;
                    b = t;
                    break;
                }
                attachedEnemy--;
                if (attachedEnemy<0)
                    attachedEnemy = 0;
            }
            else
            {
                for (int i=0; i<MAXTTACHED; i++)
                {
                    attachStruct* t = PinkAttach.at(i);
                    if (!t->attached || t->isRemoving)
                        continue;
                    b = t;
                    break;
                }
                attachedEnemyOnlyDown--;
                if (attachedEnemyOnlyDown<0)
                    attachedEnemyOnlyDown = 0;
            }
        }
        else if (attachedEnemy>0)
        {
            for (int i=0; i<MAXTTACHED; i++)
            {
                attachStruct* t = BlackAttach.at(i);
                if (!t->attached || t->isRemoving)
                    continue;
                b = t;
                break;
            }
            attachedEnemy--;
            if (attachedEnemy<0)
                attachedEnemy = 0;
        }
        else if (attachedEnemyOnlyDown>0)
        {
            for (int i=0; i<MAXTTACHED; i++)
            {
                attachStruct* t = PinkAttach.at(i);
                if (!t->attached || t->isRemoving)
                    continue;
                b = t;
                break;
            }
            attachedEnemyOnlyDown--;
            if (attachedEnemyOnlyDown<0)
                attachedEnemyOnlyDown = 0;
        }
        if (b != NULL)
        {
            CCRotateBy *rotate = CCRotateBy::create(0.8f, 360.0f);
            b->isRemoving= true;
            b->force = 0;
            b->sp->runAction(CCRepeatForever::create(rotate));
        }
        
    }
    Clicktime = getTimeTick();
    Falltime = getTimeTick();
    CCLog("amountOfClick: %i", amountOfClick);
}

float Player::getTimeTick()
{
    timeval time;
    gettimeofday(&time, NULL);
    unsigned long millisecs = (time.tv_sec * 1000) + (time.tv_usec/1000);
    return (float) millisecs;
}

void Player::shipOnAir()
{
    if (Animation == ANIM_FLY)
        return;
    Animation = ANIM_FLY;
    ship->stopAllActions();
    CCArray *animFrames = CCArray::create();
    for (int i=0; i<spriteAnimFly.size(); i++)
    {
        animFrames->addObject(spriteAnimFly.at(i));
    }
    CCAnimation* FlyAnim = CCAnimation::createWithSpriteFrames(animFrames, 0.2f);
    FlyAnim->setLoops(-1);
    FlyAnim->setDelayPerUnit(0.2);
    FlyAnim->setRestoreOriginalFrame(true);
    CCAnimate *animate = CCAnimate::create(FlyAnim);
    ship->runAction(animate);
}

void Player::shipGoesUp()
{
    if (Animation == ANIM_UP)
        return;
    Animation = ANIM_UP;
    ship->stopAllActions();
    CCArray *animFrames = CCArray::create();
    for (int i=0; i<spriteAnimUp.size()-1; i++)
    {
        animFrames->addObject(spriteAnimUp.at(i));
    }
    CCAnimation* FlyAnim = CCAnimation::createWithSpriteFrames(animFrames, 0.2f);
    FlyAnim->setLoops(1);
    FlyAnim->setDelayPerUnit(0.1);
    FlyAnim->setRestoreOriginalFrame(false);
    CCAnimate *animate = CCAnimate::create(FlyAnim);
    ship->runAction(animate);
}

void Player::shipGoesDown()
{
    if (Animation == ANIM_DOWN)
        return;
    Animation = ANIM_DOWN;
    ship->stopAllActions();
    CCArray *animFrames = CCArray::create();
    for (int i=0; i<spriteAnimDown.size()-1; i++)
    {
        animFrames->addObject(spriteAnimDown.at(i));
    }
    CCAnimation* FlyAnim = CCAnimation::createWithSpriteFrames(animFrames, 0.2f);
    FlyAnim->setLoops(1);
    FlyAnim->setDelayPerUnit(0.1);
    FlyAnim->setRestoreOriginalFrame(false);
    CCAnimate *animate = CCAnimate::create(FlyAnim);
    ship->runAction(animate);
}

float Player::randomValueBetween(float low, float high)
{
    return (((float) arc4random() / 0xFFFFFFFF) * (high - low)) + low;
}

int Player::GetMilliCount()
{
    timeb tb;
    ftime( &tb );
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int Player::GetMilliSpan( int nTimeStart )
{
    int nSpan = GetMilliCount() - nTimeStart;
    if ( nSpan < 0 )
        nSpan += 0x100000 * 1000;
    return nSpan;
}

