#ifndef __LEAD_SCENE_H__
#define __LEAD_SCENE_H__

#include "cocos2d.h"
#include "CCLocalizedString.h"

using namespace cocos2d;
using namespace std;
USING_NS_CC;

//class StoreScene : public cocos2d::CCLayerColor
class Leaderboard : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    CCMenuItemSprite *worldItem;
    CCMenuItemSprite *localeItem;
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(Leaderboard);
    
    void onExit();
    
    void backToMenu(CCObject* pSender);
    void MoveUp(CCObject* pSender);
    void MoveDown(CCObject* pSender);
    
    CCMenu* menuBack;
    CCMenu* menuUp;
    CCMenu* menuDown;
    CCMenu* menuStore;
    CCLayer *layerItems;
    bool WorldSelected;
    int StartPos;
    void WorldScore();
    void FriendScore();
    void RefreshData();
    CCMenu *pMainMenu;
    CCSprite* bgLoad;
    CCSprite* Loading;
};

#endif