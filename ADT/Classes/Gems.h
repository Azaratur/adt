//
//  Gems.h
//  AdventureTime
//
//  Created by Johnny Picciafuochi on 19/09/2013.
//
//

#ifndef __AdventureTime__Gems__
#define __AdventureTime__Gems__

#include <iostream>
#include "CCParallaxNodeExtras.h"
#include <sys/timeb.h>
#include "Player.h"

USING_NS_CC;
class Gems
{
    public:
    int Time;
    float width;
    bool Initialized;
    std::vector<CCSprite*> stars;
    Gems();
    bool statusAudio;
    CCParallaxNodeExtras* node;
    void Destroy();
    bool isIntersectingCircle(CCPoint p1, CCPoint p2, float r1, float r2);

    void Init(const char f [], CCParallaxNodeExtras* node, float startx, float y, char c []);
    void Update(CCPoint sP, float radius, Player* ship, bool Update);
    void Draw();
    void UnPause();
    void Pause();
    int GetMilliCount();
};

#endif /* defined(__AdventureTime__Gems__) */
