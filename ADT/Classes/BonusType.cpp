//
//  BonusType.cpp
//  ADT
//
//  Created by Johnny Picciafuochi on 07/02/2014.
//
//

#include "BonusType.h"
#include "Player.h"
#include "MainScene.h"
using namespace cocos2d;

BonusType::BonusType(int t, string n)
{
    Type = t;
    time = 0;
    name = n;
    Removing = false;
    force = 0;
    rot = 0;
}

void BonusType::StartEnterAnim(cocos2d::CCSpriteBatchNode * _batchNode)
{
    node = _batchNode;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    char *a=new char[name.size()+1];
    a[name.size()]=0;
    memcpy(a,name.c_str(),name.size());
    sprite = CCSprite::createWithSpriteFrameName(a);
    node->addChild(sprite, 1000);
    bg = CCSprite::create();
    int w = winSize.width;
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    _raw[0] = 255;
    _raw[1] = 255;
    _raw[2] = 255;
    _raw[3] = 255;
    
    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg->initWithTexture(tex, CCRectMake(0, 0, w, 5));
    bg->setPosition(ccp(winSize.width/2, winSize.height/2));
    ccColor3B color;
    color.r = 255;
    color.g = 255;
    color.b = 100;

    bg->setColor(color);
    bg->setOpacity(150);
    MainScene::AddNewChild(bg, 0);
    
    CCScaleTo *sizeIn = CCScaleTo::create(0.5, 1.0, 30.0);
    CCScaleTo *sizeOut = CCScaleTo::create(0.5, 30.0, 1.0);
    sprite->setPosition(ccp(-sprite->getContentSize().width, winSize.height/2));
    CCDelayTime *delayFade = CCDelayTime::create(0.5);
    CCMoveTo *moveIn = CCMoveTo::create(0.5, CCPointMake(winSize.width/2, winSize.height/2));
    CCMoveTo *moveOut = CCMoveTo::create(0.5, CCPointMake(winSize.width+sprite->getContentSize().width, winSize.height/2));
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(BonusType::AddBonus));
    sprite->runAction(CCSequence::create(delayFade, moveIn, delayFade, moveOut, call, NULL));
    bg->runAction(CCSequence::create(delayFade, sizeIn, delayFade, sizeOut, NULL));
}

void BonusType::AddBonus(cocos2d::CCObject *pSender)
{
    node->removeChild(sprite, true);
    MainScene::RemoveChild(bg);
    Player::ActiveEffect(this);
}