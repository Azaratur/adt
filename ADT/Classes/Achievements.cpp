#include "Achievements.h"
#include "MenuScene.h"

#include "CCSlidingLayer.h"

using namespace cocos2d;

static Achievements * myScene = NULL;

CCScene* Achievements::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    Achievements *layer = Achievements::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool Achievements::init()
{

    if ( !CCLayer::init() )
    {
        
        return false;
    }
    
    myScene = this;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("achiev_texture.plist", "achiev_texture.png");
    
    CCSprite *bg = CCSprite::createWithSpriteFrameName("Set_Achiev.jpg");
    
    if (bg->getContentSize().width<size.width)
        bg->setScaleX(1.5);
    bg->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(bg, 1);
    
    CCSprite *bgTitle = CCSprite::createWithSpriteFrameName("TitleSkin.png");
    bgTitle->setPosition(ccp(size.width/2, size.height-30-(bgTitle->getContentSize().height/2)));
    this->addChild(bgTitle, 2);
    
    CCLabelBMFont *labelTitle = CCLabelBMFont::create(CCLocalizedString("t1009", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    labelTitle->setPosition(ccp(bgTitle->getContentSize().width/2, bgTitle->getContentSize().height/2));
    bgTitle->addChild(labelTitle, 5);
    
    CCSprite *bgGems = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    bgGems->setPosition(ccp(size.width-(bgGems->getContentSize().width/2)-20, 50));
    this->addChild(bgGems, 2);
    
    CCSprite *boxGems = CCSprite::createWithSpriteFrameName("Gui_Gems.png");
    boxGems->setPosition(ccp(size.width-(bgGems->getContentSize().width/2)-110, 45));
    this->addChild(boxGems, 2);
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    money = userDefault->getIntegerForKey("total_gems");
    
    CCLog("Sacchi %i", money);

    char c_gems[10];
    sprintf(c_gems, "%i", money);
    
    labelMoney = CCLabelBMFont::create(c_gems, "stats_font.fnt", 80, kCCTextAlignmentCenter);
    labelMoney->setPosition(ccp(bgGems->getPositionX()+30, bgGems->getPositionY()-5));
    this->addChild(labelMoney, 5);
    
    bool meter1 = userDefault->getBoolForKey("300M");
    bool meter2 = userDefault->getBoolForKey("1000M");
    bool meter3 = userDefault->getBoolForKey("3000M");
    bool enemy1 = userDefault->getBoolForKey("50E");
    bool enemy2 = userDefault->getBoolForKey("200E");
    bool enemy3 = userDefault->getBoolForKey("300E");
    bool bonus1 = userDefault->getBoolForKey("20B");
    bool bonus2 = userDefault->getBoolForKey("50B");
    bool bonus3 = userDefault->getBoolForKey("100B");
    bool gems1 = userDefault->getBoolForKey("100G");
    bool gems2 = userDefault->getBoolForKey("500G");
    bool gems3 = userDefault->getBoolForKey("600G");
    
    bool mistery1 = userDefault->getBoolForKey("MISTERY1");
    bool mistery2 = userDefault->getBoolForKey("MISTERY2");
    bool mistery3 = userDefault->getBoolForKey("MISTERY3");
    
    int showBonus = 0;
    if (meter1 && enemy1 && bonus1 && gems1)
        showBonus++;
    if (meter2 && enemy2 && bonus2 && gems2)
        showBonus++;
    
    structStore s1;
    structStore s2;
    structStore s3;
    structStore s4;
    structStore s5;
    
    structStore s6;
    structStore s7;
    structStore s8;
    structStore s9;
    structStore s10;
    
    structStore s11;
    structStore s12;
    structStore s13;
    structStore s14;
    structStore s15;

    
 
    //    structStore s1;
    if (meter1)
        s1.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev.png");
    else
        s1.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev_off.png");
    s1.off = meter1;
    s1.sprname = "Ico1_Achiev.png";
    s1.name = CCLocalizedString("t1010", "dito qui");
    s1.money = 200;
    
    if (enemy1)
        s2.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev.png");
    else
        s2.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev_off.png");
    s2.off = enemy1;
    s2.sprname = "Ico3_Achiev.png";
    s2.name = CCLocalizedString("t1013", "dito qui");
    s2.money = 400;
    
    if (bonus1)
        s3.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev.png");
    else
        s3.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev_off.png");
    s3.off = bonus1;
    s3.sprname = "Ico2_Achiev.png";
    s3.name = CCLocalizedString("t1016", "dito qui");
    s3.money = 200;
    
    if (gems1)
        s4.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev.png");
    else
        s4.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev_off.png");
    s4.off = gems1;
    s4.sprname = "Ico4_Achiev.png";
    s4.name = CCLocalizedString("t1019", "dito qui");
    s4.money = 200;
    
    s5.spr = CCSprite::createWithSpriteFrameName("Ico_Mistery_Achiev.png");
    s5.off = mistery1;
    s5.sprname = "Buy_Shield.png";
    s5.name = "MISTERY";
    s5.money = 0;
    
    if (meter2)
        s6.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev.png");
    else
        s6.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev_off.png");
    s6.off = meter2;
    s6.sprname = "Ico1_Achiev.png";
    s6.name = CCLocalizedString("t1011", "dito qui");
    s6.money = 400;
    
    if (enemy2)
        s7.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev.png");
    else
        s7.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev_off.png");
    s7.off = enemy2;
    s7.sprname = "Ico3_Achiev.png";
    s7.name = CCLocalizedString("t1014", "dito qui");
    s7.money = 600;
    
    if (bonus2)
        s8.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev.png");
    else
        s8.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev_off.png");
    s8.off = bonus2;
    s8.sprname = "Ico2_Achiev.png";
    s8.name = CCLocalizedString("t1017", "dito qui");
    s8.money = 400;
    
    if (gems2)
        s9.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev.png");
    else
        s9.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev_off.png");
    s9.off = gems2;
    s9.sprname = "Ico4_Achiev.png";
    s9.name = CCLocalizedString("t1020", "dito qui");
    s9.money = 400;
    
    s10.spr = CCSprite::createWithSpriteFrameName("Ico_Mistery_Achiev.png");
    s10.off = mistery2;
    s10.sprname = "Buy_Wings.png";
    s10.name = "MISTERY";
    s10.money = 0;
    
    if (meter3)
        s11.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev.png");
    else
        s11.spr = CCSprite::createWithSpriteFrameName("Ico1_Achiev_off.png");
    s11.off = meter3;
    s11.sprname = "Ico1_Achiev.png";
    s11.name = CCLocalizedString("t1012", "dito qui");
    s11.money = 600;
    
    if (enemy3)
        s12.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev.png");
    else
        s12.spr = CCSprite::createWithSpriteFrameName("Ico3_Achiev_off.png");
    s12.off = enemy3;
    s12.sprname = "Ico3_Achiev.png";
    s12.name = CCLocalizedString("t1015", "dito qui");
    s12.money = 300;
    
    if (bonus3)
        s13.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev.png");
    else
        s13.spr = CCSprite::createWithSpriteFrameName("Ico2_Achiev_off.png");
    s13.off = bonus3;
    s13.sprname = "Ico2_Achiev.png";
    s13.name = CCLocalizedString("t1018", "dito qui");
    s13.money = 600;
    
    if (gems3)
        s14.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev.png");
    else
        s14.spr = CCSprite::createWithSpriteFrameName("Ico4_Achiev_off.png");
    s14.sprname = "Ico4_Achiev.png";
    s14.off = gems3;
    s14.name = CCLocalizedString("t1021", "dito qui");
    s14.money = 600;
    
    if (mistery3)
        s15.spr = CCSprite::createWithSpriteFrameName("Ico5_Achiev.png");
    else
        s15.spr = CCSprite::createWithSpriteFrameName("Ico_Mistery_Achiev.png");
    s15.off = mistery3;
    s15.sprname = "Ico5_Achiev.png";
    s15.name = "MISTERY";
    s15.money = 0;
    if (showBonus == 0)
    {
        arrayStore.push_back(s1);
        arrayStore.push_back(s2);
        arrayStore.push_back(s3);
        arrayStore.push_back(s4);
        arrayStore.push_back(s5);
    }
    else if (showBonus == 1)
    {
        arrayStore.push_back(s6);
        arrayStore.push_back(s7);
        arrayStore.push_back(s8);
        arrayStore.push_back(s9);
        arrayStore.push_back(s10);
    }
    else
    {
        arrayStore.push_back(s11);
        arrayStore.push_back(s12);
        arrayStore.push_back(s13);
        arrayStore.push_back(s14);
        arrayStore.push_back(s15);
    }
    showStore.push_back(s1);
    showStore.push_back(s2);
    showStore.push_back(s3);
    showStore.push_back(s4);
    showStore.push_back(s5);
    showStore.push_back(s6);
    showStore.push_back(s7);
    showStore.push_back(s8);
    showStore.push_back(s9);
    showStore.push_back(s10);
    showStore.push_back(s11);
    showStore.push_back(s12);
    showStore.push_back(s13);
    showStore.push_back(s14);
    showStore.push_back(s15);
        /*arrayStore.push_back(s6);
    arrayStore.push_back(s7);
    arrayStore.push_back(s8);
    arrayStore.push_back(s9);
    arrayStore.push_back(s10);
    arrayStore.push_back(s11);
    arrayStore.push_back(s12);*/
    

    layerItems = CCLayer::create();
    layerItems->setContentSize( CCSizeMake( 3000, 400));
    
    CCSprite *btnExit = CCSprite::createWithSpriteFrameName("Back.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(btnExit, btnExit, this, menu_selector(Achievements::backToMenu));
    backItem->setPosition( ccp(90, 90) );
       
    
    for(int j=0; j<arrayStore.size(); j++)
    {
        if (arrayStore[j].off)
            arrayStore[j].bgSpr = CCSprite::createWithSpriteFrameName("Buy_Skin.png");
        else if (arrayStore[j].name.compare("MISTERY") == 0)
            arrayStore[j].bgSpr = CCSprite::createWithSpriteFrameName("Key_Off_NOGems.png");
        else
            arrayStore[j].bgSpr = CCSprite::createWithSpriteFrameName("Key_Off.png");
        arrayStore[j].bgSpr->addChild(arrayStore[j].spr);
        arrayStore[j].spr->setPosition(ccp(arrayStore[j].bgSpr->getContentSize().width/2, 90+(arrayStore[j].bgSpr->getContentSize().height/2)));
        if (arrayStore[j].name.compare("MISTERY") != 0)
        {
            CCLabelBMFont *labelTitle = CCLabelBMFont::create(arrayStore[j].name.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
            labelTitle->setPosition(ccp(arrayStore[j].bgSpr->getContentSize().width/2, 60+(arrayStore[j].bgSpr->getContentSize().height/2)-arrayStore[j].spr->getContentSize().height/2));
            arrayStore[j].bgSpr->addChild(labelTitle);
            std::stringstream out;
            out << arrayStore[j].money;
            std::string converted = out.str();
            labelTitle = CCLabelBMFont::create(converted.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
            labelTitle->setPosition(ccp(50+arrayStore[j].bgSpr->getContentSize().width/2, -20+(arrayStore[j].bgSpr->getContentSize().height/2)-arrayStore[j].spr->getContentSize().height/2));
            arrayStore[j].bgSpr->addChild(labelTitle);
        }
        arrayStore[j].bgSpr->setPosition(ccp(150+(500*j), 0));
        layerItems->addChild(arrayStore[j].bgSpr);
    }
    
  
    // create menu, it's an autorelease object
    menuBack = CCMenu::create(backItem, NULL);
    menuBack->setPosition( CCPointZero );
    this->addChild(menuBack, 2);
    
    /*menuStore = CCMenu::createWithArray(arrayItemsMenu);
    menuStore->setPosition( CCPointZero );
    layer->addChild(menuStore, 3);*/
    //CCLog("Dimensioni %f-%f", layer->getContentSize().width, layer->getContentSize().height);
    CCSlidingLayer *sl = CCSlidingLayer::create(Horizontally, CCSizeMake(layerItems->getContentSize().width, layerItems->getContentSize().height/2), CCRectMake(0, 0, layerItems->getContentSize().width/2, layerItems->getContentSize().height*1.5), ccc4(0,0,0,0), "match", false);
    
    sl->addChildWithSize(layerItems, layerItems->getContentSize(), kAlignmentLeft);
    sl->setColor(ccRED);
    this->addChild(sl, 10);
    
    
    bool MustShow [15];
    MustShow[0] = userDefault->getBoolForKey("MustShow_300M");
    MustShow[1] = userDefault->getBoolForKey("MustShow_50E");
    MustShow[2] = userDefault->getBoolForKey("MustShow_20B");
    MustShow[3] = userDefault->getBoolForKey("MustShow_100G");
    MustShow[4] = userDefault->getBoolForKey("MUSTSHOW_MISTERY1");
    
    MustShow[5] = userDefault->getBoolForKey("MustShow_1000M");
    MustShow[6] = userDefault->getBoolForKey("MustShow_200E");
    MustShow[7] = userDefault->getBoolForKey("MustShow_50B");
    MustShow[8] = userDefault->getBoolForKey("MustShow_500G");
    MustShow[9] = userDefault->getBoolForKey("MUSTSHOW_MISTERY2");
    
    MustShow[10] = userDefault->getBoolForKey("MustShow_3000M");
    MustShow[11] = userDefault->getBoolForKey("MustShow_300E");
    MustShow[12] = userDefault->getBoolForKey("MustShow_100B");
    MustShow[13] = userDefault->getBoolForKey("MustShow_600G");
    MustShow[14] = userDefault->getBoolForKey("MUSTSHOW_MISTERY3");

  //  animFrames = new CCArray::create();
    moneytmp = money;
    for (int i=0; i<15; i++)
    {
        if (MustShow[i])
        {
            CCSprite* spr = CCSprite::createWithSpriteFrameName("Buy_Skin.png");
            CCSprite* spr1 = CCSprite::createWithSpriteFrameName(showStore[i].sprname.c_str());
            spr->addChild(spr1);
            spr1->setPosition(ccp(spr->getContentSize().width/2, 90+(spr->getContentSize().height/2)));
            if (showStore[i].sprname.compare("Buy_Shield.png") != 0 && showStore[i].sprname.compare("Buy_Shield.png") != 0 && showStore[i].sprname.compare("Ico5_Achiev.png") != 0)
            {
/*                CCLabelBMFont *labelTitle = CCLabelBMFont::create(CCLocalizedString("t1034", "dito qui"), "stats_font.fnt", 800, kCCTextAlignmentCenter);
                labelTitle->setPosition(ccp(spr->getContentSize().width/2, 60+(spr->getContentSize().height/2)-spr1->getContentSize().height/2));
                spr->addChild(labelTitle);*/
                CCLabelBMFont *labelTitle = CCLabelBMFont::create(showStore[i].name.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
                labelTitle->setPosition(ccp(spr->getContentSize().width/2, 60+(spr->getContentSize().height/2)-spr1->getContentSize().height/2));
                spr->addChild(labelTitle);
                std::stringstream out;
                out << showStore[i].money;
                std::string converted = out.str();
                labelTitle = CCLabelBMFont::create(converted.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
                labelTitle->setPosition(ccp(50+spr->getContentSize().width/2, -20+(spr->getContentSize().height/2)-spr1->getContentSize().height/2));
                spr->addChild(labelTitle);
            }
            spr->setPosition(ccp(size.width/2, size.height/2));
            spr->setTag(i);
            this->addChild(spr, 100);
            spr->setVisible(false);
            spr->setScale(0.3);
            money+=showStore[i].money;
            animFrames.addObject(spr);
        }
    }
    
    userDefault->setBoolForKey("MustShow_300M", false);
    userDefault->setBoolForKey("MustShow_1000M", false);
    userDefault->setBoolForKey("MustShow_1000M", false);
    userDefault->setBoolForKey("MUSTSHOW_MISTERY1", false);
    userDefault->setBoolForKey("MUSTSHOW_MISTERY2", false);
    userDefault->setBoolForKey("MUSTSHOW_MISTERY3", false);

    userDefault->setBoolForKey("MustShow_50E", false);
    userDefault->setBoolForKey("MustShow_200E", false);
    userDefault->setBoolForKey("MustShow_300E", false);
    
    userDefault->setBoolForKey("MustShow_20B", false);
    userDefault->setBoolForKey("MustShow_50B", false);
    userDefault->setBoolForKey("MustShow_100B", false);
    
    userDefault->setBoolForKey("MustShow_100G", false);
    userDefault->setBoolForKey("MustShow_500G", false);
    userDefault->setBoolForKey("MustShow_600G", false);
    userDefault->setIntegerForKey("total_gems", money);
    userDefault->flush();
    countAnim = 0;
    if (animFrames.count()>0)
    {
        first = (CCSprite*)animFrames.objectAtIndex(0);
        first->setVisible(true);
        CCDelayTime *delayFade = CCDelayTime::create(1.5);
        CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(Achievements::checkNext));
        first->runAction(CCSequence::create(CCScaleTo::create(0.5, 1.8f,1.8f),delayFade, call, NULL));
        moneytmp+=showStore[first->getTag()].money;
        char m[10];
        sprintf(m, "%i", moneytmp);
        labelMoney->setString(m);
    }
    return true;
}

void Achievements::checkPointRelease(int x, int y)
{
    Achievements::isTouchOnSprite(CCPointMake(x,y));
}

void Achievements::checkNext(cocos2d::CCObject *pSender)
{
    countAnim++;
    first->setVisible(false);
    if (countAnim<animFrames.count())
    {
        first = (CCSprite*)animFrames.objectAtIndex(countAnim);
        first->setVisible(true);
        CCDelayTime *delayFade = CCDelayTime::create(1.5);
        CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(Achievements::checkNext));
        first->runAction(CCSequence::create(CCScaleTo::create(0.5, 1.8f,1.8f),delayFade, call, NULL));
        moneytmp+=showStore[first->getTag()].money;
        char m[10];
        sprintf(m, "%i", moneytmp);
        labelMoney->setString(m);
    }
}

int Achievements::isTouchOnSprite(CCPoint touch)
{
    int touched = -1;
    
    for(int i = 0; i < myScene->arrayStore.size(); ++i)
    {
        CCRect c = CCRect(myScene->arrayStore[i].bgSpr->getPositionX() -(myScene->arrayStore[i].bgSpr->getContentSize().width/2*myScene->arrayStore[i].bgSpr->getScaleX()), myScene->arrayStore[i].bgSpr->getPositionY(), (myScene->arrayStore[i].bgSpr->getContentSize().width*myScene->arrayStore[i].bgSpr->getScaleX()), (myScene->arrayStore[i].bgSpr->getContentSize().height*myScene->arrayStore[i].bgSpr->getScaleY()));
        if(c.containsPoint(touch))
        {
            touched = i;
            
        }
    }
    if (touched >= 0 && myScene->money>=myScene->arrayStore[touched].money)
        myScene->requestConfirmBuy(touched);
    
    CCLOG("Sto toccando lo spicchio %i", touched);
    
    return touched;
}

void Achievements::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void Achievements::requestConfirmBuy(int index)
{
    menuBack->setEnabled(false);
    //menuStore->setEnabled(false);
    
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    bgLoading = CCSprite::create();
    bgLoading->setTextureRect(CCRectMake(0, 0, size.width, size.height));
    bgLoading->setPosition(ccp(size.width/2, size.height/2));
    bgLoading->setColor(ccBLACK);
    bgLoading->setOpacity(200);
    this->addChild(bgLoading, 100);

    CCSprite *bgTitle = CCSprite::createWithSpriteFrameName("TitleSkin.png");
    bgTitle->setPosition(ccp(size.width/2, size.height-50-(bgTitle->getContentSize().height/2)));
    this->addChild(bgTitle, 102);
    
    CCLabelBMFont *labelTitle = CCLabelBMFont::create("Are you sure?", "stats_font.fnt", 800, kCCTextAlignmentCenter);
    labelTitle->setPosition(ccp(bgTitle->getContentSize().width/2, bgTitle->getContentSize().height/2));
    bgTitle->addChild(labelTitle, 5);

    //CCLog("Richiesta %s", arrayStore[tmp->getTag()].name.c_str());
    
    /*CCLabelTTF* yesLabel = CCLabelTTF::create("Yes", "Thonburi", 48);
    CCMenuItemLabel *yesItem = CCMenuItemLabel::create(yesLabel, this, menu_selector(StoreScene::buyElement));
    yesItem->setTag(index);
    yesItem->setPosition( ccp(size.width / 2-50, labelConfirmBuy->getPositionY()-100) );*/
    
    
    CCSprite *btnYes = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    CCLabelBMFont *yesLabel = CCLabelBMFont::create("Yes", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    yesLabel->setPosition(ccp(btnYes->getContentSize().width/2, btnYes->getContentSize().height/2-11));
    btnYes->addChild(yesLabel);
    CCMenuItemLabel *yesItem = CCMenuItemLabel::create(btnYes, this, menu_selector(Achievements::buyElement));
    yesItem->setTag(index);
    yesItem->setPosition( ccp(300, size.height/2) );
    
    CCSprite *btnNo = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    CCLabelBMFont *noLabel = CCLabelBMFont::create("No", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    noLabel->setPosition(ccp(btnNo->getContentSize().width/2, btnNo->getContentSize().height/2-11));
    btnNo->addChild(noLabel);
    CCMenuItemLabel *noItem = CCMenuItemLabel::create(btnNo, this, menu_selector(Achievements::rejectElement));
    noItem->setTag(index);
    noItem->setPosition( ccp(size.width -300, size.height/2) );
    
    menuBuy = CCMenu::create(yesItem, noItem, NULL);
    menuBuy->setPosition( CCPointZero );
    this->addChild(menuBuy, 102);
}

void Achievements::buyElement(cocos2d::CCObject *pSender)
{
    CCLabelTTF *tmp = (CCLabelTTF*)pSender;
    
    CCLog("Elemento comprato");
    money -= arrayStore[tmp->getTag()].money;
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    userDefault->setIntegerForKey("total_gems", money);
    userDefault->flush();
    
    char m[10];
    sprintf(m, "%i", money);
    labelMoney->setString(m);

    menuBack->setEnabled(true);
   // menuStore->setEnabled(true);
    
    this->removeChild(bgLoading, true);
    this->removeChild(menuConfirmBuy, true);
    this->removeChild(menuBuy, true);
    this->removeChild(labelConfirmBuy, true);
}

void Achievements::rejectElement(cocos2d::CCObject *pSender)
{
    menuBack->setEnabled(true);
   // menuStore->setEnabled(true);
    
    this->removeChild(bgLoading, true);
    this->removeChild(menuConfirmBuy, true);
    this->removeChild(menuBuy, true);
    this->removeChild(labelConfirmBuy, true);
}

void Achievements::onExit()
{
    this->removeAllChildrenWithCleanup(true);
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("achiev_texture.plist");
}

