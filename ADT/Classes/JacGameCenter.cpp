#include "JacGameCenter.h"
#include <cstdio>
#include "HttpClient.h"
#include "HttpRequest.h"
#include "HttpResponse.h"
#include "const.h"

#define kAnimationTime 2.0

using namespace cocos2d;
static vector<string> DataArray;

JacGameCenter::JacGameCenter()
{
}

void JacGameCenter::handleFb(const char *uid, const char *name, const char *surname, const char *email, const char *gender, const char* locale, const char* accessToken)
{
    CCLog("handleFb %s (%s %s)", uid, name, surname);
    
    cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();
    //request->setUrl("http://www.smallthinggame.com/~zapp/loadUserFb.php");
    string server = (string)SERVER_BASE_URL+(string)SERVER_BASE_CMD;
    request->setUrl(server.c_str());
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPost);
    request->setResponseCallback(CCDirector::sharedDirector()->getRunningScene(), callfuncND_selector(JacGameCenter::onHttpRequestCompleted));
    
    char data[500];
    
    
    //string n = (string)name+(string)surname;
    //sprintf(name, "username=%s&password=%s", l->getString(), l2->getString());
    //sprintf(data, "uid=%s&name=%s&surname=%s&email=%s&gender=%s&locale=%s", uid, name, surname, email, gender, locale);
    sprintf(data, "cmd=login_user&social_type=fb&app_id_game=%s&uid=%s&name=%s&surname=%s&email=%s&gender=%s&locale=%s&token=%s", FB_APP_KEY, uid, name, surname, email, gender, locale, accessToken);
    const char* postData = data;
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("PHP");
    CCLog(postData);
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    request->release();
}

void JacGameCenter::saveGameSession(string score)
{
    cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();
    string server = (string)SERVER_BASE_URL+(string)SERVER_BASE_CMD;
    request->setUrl(server.c_str());
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPost);
    request->setResponseCallback(CCDirector::sharedDirector()->getRunningScene(), callfuncND_selector(JacGameCenter::onHttpRequestCompleted));
    
    char data[500];

    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    int idUser = userDefault->getIntegerForKey("id_user");
    string sessionId = userDefault->getStringForKey("session_id");
    
    
    //sprintf(name, "username=%s&password=%s", l->getString(), l2->getString());
    //sprintf(data, "uid=%s&name=%s&surname=%s&email=%s&gender=%s&locale=%s", uid, name, surname, email, gender, local e);
    sprintf(data, "cmd=save_game_session&app_id_game=%s&id_user=%i&session_id=%s&score=%s", FB_APP_KEY, idUser, sessionId.c_str(), score.c_str());
    const char* postData = data;
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("SAVE");
    CCLog(postData);
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    request->release();
    
}

void JacGameCenter::getGameSession()
{
    cocos2d::extension::CCHttpRequest* request = new cocos2d::extension::CCHttpRequest();
    string server = (string)SERVER_BASE_URL+(string)SERVER_BASE_CMD;
    request->setUrl(server.c_str());
    request->setRequestType(cocos2d::extension::CCHttpRequest::kHttpPost);
    request->setResponseCallback(CCDirector::sharedDirector()->getRunningScene(), callfuncND_selector(JacGameCenter::onHttpRequestCompleted));
    
    char data[500];
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    int idUser = userDefault->getIntegerForKey("id_user");
    string sessionId = userDefault->getStringForKey("session_id");
    
    //sprintf(name, "username=%s&password=%s", l->getString(), l2->getString());
    //sprintf(data, "uid=%s&name=%s&surname=%s&email=%s&gender=%s&locale=%s", uid, name, surname, email, gender, locale);
    sprintf(data, "cmd=get_game_session&app_id_game=%s&id_user=%i&session_id=%s&type=friends", FB_APP_KEY, idUser, sessionId.c_str());
    const char* postData = data;
    request->setRequestData(postData, strlen(postData));
    
    request->setTag("CHART");
    CCLog(postData);
    cocos2d::extension::CCHttpClient::getInstance()->send(request);
    request->release();
    
}

void JacGameCenter::onHttpRequestCompleted(cocos2d::CCNode *sender, void *data)
{
    cocos2d::extension::CCHttpResponse *response = (cocos2d::extension::CCHttpResponse*)data;
    
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLog("%s completed", response->getHttpRequest()->getTag());
    }
    
    std::string lang;
    ccLanguageType l = CCApplication::sharedApplication()->getCurrentLanguage();
    if(l==kLanguageItalian)
        lang="it";
    else
        lang="en";
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    CCLog("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLog("response failed");
        CCLog("error buffer: %s", response->getErrorBuffer());
        
        //this->removeLoading(true);
        //this->showError(CCLocalizedString("t1035", "errore rete"));
        
        return;
    }
    
    // store data
    std::vector<char> *buffer = response->getResponseData();
    char* concatenated = (char*) malloc(buffer->size() + 1);
    std::string s2(buffer->begin(), buffer->end());
    strcpy(concatenated, s2.c_str());
    
    CCLog("===");
    CCLog(concatenated);
    
    rapidjson::Document d;
    d.Parse<0>(concatenated);
    
    int sizeJson = 0;
    
    for(rapidjson::Value::ConstMemberIterator itr = d.MemberBegin(); itr!=d.MemberEnd(); ++itr)
    {
        sizeJson++;
    }
    
    if(!strcmp(response->getHttpRequest()->getTag(), "PHP"))
    {
        std::string resultMatch;
        const char* resultMatchC;
        resultMatchC = d["result"].GetString();
        
        //std :: string str;
        resultMatch = "";
        resultMatch.append (resultMatchC);
        
        if(!strcmp(resultMatch.c_str(), "ok"))
        {
            //this->reloadMatches();
            
            CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
            string strIdUser = d["id_user"].GetString();
            int idU = atoi(strIdUser.c_str());
            string sessionId = d["session_id"].GetString();
            
            userDefault->setIntegerForKey("id_user", idU);
            userDefault->setStringForKey("session_id", sessionId);
            userDefault->flush();
       //aza     getGameSession();
        }
        else
        {
            //this->showError(CCLocalizedString("t1036", "errore cancellazione"));
            
            //pMainMenu->setEnabled(true);
        }
    }
    else if(!strcmp(response->getHttpRequest()->getTag(), "SAVE"))
    {
        std::string resultMatch;
        const char* resultMatchC;
        resultMatchC = d["result"].GetString();
        
        //std :: string str;
        resultMatch = "";
        resultMatch.append (resultMatchC);
        
        if(!strcmp(resultMatch.c_str(), "ok"))
        {
            CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
            userDefault->setStringForKey("METER_MUST_UPLOAD", "");
            userDefault->flush();
            //this->reloadMatches();
            CCLog("Punteggio salvato");
        }
        else
        {
            CCLog("Punteggio NON salvato");
        }
    }
    else if(!strcmp(response->getHttpRequest()->getTag(), "CHART"))
    {
        DataArray.clear();
        rapidjson::Value &v = d["game_sessions"];
        if (v.IsArray() && v.Size() != 0)
        {
            for (rapidjson::SizeType i = 0; i < v.Size(); i++)
            {
                DataArray.push_back(v[i]["id_user"].GetString());
                DataArray.push_back(v[i]["score"].GetString());
            }
        }
        CCNotificationCenter::sharedNotificationCenter()->postNotification("HTTPGET", (CCObject*)1);
    }
    
}

vector<string> JacGameCenter::GetDoc()
{
    return DataArray;
}
