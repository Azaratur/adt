#ifndef __OPTIONS_SCENE_H__
#define __OPTIONS_SCENE_H__

#include "cocos2d.h"

using namespace cocos2d;
using namespace std;

class OptionsScene : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(OptionsScene);
    
    CCMenuItemSprite *audioOnItem;
    CCMenuItemSprite *langOnItem;
    CCMenuItemSprite *audioOffItem;
    CCLabelBMFont *labelTitle;
    void onExit();
    
    void handleSound(CCObject *pSender);
    void switchLang(CCObject *pSender);
    
    void backToMenu(CCObject* pSender);
};

#endif