//
//  LogoScene.h
//  ADT
//
//  Created by Johnny Picciafuochi on 16/04/2014.
//
//

#ifndef __ADT__LogoScene__
#define __ADT__LogoScene__

#include <iostream>
#include "cocos2d.h"
#include "common.h"

using namespace cocos2d;


USING_NS_CC;

class LogoScene : public cocos2d::CCLayer
{
    public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    void GoToGame();
    
    void onExit();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(LogoScene);
};

#endif /* defined(__ADT__LogoScene__) */


