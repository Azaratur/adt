#ifndef __JACGAME_SCENE_H__
#define __JACGAME_SCENE_H__

#include "cocos2d.h"
#include "json/document.h"		// rapidjson's DOM-style API
#include "json/prettywriter.h"	// for stringify JSON
#include "json/filestream.h"	// wrapper of C stream for prettywriter as output

using namespace cocos2d;
using namespace std;
USING_NS_CC;
class JacGameCenter
{
public:
    JacGameCenter();
    static void handleFb(const char *uid, const char *name, const char *surname, const char *email, const char *gender, const char* locale, const char* accessToken);
    
    static void saveGameSession(string score);
    static void getGameSession();
    void onHttpRequestCompleted(cocos2d::CCNode *sender, void *data);
    static vector<string> GetDoc();
};

#endif
