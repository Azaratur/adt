/****************************************************************************
 Copyright (c) 2012-2013 Smallthing
 Author: Stefano Campodall'Orto
 
 http://www.smallthinggame.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

//
//  common.h
//  Coco2d-AdMob
//
//
//

#ifndef common_h
#define common_h

USING_NS_CC;

#define CREATE_FUNC_PARAMS(__TYPE__) \
static __TYPE__* create(int param) \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->init(param)) \
{ \
pRet->autorelease(); \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}

#define CREATE_FUNC_BOOL(__TYPE__) \
static __TYPE__* create(bool param) \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->init(param)) \
{ \
pRet->autorelease(); \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}

#define CREATE_FUNC_NEW_MATCH(__TYPE__) \
static __TYPE__* create(int param, bool param2) \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->init(param, param2)) \
{ \
pRet->autorelease(); \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}

#define CREATE_FUNC_GAME(__TYPE__) \
static __TYPE__* create(int param1, int param2, int param3, int param4, std::string param5, cocos2d::CCSprite *spr1, cocos2d::CCSprite *spr2, std::string param6) \
{ \
__TYPE__ *pRet = new __TYPE__(); \
if (pRet && pRet->init(param1, param2, param3, param4, param5, spr1, spr2, param6)) \
{ \
pRet->autorelease(); \
return pRet; \
} \
else \
{ \
delete pRet; \
pRet = NULL; \
return NULL; \
} \
}


#endif
