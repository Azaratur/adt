//
//  OptionsScene.cpp
//  AdventureTime
//
//  Created by Artificium on 26/09/13.
//
//

#include "OptionsScene.h"
#include "MenuScene.h"
#include "SimpleAudioEngine.h"

CCScene* OptionsScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    OptionsScene *layer = OptionsScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool OptionsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        
        return false;
    }
    // ask director the window size
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite* bg1 = CCSprite::create();
    int w = size.width;
    int h = size.height;
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    _raw[0] = 255;
    _raw[1] = 255;
    _raw[2] = 255;
    _raw[3] = 255;
    
    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg1->initWithTexture(tex, CCRectMake(0, 0, w, h));
    bg1->setPosition(ccp(size.width/2, size.height/2));
    ccColor3B color;
    color.r = 70;
    color.g = 171;
    color.b = 253;
    
    bg1->setColor(color);
    this->addChild(bg1,0);
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("options_texture.plist", "options_texture.png");
    
    CCSprite *bg = CCSprite::createWithSpriteFrameName("Set_Options.jpg");
    bg->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(bg, 1);
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    bool statusAudio = userDefault->getBoolForKey("audio_disable");
    
    CCLog("audio_disable sopra %i", statusAudio);
    
    // create and initialize a label
    CCSprite *sprBack = CCSprite::createWithSpriteFrameName("Back.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(sprBack, sprBack, this, menu_selector(OptionsScene::backToMenu));
    backItem->setPosition( ccp(backItem->getContentSize().width, size.height-backItem->getContentSize().height) );
    
    CCSprite* sprAudio = CCSprite::createWithSpriteFrameName("Audio.png");
    audioOnItem = CCMenuItemSprite::create(sprAudio, sprAudio, this, menu_selector(OptionsScene::handleSound));
    audioOnItem->setPosition( ccp(size.width/2, size.height/2) );
    
    CCSprite* sprAudioOff = CCSprite::createWithSpriteFrameName("Off.png");
    audioOffItem = CCMenuItemSprite::create(sprAudioOff, sprAudioOff, this, menu_selector(OptionsScene::handleSound));
    audioOffItem->setPosition( ccp(size.width/2, size.height/2) );
    
    if(!statusAudio)
    {
        audioOffItem->setEnabled(false);
        audioOffItem->setVisible(false);
    }
    
    /*CCLabelTTF* audioLabel = CCLabelTTF::create("AUDIO:", "Thonburi", 48);
    audioLabel->setPosition( ccp(size.width/2-100, size.height/2) );
    this->addChild(audioLabel, 2);
    
    CCLabelTTF* onLabel = CCLabelTTF::create("ON", "Thonburi", 48);
    CCMenuItemLabel *onItem = CCMenuItemLabel::create(onLabel, this, NULL);
    
    CCLabelTTF* offLabel = CCLabelTTF::create("OFF", "Thonburi", 48);
    CCMenuItemLabel *offItem = CCMenuItemLabel::create(offLabel, this, NULL);
    
    CCMenuItemToggle *item = CCMenuItemToggle::createWithTarget(this, menu_selector(OptionsScene::handleSound), onItem, offItem, NULL);
    item->setPosition( ccp(size.width/2, size.height/2) );
    
    if(status==1)
        item->setSelectedIndex(1);*/
    
    // create menu, it's an autorelease object
    CCMenu *pMenu = CCMenu::create(backItem, audioOnItem, audioOffItem, NULL);
    pMenu->setPosition( CCPointZero );
    this->addChild(pMenu, 1);
    
    return true;
}

void OptionsScene::handleSound(CCObject *pSender)
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    bool statusAudio = userDefault->getBoolForKey("audio_disable");
    
    CCLog("audio_disable %i", statusAudio);
    
    if(!statusAudio)
    {
        userDefault->setBoolForKey("audio_disable", true);
        audioOffItem->setEnabled(true);
        audioOffItem->setVisible(true);
        
        audioOnItem->setEnabled(false);
        
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
    }
    else
    {
        userDefault->setBoolForKey("audio_disable", false);
        
        audioOffItem->setEnabled(false);
        audioOffItem->setVisible(false);
        
        audioOnItem->setEnabled(true);
        
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("POL-two-fat-gangsters-short.mp3", true);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0.5f);
    }
    
    userDefault->flush();
}

void OptionsScene::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void OptionsScene::onExit()
{
    this->removeAllChildrenWithCleanup(true);
}