//
//  LogoScene.cpp
//  ADT
//
//  Created by Johnny Picciafuochi on 16/04/2014.
//
//

#include "LogoScene.h"
#include "IntroScene.h"
#include "MenuScene.h"

CCScene* LogoScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    LogoScene *layer = LogoScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool LogoScene::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite* bg = CCSprite::create();
    int w = size.width;
    int h = size.height;
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    _raw[0] = 255;
    _raw[1] = 255;
    _raw[2] = 255;
    _raw[3] = 255;
    
    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg->initWithTexture(tex, CCRectMake(0, 0, w, h));
    bg->setPosition(ccp(size.width/2, size.height/2));
    ccColor3B color;
    color.r = 255;
    color.g = 255;
    color.b = 255;
    bg->setColor(color);
    this->addChild(bg,0);
    CCSprite *sprite = CCSprite::create("cnlogo.png");
    sprite->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(sprite,0);
    
    CCSprite* bg1 = CCSprite::create();
    CCTexture2D *tex1 = new CCTexture2D();
    char _raw1[4];
    _raw1[0] = 255;
    _raw1[1] = 255;
    _raw1[2] = 255;
    _raw1[3] = 255;
    
    tex1->initWithData(_raw1, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg1->initWithTexture(tex1, CCRectMake(0, 0, w, h));
    bg1->setPosition(ccp(size.width/2, size.height/2));
    ccColor3B color1;
    color1.r = 0;
    color1.g = 0;
    color1.b = 0;
    bg1->setColor(color1);
    this->addChild(bg1,0);
    bg1->setVisible(false);
    CCSprite *sprite1 = CCSprite::create("gf_logo.png");
    sprite1->setPosition(ccp(size.width/2, size.height/2));
    sprite1->setVisible(false);
    this->addChild(sprite1,0);
    
    
    CCDelayTime *delayFade = CCDelayTime::create(1.5);
    CCDelayTime *delayFade1 = CCDelayTime::create(1.0);
    CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(LogoScene::GoToGame));
    CCShow *show = CCShow::create();
    bg1->runAction(CCSequence::create(delayFade, show, NULL));
    sprite1->runAction(CCSequence::create(delayFade, show, delayFade1, call, NULL));
    return true;
}

void LogoScene::GoToGame()
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    
    bool skipIntro = userDefault->getBoolForKey("skip_intro");
    CCScene *pScene;
    
    if(!skipIntro)
    {
        pScene = IntroScene::scene();
    }
    else
    {
        pScene = MenuScene::scene();
    }
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->replaceScene(pScene);
}

void LogoScene::onExit()
{
    this->removeAllChildrenWithCleanup(true);
}