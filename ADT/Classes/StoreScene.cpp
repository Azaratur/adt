#include "StoreScene.h"
#include "MenuScene.h"

#include "CCSlidingLayer.h"

using namespace cocos2d;

static StoreScene * myScene = NULL;

CCScene* StoreScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    StoreScene *layer = StoreScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool StoreScene::init()
{

    if ( !CCLayer::init() )
    {
        
        return false;
    }
    
    myScene = this;
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("shop_texture.plist", "shop_texture.png");
    
    CCSprite *bg = CCSprite::createWithSpriteFrameName("Set_Shop.jpg");
    
    if (bg->getContentSize().width<size.width)
        bg->setScaleX(1.5);
    bg->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(bg, 1);
    
    CCSprite *bgTitle = CCSprite::createWithSpriteFrameName("TitleSkin.png");
    bgTitle->setPosition(ccp(size.width/2, size.height-30-(bgTitle->getContentSize().height/2)));
    this->addChild(bgTitle, 2);
    
    CCLabelBMFont *labelTitle = CCLabelBMFont::create(CCLocalizedString("t1008", "dito qui"), "shop_font.fnt", 1024, kCCTextAlignmentCenter);
    labelTitle->setPosition(ccp(bgTitle->getContentSize().width/2, bgTitle->getContentSize().height/2));
    bgTitle->addChild(labelTitle, 5);
    
    CCSprite *bgGems = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    bgGems->setPosition(ccp(size.width-(bgGems->getContentSize().width/2)-20, 50));
    this->addChild(bgGems, 2);
    
    CCSprite *boxGems = CCSprite::createWithSpriteFrameName("Gui_Gems.png");
    boxGems->setPosition(ccp(size.width-(bgGems->getContentSize().width/2)-110, 45));
    this->addChild(boxGems, 2);
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    money = userDefault->getIntegerForKey("total_gems");
    
    CCLog("Sacchi %i", money);

    char c_gems[10];
    sprintf(c_gems, "%i", money);
    
    labelMoney = CCLabelBMFont::create(c_gems, "stats_font.fnt", 80, kCCTextAlignmentCenter);
    labelMoney->setPosition(ccp(bgGems->getPositionX()+30, bgGems->getPositionY()-5));
    this->addChild(labelMoney, 5);
    bool shield = userDefault->getBoolForKey("SHIELD_UNLOCKED");
    bool wings = userDefault->getBoolForKey("WINGS_UNLOCKED");
    //Istanzio l'array degli elementi
    if (wings)
    {
        structStore s1;
        s1.spr = CCSprite::createWithSpriteFrameName("Buy_Wings.png");
        s1.name = CCLocalizedString("t1023", "dito qui");
        s1.money = 5000;
        arrayStore.push_back(s1);
    }

   
    structStore s2;
    s2.spr = CCSprite::createWithSpriteFrameName("Buy_Stabilizer.png");
    s2.name = CCLocalizedString("t1024", "dito qui");
    s2.money = 5000;
    
    if (shield)
    {
        structStore s3;
        s3.spr = CCSprite::createWithSpriteFrameName("Buy_Shield.png");
        s3.name = CCLocalizedString("t1025", "dito qui");
        s3.money = 5000;
        arrayStore.push_back(s3);
    }
    
    structStore s4;
    s4.spr = CCSprite::createWithSpriteFrameName("Buy_AllBonus.png");
    s4.name = CCLocalizedString("t1026", "dito qui");
    s4.money = 10000;
    
    arrayStore.push_back(s2);
    arrayStore.push_back(s4);
     

    layerItems = CCLayer::create();
    layerItems->setContentSize( CCSizeMake( 500*arrayStore.size(), 400));
    
    CCSprite *btnExit = CCSprite::createWithSpriteFrameName("Button_EXIT.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(btnExit, btnExit, this, menu_selector(StoreScene::backToMenu));
    backItem->setPosition( ccp(90, 90) );
       
    
    for(int j=0; j<arrayStore.size(); j++)
    {
        arrayStore[j].bgSpr = CCSprite::createWithSpriteFrameName("Buy_Skin.png");
        arrayStore[j].bgSpr->addChild(arrayStore[j].spr);
        arrayStore[j].spr->setPosition(ccp(arrayStore[j].bgSpr->getContentSize().width/2, 90+(arrayStore[j].bgSpr->getContentSize().height/2)));
        CCLabelBMFont *labelTitle = CCLabelBMFont::create(arrayStore[j].name.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
        labelTitle->setPosition(ccp(arrayStore[j].bgSpr->getContentSize().width/2, 60+(arrayStore[j].bgSpr->getContentSize().height/2)-arrayStore[j].spr->getContentSize().height/2));
        arrayStore[j].bgSpr->addChild(labelTitle);
        std::stringstream out;
        out << arrayStore[j].money;
        std::string converted = out.str();
        labelTitle = CCLabelBMFont::create(converted.c_str(), "stats_font.fnt", 800, kCCTextAlignmentCenter);
        labelTitle->setPosition(ccp(50+arrayStore[j].bgSpr->getContentSize().width/2, -20+(arrayStore[j].bgSpr->getContentSize().height/2)-arrayStore[j].spr->getContentSize().height/2));
        arrayStore[j].bgSpr->addChild(labelTitle);
        arrayStore[j].bgSpr->setPosition(ccp(150+(500*j), 0));
        layerItems->addChild(arrayStore[j].bgSpr);
    }
    
  
    // create menu, it's an autorelease object
    menuBack = CCMenu::create(backItem, NULL);
    menuBack->setPosition( CCPointZero );
    this->addChild(menuBack, 2);
    
    /*menuStore = CCMenu::createWithArray(arrayItemsMenu);
    menuStore->setPosition( CCPointZero );
    layer->addChild(menuStore, 3);*/
    //CCLog("Dimensioni %f-%f", layer->getContentSize().width, layer->getContentSize().height);
    CCSlidingLayer *sl = CCSlidingLayer::create(Horizontally, CCSizeMake(layerItems->getContentSize().width, layerItems->getContentSize().height/2), CCRectMake(0, 0, layerItems->getContentSize().width, layerItems->getContentSize().height*1.5), ccc4(0,0,0,0), "match", true);
    
    sl->addChildWithSize(layerItems, layerItems->getContentSize(), kAlignmentLeft);
    sl->setColor(ccRED);
    this->addChild(sl, 10);
    
    return true;
}

void StoreScene::checkPointRelease(int x, int y)
{
    StoreScene::isTouchOnSprite(CCPointMake(x,y));
}

int StoreScene::isTouchOnSprite(CCPoint touch)
{
    int touched = -1;
    
    for(int i = 0; i < myScene->arrayStore.size(); ++i)
    {
        CCRect c = CCRect(myScene->arrayStore[i].bgSpr->getPositionX() -(myScene->arrayStore[i].bgSpr->getContentSize().width/2*myScene->arrayStore[i].bgSpr->getScaleX()), myScene->arrayStore[i].bgSpr->getPositionY(), (myScene->arrayStore[i].bgSpr->getContentSize().width*myScene->arrayStore[i].bgSpr->getScaleX()), (myScene->arrayStore[i].bgSpr->getContentSize().height*myScene->arrayStore[i].bgSpr->getScaleY()));
        if(c.containsPoint(touch))
        {
            touched = i;
            
        }
    }
    if (touched >= 0 && myScene->money>=myScene->arrayStore[touched].money)
        myScene->requestConfirmBuy(touched);
    
    CCLOG("Sto toccando lo spicchio %i", touched);
    
    return touched;
}

void StoreScene::backToMenu(CCObject* pSender)
{
    CCScene *pScene = MenuScene::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
}

void StoreScene::requestConfirmBuy(int index)
{
    menuBack->setEnabled(false);
    //menuStore->setEnabled(false);
    
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    
    bgLoading = CCSprite::create();
    bgLoading->setTextureRect(CCRectMake(0, 0, size.width, size.height));
    bgLoading->setPosition(ccp(size.width/2, size.height/2));
    bgLoading->setColor(ccBLACK);
    bgLoading->setOpacity(200);
    this->addChild(bgLoading, 100);

    CCSprite *bgTitle = CCSprite::createWithSpriteFrameName("TitleSkin.png");
    bgTitle->setPosition(ccp(size.width/2, size.height-50-(bgTitle->getContentSize().height/2)));
    bgLoading->addChild(bgTitle, 102);
    
    CCLabelBMFont *labelTitle = CCLabelBMFont::create(CCLocalizedString("t1022", "dito qui"), "stats_font.fnt", 800, kCCTextAlignmentCenter);
    labelTitle->setPosition(ccp(bgTitle->getContentSize().width/2, bgTitle->getContentSize().height/2));
    bgTitle->addChild(labelTitle, 5);

    //CCLog("Richiesta %s", arrayStore[tmp->getTag()].name.c_str());
    
    /*CCLabelTTF* yesLabel = CCLabelTTF::create("Yes", "Thonburi", 48);
    CCMenuItemLabel *yesItem = CCMenuItemLabel::create(yesLabel, this, menu_selector(StoreScene::buyElement));
    yesItem->setTag(index);
    yesItem->setPosition( ccp(size.width / 2-50, labelConfirmBuy->getPositionY()-100) );*/
    
    
    CCSprite *btnYes = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    CCLabelBMFont *yesLabel = CCLabelBMFont::create("Yes", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    yesLabel->setPosition(ccp(btnYes->getContentSize().width/2, btnYes->getContentSize().height/2-11));
    btnYes->addChild(yesLabel);
    CCMenuItemLabel *yesItem = CCMenuItemLabel::create(btnYes, this, menu_selector(StoreScene::buyElement));
    yesItem->setTag(index);
    yesItem->setPosition( ccp(300, size.height/2) );
    
    CCSprite *btnNo = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    CCLabelBMFont *noLabel = CCLabelBMFont::create("No", "menu_font.fnt", 80, kCCTextAlignmentCenter);
    noLabel->setPosition(ccp(btnNo->getContentSize().width/2, btnNo->getContentSize().height/2-11));
    btnNo->addChild(noLabel);
    CCMenuItemLabel *noItem = CCMenuItemLabel::create(btnNo, this, menu_selector(StoreScene::rejectElement));
    noItem->setTag(index);
    noItem->setPosition( ccp(size.width -300, size.height/2) );
    
    menuBuy = CCMenu::create(yesItem, noItem, NULL);
    menuBuy->setPosition( CCPointZero );
    this->addChild(menuBuy, 102);
}

void StoreScene::buyElement(cocos2d::CCObject *pSender)
{
    CCLabelTTF *tmp = (CCLabelTTF*)pSender;
    
    CCLog("Elemento comprato");
    money -= arrayStore[tmp->getTag()].money;
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    userDefault->setIntegerForKey("total_gems", money);
    
    if (tmp->getTag() == 0)
        userDefault->setBoolForKey("Wings", true);
    else if (tmp->getTag() == 1)
        userDefault->setBoolForKey("Stabilizer", true);
    else if (tmp->getTag() == 2)
        userDefault->setBoolForKey("Shield", true);
    else if (tmp->getTag() == 3)
        userDefault->setBoolForKey("Allbonus", true);
    userDefault->flush();
    
    char m[10];
    sprintf(m, "%i", money);
    labelMoney->setString(m);

    menuBack->setEnabled(true);
   // menuStore->setEnabled(true);
    
    this->removeChild(bgLoading, true);
    this->removeChild(menuConfirmBuy, true);
    this->removeChild(menuBuy, true);
    this->removeChild(labelConfirmBuy, true);
}

void StoreScene::rejectElement(cocos2d::CCObject *pSender)
{
    menuBack->setEnabled(true);
   // menuStore->setEnabled(true);
    
    this->removeChild(bgLoading, true);
    this->removeChild(menuConfirmBuy, true);
    this->removeChild(menuBuy, true);
    this->removeChild(labelConfirmBuy, true);
}

void StoreScene::onExit()
{
    this->removeAllChildrenWithCleanup(true);
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("shop_texture.plist");
}

