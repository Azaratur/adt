#include "MainScene.h"
#include "SimpleAudioEngine.h"
#include "MenuScene.h"
#include "Achievements.h"
#include <ctime>

#define kBonusProbability 30

using namespace cocos2d;
using namespace CocosDenshion;
static MainScene* main;

CCScene* MainScene::scene()
{
    CCScene *scene = CCScene::create();
    MainScene *layer = MainScene::create();
    layer->isKeypadEnabled();
    scene->addChild(layer);
    return scene;
}

bool MainScene::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    animDead = false;
    currentDiff = 10;
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("world_texture.plist", "world_texture.png");
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("world_texture2.plist", "world_texture2.png");
    main = this;
    this->setKeypadEnabled(true);
    ChangeNow = 0;
    changelvl = 0;
    RemovingAll = false;
    ForcePause = false;
    MoveLevel = false;
    SkipLevel = false;
    bouncing = false;
    FallSound = false;
    AnimationUp = false;
    AnimationFallY = 0;
    AnimationFallX = 0;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    improvingSpeed = 0;
    ScreenX = 0;
    isAlreadyDead = false;

    ItemNode = CCSpriteBatchNode::create("main.png", 10000);
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("main.plist");
    CCSprite *bgMeters = CCSprite::createWithSpriteFrameName("Skin_Distance.png");
    bgMeters->setPosition(ccp(bgMeters->getContentSize().width/2+200, winSize.height-50));
    ItemNode->addChild(bgMeters, 2);
    
    CCSprite *bgGems = CCSprite::createWithSpriteFrameName("Skin_Gems.png");
    bgGems->setPosition(ccp(winSize.width-(bgGems->getContentSize().width/2)-200, winSize.height-50));
    ItemNode->addChild(bgGems, 2);
    
    CCSprite *boxGems = CCSprite::createWithSpriteFrameName("Gui_Gems.png");
    boxGems->setPosition(ccp(winSize.width-(bgGems->getContentSize().width/2)-110, winSize.height-45));
    ItemNode->addChild(boxGems, 2);
    
    CCSprite *sprPause = CCSprite::createWithSpriteFrameName("Pause.png");
    CCMenuItemSprite *itemPause = CCMenuItemSprite::create(sprPause, sprPause, this, menu_selector(MainScene::pauseGame));
    itemPause->setPosition(ccp(winSize.width/2, winSize.height-50));
    
    pMainPauseMenu= CCMenu::create(itemPause, NULL);
    pMainPauseMenu->setPosition(CCPointZero);
    this->addChild(pMainPauseMenu, 5);
    
   // titleMetersLabel = CCLabelBMFont::create("m", "stats_font.fnt", 80, kCCTextAlignmentCenter);
    //titleMetersLabel->setPosition(ccp(bgMeters->getPositionX()+110, bgMeters->getPositionY()-5));
   // this->addChild(titleMetersLabel, 2);
    
    metersLabel = CCLabelBMFont::create("100000", "stats_font.fnt", 80, kCCTextAlignmentCenter);
    metersLabel->setPosition(ccp(bgMeters->getPositionX()-20, bgMeters->getPositionY()-5));
    this->addChild(metersLabel, 5);
    
    gemsLabel = CCLabelBMFont::create("1002", "stats_font.fnt", 80, kCCTextAlignmentCenter);
    gemsLabel->setPosition(ccp(bgGems->getPositionX()-20, bgGems->getPositionY()-5));
    this->addChild(gemsLabel, 5);
    
    CardCreated = false;
    nextGems = 0;
    
    this->addChild(ItemNode, 0);
   // this->addChild(CCSpriteBatchNode::create("world_texture.png", 10000));
    gems = new Gems();
    _backgroundNode = CCParallaxNodeExtras::node();
    this->addChild(_backgroundNode,-1);
    
    
    //Istanzio i bonus
    //TODO: aggiungere le sprite
    bonusStruct b;
    b.index = 0;
    b.desc = "Life";
    b.sprName = "Card_Reborn.png";
    bonusArray.push_back(b);

    b.index = 1;
    b.desc = "Shield";
    b.sprName = "Card_Shield.png";
    bonusArray.push_back(b);
    
    b.index = 2;
    b.desc = "Stabilizer";
    b.sprName = "Card_Stabilizer.png";
    bonusArray.push_back(b);

    b.index = 3;
    b.desc = "+ 500";
    b.sprName = "Card_RedGem.png";
    bonusArray.push_back(b);
    
    b.index = 4;
    b.desc = "+50";
    b.sprName = "Card_YellowGem.png";
    bonusArray.push_back(b);
    
    b.index = 5;
    b.desc = "Wings";
    b.sprName = "Card_Wings.png";
    bonusArray.push_back(b);
    
    b.index = 6;
    b.desc = "All";
    b.sprName = "Card_Allbonus.png";
    bonusArray.push_back(b);
    
    BonusBg = CCSprite::createWithSpriteFrameName("Set_Bonus.jpg");
    BonusBg->setVisible(false);
    BonusBg->cocos2d::CCNode::setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(BonusBg, 99);
    
    GameOverBg = CCSprite::createWithSpriteFrameName("Set_GameOver.jpg");
    GameOverBg->setVisible(false);
    GameOverBg->cocos2d::CCNode::setPosition(ccp(winSize.width/2, winSize.height/2));
    this->addChild(GameOverBg, 100);
    
    if (BonusBg->getContentSize().width<winSize.width)
        BonusBg->setScaleX(1.5);
    
    GreenScroll1 = CCSprite::createWithSpriteFrameName("World01_a.jpg");
    GreenScroll2 = CCSprite::createWithSpriteFrameName("World01_b.jpg");
    YellowScroll1 = CCSprite::createWithSpriteFrameName("World02_a.jpg");
    YellowScroll2 = CCSprite::createWithSpriteFrameName("World02_b.jpg");
    OrangeScroll1 = CCSprite::createWithSpriteFrameName("World03_a.jpg");
    OrangeScroll2 = CCSprite::createWithSpriteFrameName("World03_b.jpg");
    
    if (GreenScroll1->getContentSize().width<winSize.width)
    {
        GreenScroll1->setScaleX(1.5);
        GreenScroll1->setContentSize(ccp(GreenScroll1->getContentSize().width*1.5, GreenScroll1->getContentSize().height));
        GreenScroll2->setScaleX(1.5);
        GreenScroll2->setContentSize(ccp(GreenScroll2->getContentSize().width*1.5, GreenScroll2->getContentSize().height));
        
        YellowScroll1->setScaleX(1.5);
        YellowScroll1->setContentSize(ccp(YellowScroll1->getContentSize().width*1.5, YellowScroll1->getContentSize().height));
        YellowScroll2->setScaleX(1.5);
        YellowScroll2->setContentSize(ccp(YellowScroll2->getContentSize().width*1.5, YellowScroll2->getContentSize().height));
        
        OrangeScroll1->setScaleX(1.5);
        OrangeScroll1->setContentSize(ccp(OrangeScroll1->getContentSize().width*1.5, OrangeScroll1->getContentSize().height));
        OrangeScroll2->setScaleX(1.5);
        OrangeScroll2->setContentSize(ccp(OrangeScroll2->getContentSize().width*1.5, OrangeScroll2->getContentSize().height));
    }
    ChangeLevel = CCSprite::createWithSpriteFrameName("ChangeLevel.png");
    Current1 = GreenScroll1;
    Current2 = GreenScroll2;
    
    CCPoint dustSpeed = ccp(0.4, 0.4);
    CCPoint bgSpeed = ccp(0.4, 0.4);
    
    _pivot = CCSprite::create();
    
    _backgroundNode->addChild(Current1, -1, dustSpeed, ccp(GreenScroll1->getContentSize().width/2,winSize.height/2) ); // 2
    _backgroundNode->addChild(Current2, -1, dustSpeed, ccp( -GreenScroll1->getContentSize().width,winSize.height/2));
    _backgroundNode->addChild(YellowScroll1, -1, dustSpeed, ccp( -5000,winSize.height/2));
    _backgroundNode->addChild(YellowScroll2, -1, dustSpeed, ccp( -5000,winSize.height/2));
    _backgroundNode->addChild(OrangeScroll1, -1, dustSpeed, ccp( -5000,winSize.height/2));
    _backgroundNode->addChild(OrangeScroll2, -1, dustSpeed, ccp( -5000,winSize.height/2));
    
    _backgroundNode->addChild(ChangeLevel, -1, dustSpeed, ccp( -5000,winSize.height/2));
    _backgroundNode->addChild(_pivot, -1, bgSpeed, ccp(GreenScroll1->getContentSize().width/2, 0));
    CurrentBg = GreenScroll1;

    Block = new BlockType();
    this->scheduleUpdate();
    this->setTouchEnabled(true);
    bl = new BlockReader();
    mov = -1000;
    LastTimeMissile = GetMilliCount();
    LastTimeAttach = GetMilliCount();
    MissileNumber = 0;
    AttachNumber = 0;
    
    std::string str = "block";
    str=str+".txt";
    char *a=new char[str.size()+1];
    a[str.size()]=0;
    memcpy(a,str.c_str(),str.size());
    CCLog("CreateNormalLaser %s", a);
    bl->Init(a);
    vector <FrameADT> frame;
    frame.clear();
    SpriteList.clear();
    for (int i=0; i<bl->ContainerSprite.size(); i++)
    {
        BlockReader::structSpriteCollision* spr = bl->ContainerSprite.at(i);
        FrameADT* f = new FrameADT (spr);
        if (f->Name.find("_Frame") !=std::string::npos)
        {
            if (f->Name.find("_Frame1.png") !=std::string::npos)
            {
                unsigned pos = f->Name.find("_Frame1.png");
                std::string str3 = f->Name.substr (0,pos);
                Sprite* s = new Sprite (f->Name);
                s->addFrame(*f);
                vector<FrameADT>::iterator it = frame.begin();
                for ( ; it != frame.end(); )
                {
                    if (it->Name.find(str3) !=std::string::npos)
                    {
                        s->addFrame(*it);
                        ++it;
                    } else
                    {
                        ++it;
                    }
                }
                SpriteList.push_back(s);
            }
            else
            {
                unsigned pos = f->Name.find("_Frame");
                std::string str3 = f->Name.substr (0,pos)+"_Frame1.png";
                bool added = false;
                for (int i=0; i<SpriteList.size(); i++)
                {
                    Sprite* sp = SpriteList.at(i);
                    if (sp->Name.find(str3) !=std::string::npos)
                    {
                        sp->addFrame(*f);
                        added = true;
                        break;
                    }
                }
                if (!added)
                {
                    frame.push_back(*f);
                }
            }
        }
        else
        {
            Sprite* s = new Sprite (f->Name);
            s->addFrame(*f);
            SpriteList.push_back(s);
        }
    }
    for (int i=0; i<SpriteList.size(); i++)
        SpriteList.at(i)->Sort();
    Sprite* d;
    Sprite* u;
    Sprite* f;
    for (int i=0; i<SpriteList.size(); i++)
    {
        Sprite* s = SpriteList.at(i);
        if (s->Name.compare("Up_Frame1.png") == 0 )
            u = s;
        if (s->Name.compare("Dw_Frame1.png") == 0 )
            d = s;
        if (s->Name.compare("Fly_Frame1.png") == 0 )
            f = s;
    }
    player = new Player (f,u, d, ItemNode);
    player->setPosition(ccp(winSize.width * 0.2, winSize.height * 0.5));
    player->shadow->setPosition(ccp(player->getPosition().x-76, 40));
    
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    bool shield = userDefault->getBoolForKey("Shield");
    bool wings = userDefault->getBoolForKey("Wings");
    bool stabilizer = userDefault->getBoolForKey("Stabilizer");
    bool allbonus = userDefault->getBoolForKey("Allbonus");
    statusAudio = userDefault->getBoolForKey("audio_disable");
    
    CCArray *animFrames;
    CCSpriteFrame* sprf1;
    CCSpriteFrame* sprf2;
    CCAnimation *animation;
    CCAnimate *animate;
    if (allbonus)
    {
        player->Bombed = true;
        BonusType* bt1 = new BonusType(BonusType::ATOMIC, "");
        bt1->spr =CCSprite::createWithSpriteFrameName("OnBall_Atomic.png");
        player->node->addChild(bt1->spr, 3);
        bt1->rot = 90;
        bt1->spr->setPosition(player->ship->getPosition());
        player->spriteBonus.push_back(bt1);
        
        CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
        if (userDefault->getBoolForKey("SHIELD_UNLOCKED"))
        {
            player->StartShiled = player->GetMilliCount()+22000;
            player->Shielded = true;
            BonusType* bt2 = new BonusType(BonusType::SHIELD, "");
            bt2->spr =CCSprite::createWithSpriteFrameName("OnJake_Shield.png");
            player->node->addChild(bt2->spr, 3);
            bt2->spr->setPosition(player->ship->getPosition());
            player->spriteBonus.push_back(bt2);

        }
        
        if (userDefault->getBoolForKey("WINGS_UNLOCKED"))
        {
            player->Winged = true;
            BonusType* bt = new BonusType(BonusType::WINGS, "");
            bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Wing_Frame1.png");
            player->node->addChild(bt->spr, 3);
            bt->spr->setPosition(player->ship->getPosition());
            player->spriteBonus.push_back(bt);
            animFrames = CCArray::create();
            sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame2.png");
            animFrames->addObject(sprf1);
            sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame1.png");
            animFrames->addObject(sprf2);
            animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
            animate = CCAnimate::create(animation);
            bt->spr->runAction(CCRepeatForever::create(animate));
        }
    
        player->Stabilized = true;
        BonusType* bt3 = new BonusType(BonusType::STABILIZER, "");
        bt3->spr =CCSprite::createWithSpriteFrameName("OnJake_Stabilizer_Frame1.png");
        player->node->addChild(bt3->spr, 3);
        bt3->spr->setPosition(player->ship->getPosition());
        bt3->rot = 0;
        player->spriteBonus.push_back(bt3);
        animFrames = CCArray::create();
        sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame2.png");
        sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame1.png");
        animFrames->addObject(sprf1);
        animFrames->addObject(sprf2);
        animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
        animate = CCAnimate::create(animation);
        bt3->spr->runAction(CCRepeatForever::create(animate));
    }
    else
    {
        if (shield)
        {
            player->StartShiled = player->GetMilliCount()+22000;
            player->Shielded = true;
            BonusType* bt2 = new BonusType(BonusType::SHIELD, "");
            bt2->spr =CCSprite::createWithSpriteFrameName("OnJake_Shield.png");
            player->node->addChild(bt2->spr, 3);
            bt2->spr->setPosition(player->ship->getPosition());
            player->spriteBonus.push_back(bt2);
        }
        if (stabilizer)
        {
            player->Stabilized = true;
            BonusType* bt3 = new BonusType(BonusType::STABILIZER, "");
            bt3->spr =CCSprite::createWithSpriteFrameName("OnJake_Stabilizer_Frame1.png");
            player->node->addChild(bt3->spr, 3);
            bt3->spr->setPosition(player->ship->getPosition());
            bt3->rot = 0;
            player->spriteBonus.push_back(bt3);
            animFrames = CCArray::create();
            sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame2.png");
            sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Stabilizer_Frame1.png");
            animFrames->addObject(sprf1);
            animFrames->addObject(sprf2);
            animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
            animate = CCAnimate::create(animation);
            bt3->spr->runAction(CCRepeatForever::create(animate));
        }
        if (wings)
        {
            player->Winged = true;
            player->WingedPerc = 30;
            player->StartWing = GetMilliCount();
            BonusType* bt = new BonusType(BonusType::WINGS, "");
            bt->spr =CCSprite::createWithSpriteFrameName("OnJake_Wing_Frame1.png");
            player->node->addChild(bt->spr, 3);
            bt->spr->setPosition(player->ship->getPosition());
            player->spriteBonus.push_back(bt);
            animFrames = CCArray::create();
            sprf1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame2.png");
            animFrames->addObject(sprf1);
            sprf2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("OnJake_Wing_Frame1.png");
            animFrames->addObject(sprf2);
            animation = CCAnimation::createWithSpriteFrames(animFrames, 0.1f);
            animate = CCAnimate::create(animation);
            bt->spr->runAction(CCRepeatForever::create(animate));
        }
    }
    userDefault->setBoolForKey("Shield", false);
    userDefault->setBoolForKey("Wings", false);
    userDefault->setBoolForKey("Stabilizer", false);
    userDefault->setBoolForKey("Allbonus", false);
    userDefault->flush();
    
    return true;
}

void MainScene::PlayerIsDying()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    if(!animDead)
    {
        ScreenX = 0;
        AnimationFallY = 0;
        animDead = true;
        AnimationUp = true;
        this->finnDeathAnimFly();
        DeathY = finnDeath->getPositionY();
    }
    else
    {
        ScreenX+=5;
        if (ScreenX>150)
            ScreenX = 150;
        if (AnimationFallY<50 && AnimationUp)
        {
            AnimationFallY+=(50-AnimationFallY >= 30 ? 10 : 50-AnimationFallY >= 10 ? 5 : 2);
        }
        else if (AnimationFallY>=50 && AnimationUp)
        {
            AnimationUp = false;
        }
        else
            AnimationFallY-=(50-AnimationFallY >= 30 ? 10 : 50-AnimationFallY >= 10 ? 5 : 2);
        if (AnimationFallX<150)
            AnimationFallX+=(150-AnimationFallX >= 100 ? 9 : 150-AnimationFallX >= 50 ? 6 : 150-AnimationFallX >= 30 ? 3 : 150-AnimationFallY >= 10 ? 2 : 1);
        else
            AnimationFallX = 150;
        int downY = DeathY+AnimationFallY;
        if (downY<finnDeath->getContentSize().height/2)
        {
            downY = finnDeath->getContentSize().height/2;
            finnDeath1->setVisible(true);
            finnDeath1->setPosition(ccp( 50+ScreenX+AnimationFallX+finnDeath1->getContentSize().width/2, downY-45));
            finnDeath->setVisible(false);
            jackDeath1->setVisible(true);
            jackDeath1->setPosition(ccp( 50+ScreenX-AnimationFallX+jackDeath1->getContentSize().width/2, downY-10));
            jackDeath->setVisible(false);
            if (mov >= -100)
                bouncing = true;
            else
                bouncing = false;
            jackShadow->setPosition(ccp(50+ScreenX-AnimationFallX+jackDeath1->getContentSize().width/2, 40));
            finnShadow->setPosition(ccp(50+ScreenX+AnimationFallX+finnDeath1->getContentSize().width/2, 40));
            if (!FallSound)
            {
                if (!statusAudio)
                {
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("fall_bounce_1.mp3");
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("bones_bounce_1.mp3");
                }
                FallSound = true;
            }
        }
        else
        {
            jackShadow->setPosition(ccp(50+ScreenX-AnimationFallX+jackDeath->getContentSize().width/2, 40));
            finnShadow->setPosition(ccp(50+ScreenX+AnimationFallX+finnDeath->getContentSize().width/2, 40));
            finnDeath->setPosition(ccp( 50+ScreenX+AnimationFallX+finnDeath->getContentSize().width/2, downY));
            jackDeath->setPosition(ccp( 50+ScreenX-AnimationFallX+jackDeath->getContentSize().width/2, downY));
        }
        if(finnDeath->getPositionY()<=winSize.height/2)
        {
            finnShadow->setVisible(true);
            
            float scaleOrig = finnDeath->getPositionY()/(winSize.height/2);
            float scale = 1-scaleOrig+0.25;
            
            float opacity = 255*scale;
            
            finnShadow->setOpacity(opacity);
            finnShadow->setScale(scale);
        }
        else
        {
            finnShadow->setVisible(false);
        }
        if(jackDeath->getPositionY()<=winSize.height/2)
        {
            jackShadow->setVisible(true);
            
            float scaleOrig = jackDeath->getPositionY()/(winSize.height/2);
            float scale = 1-scaleOrig+0.25;
            
            float opacity = 255*scale;
            
            jackShadow->setOpacity(opacity);
            jackShadow->setScale(scale);
        }
        else
        {
            jackShadow->setVisible(false);
        }
    }
    
    mov+=10;
    if (mov>0)
        mov = 0;
}

void MainScene::MoveBG (float dt)
{
    
    CCPoint backgroundScrollVert = ccp(player->Winged ? (mov - ((mov*player->WingedPerc)/100)) : mov, 0);
    CCPoint xScroll = ccpMult(backgroundScrollVert, dt);
    _backgroundNode->setPosition(ccpAdd(_backgroundNode->getPosition(), xScroll));
    UpdateBG();
    meters = _backgroundNode->getPosition().x/50;
    if (MoveLevel || SkipLevel)
    {
        if (MoveLevel)
        {
            CCDelayTime *delayFade = CCDelayTime::create(0.2);
            CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MainScene::fastMoveLevel));
            this->runAction(CCSequence::create(delayFade, call, NULL));
            MoveLevel = false;
            SkipLevel = true;
        }
    }
    else if ((meters - improvingSpeed)<-50)
    {
        improvingSpeed = meters;
        //mov-=5;
        mov-=10;
        if (mov<-2500)
            mov = -2500;
    }

}

void MainScene::UpdateGemsMEters()
{
    char m[10];
    sprintf(m, "%.2f", meters*(-1));
    metersLabel->setString(m);
    
    char chargems[10];
    sprintf(chargems, "%i", player->Gems);
    gemsLabel->setString(chargems);
}

void MainScene::HandleMissileAttachment ()
{
    if (MissileNumber != 0 && GetMilliSpan(LastTimeMissile)>TIME_BETWEEN_MISSILE)
    {
        ObstacleType* ob = new ObstacleType();
        //randomValueBetween(0,3);
        if (randomValueBetween(0,3) < 1)
        {
            ob->InitMissile("Rocket_UpDw.png", "RocketAlert.png", "Rocket_Smoke.png", ItemNode, true, SpriteList);
        }
        else
        {
            ob->InitMissile("Rocket_Line.png", "RocketAlert.png", "Rocket_Smoke.png", ItemNode, false, SpriteList);
        }
        arrayObstacle.push_back(ob);
        MissileNumber--;
        LastTimeMissile = GetMilliCount();
        LastTimeAttach = GetMilliCount();
    }
    if (AttachNumber != 0 && GetMilliSpan(LastTimeAttach)>TIME_BETWEEN_ATTACH)
    {
        ObstacleType* ob = new ObstacleType();
        randomValueBetween(0,3);
        if (randomValueBetween(0,3) < 1)
        {
            ob->InitFlyingEnemy("Glue1_Frame1.png", ItemNode, SpriteList);
        }
        else
        {
            ob->InitFlyingOnlyDownEnemy("Glue2_Frame1.png", ItemNode, SpriteList);
        }
        arrayObstacle.push_back(ob);
        AttachNumber--;
        LastTimeAttach = GetMilliCount();
    }
    if (GetMilliSpan(LastTimeMissile)>TIME_NEXT_MISSILE && MissileNumber==0 && !player->Dying)
    {
        LastTimeMissile = GetMilliCount();
        int r = (int)randomValueBetween(0,1000);
        CCLog("Random cazzo %i", r);
        if (r<500)
        {
            MissileNumber = 1;
        }
        else if (r<600)
        {
            MissileNumber = 2;
        }
        else if (r<800)
        {
            MissileNumber = 3;
        }
        else if (r<900)
        {
            MissileNumber = 4;
        }
    }
    if (GetMilliSpan(LastTimeAttach)>TIME_NEXT_ATTACH && AttachNumber==0 && !player->Dying)
    {
        LastTimeAttach = GetMilliCount();
        float r =randomValueBetween(0,1000);
        if (r<500)
        {
            AttachNumber = 1;
        }
        else if (r<600)
        {
            AttachNumber = 2;
        }
        else if (r<700)
        {
            AttachNumber = 3;
        }
        else if (r<800)
        {
            AttachNumber = 4;
        }
        else if (r<900)
        {
            AttachNumber = 5;
        }
    }
}

void MainScene::CheckCollisionsAndUpdate(float dt)
{
    bool justUpdate = true;
    if (player->Dying)
    {
        for (int j=0; j<arrayObstacle.size(); j++)
        {
            ObstacleType* ob = arrayObstacle.at(j);
            ob->setVisible(false);
            Block->Hide();
        }
    }
    else
    {
        vector<FrameADT::dimStruct> array;
        if (player->Animation == player->ANIM_UP)
        {
            for (int i=0; i<player->spriteAnimUp.size(); i++)
            {
                if (player->ship->isFrameDisplayed(player->spriteAnimUp.at(i)))
                {
                    array = player->CollisionUp.at(i);
                    break;
                }
            }
        }
        else if (player->Animation == player->ANIM_DOWN)
        {
            for (int i=0; i<player->spriteAnimDown.size(); i++)
            {
                if (player->ship->isFrameDisplayed(player->spriteAnimDown.at(i)))
                {
                    array = player->CollisionDown.at(i);
                    break;
                }
            }
        }
        else
        {
            for (int i=0; i<player->spriteAnimFly.size(); i++)
            {
                if (player->ship->isFrameDisplayed(player->spriteAnimFly.at(i)))
                {
                    array = player->CollisionFly.at(i);
                    break;
                }
            }
        }
        float px = player->ship->getPosition().x;
        float py = player->ship->getPosition().y;
        for (int i=0; i<array.size(); i++)
        {
            FrameADT::dimStruct d = array.at(i);
            Block->Update(ccp(d.x+px,py-d.y), d.radius, player, justUpdate, ForcePause, RemovingAll, dt);
            if (ForcePause || RemovingAll)
                return;
            gems->Update(ccp(d.x+px,py-d.y), d.radius, player, justUpdate);
            for (int j=0; j<arrayObstacle.size(); j++)
            {
                ObstacleType* ob = arrayObstacle.at(j);
                ob->Update(ccp(d.x+px,py-d.y), d.radius, player, justUpdate, dt);
            }
            justUpdate = false;
        }
        
        for (int j=0; j<arrayObstacle.size(); j++)
        {
            ObstacleType* ob = arrayObstacle.at(j);
            if (ob->type == ob->MISSILE || ob->type == ob->MISSILEUPDOWN)
            {
                if (ob->MissileIsOut())
                {
                    ob->Destroy();
                    arrayObstacle.erase(arrayObstacle.begin()+(j));
                }
            }
            else if(ob->type == ob->FLYINGENEMY || ob->type == ob->FLYINGENEMYONLYDOWN)
            {
                if (ob->MissileIsOut())
                {
                    ob->Destroy();
                    arrayObstacle.erase(arrayObstacle.begin()+(j));
                }
            }
        }
    }
}

void MainScene::update(float dt) {
    if (ForcePause)
        return;
    
    if (player->Dying)
    {
        PlayerIsDying();
        if (mov == 0)
        {
            CCSize winSize = CCDirector::sharedDirector()->getWinSize();
            if(!isAlreadyDead)
            {
                if(!CardCreated)
                {
                    this->showCards();
                    CardCreated = true;
                }
            }
            else
            {
                this->unscheduleUpdate();
                this->unscheduleAllSelectors();
                bgEnd = CCSprite::create();
                CCTexture2D *tex = new CCTexture2D();
                char _raw[4];
                for(int i = 0; i < 4; i++)
                    _raw[i] = 255;
                tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
                bgEnd->initWithTexture(tex, CCRectMake(0, 0, winSize.width, winSize.height));
                bgEnd->setPosition(ccp(winSize.width/2, winSize.height/2));
                bgEnd->setColor(ccBLACK);
                bgEnd->setOpacity(200);
                this->addChild(bgEnd, 100);
                this->showResults();
                CCLog("Show result finale");
            }
            return;
        }
    }
    CheckCollisionsAndUpdate(dt);
    UpdateGemsMEters();
    player->Update(dt);
    HandleMissileAttachment();

    if (RemovingAll)
    {
        Block->Destroy();
        for (int j=0; j<arrayObstacle.size(); j++)
        {
            ObstacleType* ob = arrayObstacle.at(j);
            ob->Destroy();
        }
        arrayObstacle.clear();
        RemovingAll = false;
        return;
    }
    MoveBG (dt);
    if (MoveLevel || SkipLevel || ForcePause)
        return;
    UpdatePivot();
    
}

bool MainScene::isIntersectingRect(CCRect r1, CCRect r2)
{
    return !(r1.getMinX() > r2.getMinX()+(r2.getMaxX()-r2.getMinX()) || r1.getMinX()+(r1.getMaxX()-r1.getMinX()) < r2.getMinX() || r1.getMinY() > r2.getMinY()+(r2.getMaxY()-r2.getMinY()) || r1.getMinY()+(r1.getMaxY()-r1.getMinY()) < r2.getMinY());
}

void MainScene::UpdateBG()
{
    float xPosition = _backgroundNode->convertToWorldSpace(CurrentBg->getPosition()).x-((CurrentBg->getContentSize().width/4)*3);
    float size = CurrentBg->getContentSize().width;
    CCPoint offset = _backgroundNode->getOffset(CurrentBg);
    if (ChangeNow != 0 && _backgroundNode->getPosition().x<NextMovLevel && NextMovLevel != -1)
    {
        StopAll(false);
        MoveLevel = true;
        NextMovLevel = -1;
    }
    if ( xPosition < -15 )
    {
        if (meters-changelvl < -2000)
        {
            changelvl = meters;
            ChangeNow = 2;
            NextMovLevel =(_backgroundNode->getPosition().x-ChangeLevel->getContentSize().width-(ChangeLevel->getContentSize().width/2)-size)-2;
            _backgroundNode->setRealOffset(ccp((ChangeLevel->getContentSize().width+size+offset.x)-2,getContentSize().height/2), ChangeLevel);
        }
        else
        {
            if (ChangeNow == -1)
            {
                SkipLevel = false;
                ResumeAll ();
                ChangeNow = 0;
                _backgroundNode->setRealOffset(ccp((size+offset.x)-2,0),_pivot);
            }
            else if (ChangeNow == 1)
            {
                if (Current1 == GreenScroll1)
                {
                    Current1 = YellowScroll1;
                    Current2 = YellowScroll2;
                }
                else if (Current1 == YellowScroll1)
                {
                    Current1 = OrangeScroll1;
                    Current2 = OrangeScroll2;
                }
                else
                {
                    Current1 = GreenScroll1;
                    Current2 = GreenScroll2;
                }
                ChangeNow = -1;
            }
            else if (ChangeNow == 2)
            {
                ChangeNow = 1;
            }
            _backgroundNode->setRealOffset(ccp((size+offset.x)-2,getContentSize().height/2),CurrentBg == Current2 ? Current1 : Current2);
            CurrentBg = CurrentBg == Current2 ? Current1 : Current2;
        }
    }
}

void MainScene::fastMoveLevel()
{
    mov = -2500;
}

void MainScene::UpdatePivot()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    float xPosition = _backgroundNode->convertToWorldSpace(_pivot->getPosition()).x;
    CCLog("UpdatePivot %f", xPosition);
    if ( xPosition < 0 ) {
        
        _backgroundNode->incrementOffset(ccp(winSize.width,0),_pivot);
        xPosition = _backgroundNode->getOffset(_pivot).x;
        CCLog("UpdatePivot nextGems %i", nextGems);
        if (nextGems == 2)
        {
            CCLog("UpdatePivot CreateGem");
            CreateGem(xPosition);
        }
        else
        {
            if (nextGems == 1)
            {
                CCLog("UpdatePivot CreateNormalLaser");
                CreateNormalLaser(xPosition);
            }
            else
            {
                CCLog("UpdatePivot incrementOffset");
                _backgroundNode->incrementOffset(ccp(winSize.width,0),_pivot);
                nextGems++;
            }
        }
    }
}

void MainScene::CreateNormalLaser (float xPosition)
{
    BlockReader::structBlock* block = bl->ContainerBlock.at(randomValueBetween(0,bl->ContainerBlock.size()));
    Block->Init(block, _backgroundNode, xPosition, SpriteList, Current1 == GreenScroll1? 1: Current1 == YellowScroll1 ? 2 :3, currentDiff);
    currentDiff--;
    if (currentDiff <0)
        currentDiff = 0;
    _backgroundNode->incrementOffset(ccp((block->Width+(50*currentDiff)*block->obstacle.size())-960,0),_pivot);
    nextGems++;
}

/*void MainScene::CreateHugeLaser()
{
    HugeLaserActive = true;
    HugeLaserWaiting = false;
    LastTimeHugeLaser = GetMilliCount();
    //TODO qui scelgo il tipo di laser.. per ora mi limito a farne uno
    hugeLaserBlock = new HugeLaserBlock();
    hugeLaserBlock->Init(_batchNode, randomValueBetween(0,3));
}*/

void MainScene::CreateGem(float xPosition)
{
    std::string str = "gems";
  //  aza
    std::string String = static_cast<ostringstream*>( &(ostringstream() << ((arc4random() % 6)+1)) )->str();
    str=str+String+".txt";
    char *a=new char[str.size()+1];
    a[str.size()]=0;
    memcpy(a,str.c_str(),str.size());
    gems->Init(a, _backgroundNode, xPosition, 450, "Gem_Idle.png");
    _backgroundNode->incrementOffset(ccp(gems->width,0),_pivot);
    nextGems=0;
}

void MainScene::addEnemyPassed()
{
    main->player->enemyPassed++;
}

void MainScene::StopAll (bool pause)
{
    if (pause)
        main->ForcePause = true;
    main->oldMov = main->mov;
    main->mov = 0;
    main->gems->Destroy();
    main->player->Pause();
    main->Block->Destroy();
    for (int j=0; j<main->arrayObstacle.size(); j++)
    {
        ObstacleType* ob = main->arrayObstacle.at(j);
        ob->Destroy();
    }
    main->arrayObstacle.clear();
}

void MainScene::RemoveAll ()
{
    main->RemovingAll = true;
}

void MainScene::ResumeAll ()
{
    main->mov = main->oldMov;
    main->ForcePause = false;
    main->player->UnPause();
}

void MainScene::AddNewChild (CCSprite* sp, int pos)
{
    main->addChild(sp, pos);
}

void MainScene::RemoveChild (CCSprite* sp)
{
    main->removeChild(sp, true);
}

void MainScene::draw()
{
    
}

void MainScene::showCards()
{
    pMainPauseMenu->setEnabled(false);
    CCLog("showCards");
    this->unscheduleAllSelectors();
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    BonusBg->setVisible(true);
    int i=0;
    
    
    CCArray *arrayItemsMenu = CCArray::create();
    
    for (int y=0; y<2; y++)
    {
        for (int x=0; x<4; x++)
        {
            cards[i] = CCSprite::createWithSpriteFrameName("Top.png");
            
            int w = cards[i]->getContentSize().width;
            int h = cards[i]->getContentSize().height;
            int spaceW = (winSize.width-(w*4))/4;
            int spaceH = (winSize.height-(h*2))/2;
            
            CCMenuItemSprite *cartItem = CCMenuItemSprite::create(cards[i], cards[i], this, menu_selector(MainScene::selectCard));
            cartItem->setPosition(ccp((spaceW/2)+(spaceW*x)+(w*x)+(w/2), (spaceH/2)+(spaceH*y)+(h*y)+(h/2)));
            cartItem->setTag(i);
            arrayItemsMenu->addObject(cartItem);
            i++;
        }
    }
    
    pCardMenu = CCMenu::createWithArray(arrayItemsMenu);
    pCardMenu->setPosition(CCPointZero);
    this->addChild(pCardMenu, 100);
}

void MainScene::selectCard(cocos2d::CCObject *pSender)
{
    srand ( time(NULL) );
    int rnd = this->randomValueBetween(1, 100);
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    CCSprite *spr = (CCSprite *)pSender;
    
    CCFadeOut *fadeOut = CCFadeOut::create(0.5);
    CCLog("Random %i", rnd);
    bool shield = false;
    bool stabilizer = false;
    bool wings = false;
    bool all = false;

    if(rnd<=kBonusProbability)
    {
        CCLog("Bonus");
        int rndBonus = this->randomValueBetween(0, bonusArray.size());
        
        CCLog("Selezionata %i-%s", rndBonus, bonusArray[rndBonus].desc.c_str());
        sprBonus = CCSprite::createWithSpriteFrameName(bonusArray[rndBonus].sprName.c_str());
        sprBonus->setPosition(spr->getPosition());
        sprBonus->setTag(rndBonus);
        this->addChild(sprBonus, 101);
        CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
        if (!userDefault->getBoolForKey("SHIELD_UNLOCKED") && rndBonus == 1)
            rndBonus = 4;
        
        if (!userDefault->getBoolForKey("WINGS_UNLOCKED") && rndBonus == 5)
            rndBonus = 4;
        
        switch (rndBonus) {
                
            case 0:
                break;
                
            case 1:
                shield = true;
                break;
                
            case 2:
                stabilizer = true;
                break;
                
            case 3:
                player->Gems+=500;
                char g[10];
                sprintf(g, "%i", player->Gems);
                gemsLabel->setString(g);
                break;
                
            case 4:
                player->Gems+=50;
                char gi[10];
                sprintf(gi, "%i", player->Gems);
                gemsLabel->setString(gi);
                break;
            
            case 5:
                wings = true;
                break;
                
            case 6:
                all = true;
                break;
                
            default:
                break;
        }
        userDefault->setBoolForKey("Shield", shield);
        userDefault->setBoolForKey("Wings", wings);
        userDefault->setBoolForKey("Stabilizer", stabilizer);
        userDefault->setBoolForKey("Allbonus", all);
        userDefault->flush();

        CCDelayTime *delayFade = CCDelayTime::create(0.5);
        CCMoveTo *move = CCMoveTo::create(0.5, CCPointMake(winSize.width/2, winSize.height/2));
        CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MainScene::showBonusDesc));
        sprBonus->runAction(CCSequence::create(delayFade, move, call, NULL));
    }
    else
    {
        sprBonus = CCSprite::createWithSpriteFrameName("Card_NoBonus.png");
        sprBonus->setPosition(spr->getPosition());
        sprBonus->setTag(-1);
        this->addChild(sprBonus, 101);
        CCDelayTime *delayFade = CCDelayTime::create(0.5);
        CCMoveTo *move = CCMoveTo::create(0.5, CCPointMake(winSize.width/2, winSize.height/2));
        CCCallFunc *call = CCCallFunc::create(this, callfunc_selector(MainScene::showBonusDesc));
        sprBonus->runAction(CCSequence::create(delayFade, move, call, NULL));
    }
    
    pCardMenu->setEnabled(false);
    pCardMenu->runAction(fadeOut);
}

void MainScene::showBonusDesc(cocos2d::CCObject *pSender)
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    //CCSprite *spr = (CCSprite *)pSender;
    
    CCLog("showBonusDesc %i", sprBonus->getTag());
    int tag = sprBonus->getTag();
    if (tag != -1)
        descBonus = CCLabelTTF::create(bonusArray[tag].desc.c_str(), "Thonburi", 48);
    else
        descBonus = CCLabelTTF::create("Nothing", "Thonburi", 48);

    descBonus->setColor(ccWHITE);
    descBonus->setPosition(ccp(winSize.width/2, (winSize.height/2)-200));
    //desc->setOpacity(0);
    this->addChild(descBonus, 102);
    //CCFadeIn *fadeIn = CCFadeIn::create(0.5);
    //desc->runAction(fadeIn);
    CCDelayTime *delay = CCDelayTime::create(2.0f);
    CCFadeOut *fadeOut = CCFadeOut::create(0.5);
    CCCallFunc *call = CCCallFunc::create(this, tag != -1 && bonusArray[tag].desc.compare("Life") ==0 ? callfunc_selector(MainScene::Revive) :                                                                                                 callfunc_selector(MainScene::showResults));
    sprBonus->runAction(CCSequence::create(delay, fadeOut, call, NULL));
}

void MainScene::finnDeathAnimFly()
{
    CCLog("Morte in volo");
    RemoveAll();
    finnDeath1 = CCSprite::createWithSpriteFrameName("Finn_DeathDown.png");
    finnDeath = CCSprite::createWithSpriteFrameName("Finn_DeathFly.png");
    jackShadow =CCSprite::createWithSpriteFrameName("Shadow.png");
    finnShadow =CCSprite::createWithSpriteFrameName("Shadow.png");
    
    ItemNode->addChild(jackShadow, 1);
    ItemNode->addChild(finnShadow, 1);
    jackShadow->setVisible(false);
    finnShadow->setVisible(false);
    jackDeath1 = CCSprite::createWithSpriteFrameName("Jake_DeathDown.png");
    jackDeath = CCSprite::createWithSpriteFrameName("Jake_DeathFly.png");
    
    finnDeath->setPosition(ccp(player->getPosition().x+50, player->getPosition().y));
    finnDeath1->setPosition(ccp(player->getPosition().x+50, player->getPosition().y));
    ItemNode->addChild(finnDeath1, 1);
    finnDeath1->setVisible(false);
    ItemNode->addChild(finnDeath, 1);
    
    jackDeath->setPosition(ccp(player->getPosition().x+50, player->getPosition().y));
    jackDeath1->setPosition(ccp(player->getPosition().x+50, player->getPosition().y));
    ItemNode->addChild(jackDeath1, 1);
    jackDeath1->setVisible(false);
    ItemNode->addChild(jackDeath, 1);
    
    CCRotateBy *rotate = CCRotateBy::create(0.4f, 360.0f);
    CCRepeatForever *rep = CCRepeatForever::create(rotate);
    finnDeath->runAction(rep);
    
    CCRotateBy *rotate1 = CCRotateBy::create(0.4f, -360.0f);
    CCRepeatForever *rep1 = CCRepeatForever::create(rotate1);
    jackDeath->runAction(rep1);
    player->ship->setVisible(false);
    player->shadow->setVisible(false);
    jackShadow->setPosition(ccp(player->getPosition().x-56, 30));
    finnShadow->setPosition(ccp(player->getPosition().x-56, 30));
}

void MainScene::Revive ()
{
    BonusBg->setVisible(false);
    pMainPauseMenu->setEnabled(true);
    descBonus->removeAllChildrenWithCleanup(true);
    finnDeath1->removeAllChildrenWithCleanup(true);
    finnDeath->removeAllChildrenWithCleanup(true);
    jackShadow->removeAllChildrenWithCleanup(true);
    finnShadow->removeAllChildrenWithCleanup(true);
    jackDeath1->removeAllChildrenWithCleanup(true);
    ItemNode->removeChild(finnDeath1, true);
    ItemNode->removeChild(finnDeath, true);
    ItemNode->removeChild(jackShadow, true);
    ItemNode->removeChild(finnShadow, true);
    this->removeChild(descBonus, true);
    ItemNode->removeChild(jackDeath1, true);
    
    player->ship->setVisible(true);
    player->shadow->setVisible(true);
    isAlreadyDead = true;
    player->Dying = false;
    mov = -1000;
    
    this->removeChild(bgEnd, true);
    this->scheduleUpdate();
    animDead = false;
}

void MainScene::showResults()
{
    
    this->removeChild(descBonus, true);
    this->removeChild(sprBonus, true);
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    this->unscheduleUpdate();
    this->unscheduleAllSelectors();
    this->stopAllActions();
    _backgroundNode->unscheduleAllSelectors();
    _backgroundNode->unscheduleUpdate();
    _backgroundNode->stopAllActions();
    CCLog("Ostacoli %li", arrayObstacle.size());
    for (int j=0; j<arrayObstacle.size(); j++)
    {
        ObstacleType* ob = arrayObstacle.at(j);
        ob->setVisible(false);
    }

    Block->Pause();
    gems->Pause();
    
    CCSprite* bg = CCSprite::create();
    int w = size.width;
    int h = size.height;
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    _raw[0] = 255;
    _raw[1] = 255;
    _raw[2] = 255;
    _raw[3] = 255;
    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg->initWithTexture(tex, CCRectMake(0, 0, w, h));
    bg->setPosition(ccp(size.width/2, size.height/2));
    ccColor3B color;
    color.r = 255;
    color.g = 187;
    color.b = 28;
    bg->setColor(color);
    this->addChild(bg, 99);
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    float oldm = userDefault->getFloatForKey("METER_RECORD", 0);
    string str = userDefault->getStringForKey("UID", "");
    if (oldm<(meters*(-1)))
    {
        char m[10];
        sprintf(m, "%.2f", meters*(-1));
        userDefault->setStringForKey("METER_RECORD", m);
        userDefault->setStringForKey("METER_MUST_UPLOAD", m);
    }
    GameOverBg->setVisible(true);
    
    CCLabelBMFont *gameover = CCLabelBMFont::create("Game Over", "results_font.fnt", 300, kCCTextAlignmentCenter);
    gameover->setPosition(ccp(size.width/2+150, size.height-100));
    this->addChild(gameover, 101);
    
        

    char m[10];
    sprintf(m, "%.2f", meters*(-1));
    CCLabelBMFont *metersInGameValue = CCLabelBMFont::create(m, "results_font.fnt", 100, kCCTextAlignmentLeft);
    //metersInGameValue->setAnchorPoint(ccp(1.0f, 0.5f));
    metersInGameValue->setPosition(ccp(size.width/2+150, size.height-320));
    this->addChild(metersInGameValue, 101);

    
    char g[10];
    sprintf(g, "%i", player->Gems);
    CCLabelBMFont *gemsLastGameValue = CCLabelBMFont::create(g, "results_font.fnt", 100, kCCTextAlignmentLeft);
    //gemsLastGameValue->setAnchorPoint(ccp(1.0f, 0.5f));
    gemsLastGameValue->setPosition(ccp(size.width/2, size.height-450));
    this->addChild(gemsLastGameValue, 101);

    int totalGems = userDefault->getIntegerForKey("total_gems");
    totalGems+=player->Gems;
    userDefault->setIntegerForKey("total_gems", totalGems);
    userDefault->flush();
    
    CCSprite *sprBack = CCSprite::createWithSpriteFrameName("Back.png");
    CCMenuItemSprite *backItem = CCMenuItemSprite::create(sprBack, sprBack, this, menu_selector(MainScene::gotoMenu));
    backItem->setPosition( ccp(backItem->getContentSize().width, backItem->getContentSize().height+5) );
    CCMenu *pMenu = CCMenu::create(backItem, NULL);
    
    pMenu->setPosition( CCPointZero );
    this->addChild(pMenu, 102);

}

void MainScene::showResultVisible(cocos2d::CCObject *pSender)
{
    CCLabelBMFont *label = (CCLabelBMFont*)pSender;
    label->setOpacity(255);
}

void MainScene::gotoMenu(cocos2d::CCObject *pSender)
{
    if (checkAchievements())
    {
        CCScene *pScene = Achievements::scene();
        CCDirector::sharedDirector()->replaceScene(pScene);
    }
    else
    {
        CCScene *pScene = MenuScene::scene();
        CCDirector::sharedDirector()->replaceScene(pScene);
    }
}

bool MainScene::checkAchievements()
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    bool mustShowAch = false;
    bool meter1 = userDefault->getBoolForKey("300M");
    bool meter2 = userDefault->getBoolForKey("1000M");
    bool meter3 = userDefault->getBoolForKey("3000M");
    bool enemy1 = userDefault->getBoolForKey("50E");
    bool enemy2 = userDefault->getBoolForKey("200E");
    bool enemy3 = userDefault->getBoolForKey("300E");
    bool bonus1 = userDefault->getBoolForKey("20B");
    bool bonus2 = userDefault->getBoolForKey("50B");
    bool bonus3 = userDefault->getBoolForKey("100B");
    bool gems1 = userDefault->getBoolForKey("100G");
    bool gems2 = userDefault->getBoolForKey("500G");
    bool gems3 = userDefault->getBoolForKey("600G");
    if (meter3 && enemy3 && bonus3 && gems3)
        return false;
    int m = meters*-1;
    int passed = userDefault->getIntegerForKey("Passed");
    passed+=player->enemyPassed;
    int bonus = userDefault->getIntegerForKey("Bonus");
    bonus+=player->bonusTaken;
    int gem = userDefault->getIntegerForKey("AchGem");
    gem+=player->Gems;
    if (meter2 && enemy2 && bonus2 && gems2)
    {
        if (m >BONUSMETER3 && !meter3)
        {
            userDefault->setBoolForKey("3000M", true);
            userDefault->setBoolForKey("MustShow_3000M", true);
            mustShowAch = true;
            meter3 = true;
        }
        if (passed >BONUSPASSED3 && !enemy3)
        {
            userDefault->setBoolForKey("300E", true);
            userDefault->setBoolForKey("MustShow_300E", true);
            mustShowAch = true;
            enemy3 = true;
        }
        if (bonus >BONUSBONUS3 && !bonus3)
        {
            userDefault->setBoolForKey("100B", true);
            userDefault->setBoolForKey("MustShow_100B", true);
            mustShowAch = true;
            bonus3 = true;
        }
        if (gem >BONUSGEMS3 && !gems3)
        {
            userDefault->setBoolForKey("600G", true);
            userDefault->setBoolForKey("MustShow_600G", true);
            mustShowAch = true;
            gems3 = true;
        }
        if (meter3 && enemy3 && bonus3 && gems3)
        {
            passed = 0;
            bonus = 0;
            gem = 0;
            userDefault->setBoolForKey("MUSTSHOW_MISTERY3", true);
            userDefault->setBoolForKey("MISTERY3", true);
        }
    }
    else if (meter1 && enemy1 && bonus1 && gems1)
    {
        if (m >BONUSMETER2 && !meter2)
        {
            userDefault->setBoolForKey("1000M", true);
            userDefault->setBoolForKey("MustShow_1000M", true);
            mustShowAch = true;
            meter2 = true;
        }
        if (passed >BONUSPASSED2 && !enemy2)
        {
            userDefault->setBoolForKey("200E", true);
            userDefault->setBoolForKey("MustShow_200E", true);
            mustShowAch = true;
            enemy2 = true;
        }
        if (bonus >BONUSBONUS2 && !bonus2)
        {
            userDefault->setBoolForKey("50B", true);
            userDefault->setBoolForKey("MustShow_50B", true);
            mustShowAch = true;
            bonus2 = true;
        }
        if (gem >BONUSGEMS2 && !gems2)
        {
            userDefault->setBoolForKey("500G", true);
            userDefault->setBoolForKey("MustShow_500G", true);
            mustShowAch = true;
            gems2 = true;
        }
        if (meter2 && enemy2 && bonus2 && gems2)
        {
            passed = 0;
            bonus = 0;
            gem = 0;
            userDefault->setBoolForKey("MUSTSHOW_MISTERY2", true);
            userDefault->setBoolForKey("MISTERY2", true);
            userDefault->setBoolForKey("WINGS_UNLOCKED", true);
        }
    }
    else
    {
        if (m >BONUSMETER1 && !meter1)
        {
            userDefault->setBoolForKey("300M", true);
            userDefault->setBoolForKey("MustShow_300M", true);
            mustShowAch = true;
            meter1 = true;
        }
        if (passed >BONUSPASSED1 && !enemy1)
        {
            userDefault->setBoolForKey("50E", true);
            userDefault->setBoolForKey("MustShow_50E", true);
            mustShowAch = true;
            enemy1 = true;
        }
        if (bonus >BONUSBONUS1 && !bonus1)
        {
            userDefault->setBoolForKey("20B", true);
            userDefault->setBoolForKey("MustShow_20B", true);
            mustShowAch = true;
            bonus1 = true;
        }
        if (gem >BONUSGEMS1 && !gems1)
        {
            userDefault->setBoolForKey("100G", true);
            userDefault->setBoolForKey("MustShow_100G", true);
            mustShowAch = true;
            gems1 = true;
        }
        if (meter1 && enemy1 && bonus1 && gems1)
        {
            passed = 0;
            bonus = 0;
            gem = 0;
            userDefault->setBoolForKey("MUSTSHOW_MISTERY1", true);
            userDefault->setBoolForKey("MISTERY1", true);
            userDefault->setBoolForKey("SHIELD_UNLOCKED", true);
        }
    }
    userDefault->setIntegerForKey("Passed",passed);
    userDefault->setIntegerForKey("Bonus",bonus);
    userDefault->setIntegerForKey("AchGem",gem);
    userDefault->flush();
    return mustShowAch;
}

void MainScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if (!player->Dying)
    {
        player->isAccelerating=true;
        player->touchBegan();
    }
}

void MainScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    player->isAccelerating = false;
    if (!player->Dying)
        player->touchEnded();
}

int MainScene::randomValueBetween(int low, int high)
{
    srand ( time(NULL) );
    int r = arc4random() % (high-low);
    return r+low;
    //return (((int) arc4random() / 0xFFFFFFFF) * (high - low)) + low;
}

void MainScene::setInvisible(CCNode * node) {
    node->setVisible(false);
}


float MainScene::getTimeTick() {
    timeval time;
    gettimeofday(&time, NULL);
    unsigned long millisecs = (time.tv_sec * 1000) + (time.tv_usec/1000);
    return (float) millisecs;
}

void MainScene::menuCloseCallback(CCObject* pSender)
{
    CCDirector::sharedDirector()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MainScene::pauseGame(CCObject* pSender)
{
    pMainPauseMenu->setEnabled(false);
    CCLog("pauseGame");
    this->unscheduleAllSelectors();
    gems->Pause();
    player->Pause();
    Block->Pause();
    for (int j=0; j<arrayObstacle.size(); j++)
    {
        ObstacleType* ob = arrayObstacle.at(j);
        ob->Pause();
    }
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    
    bgPause = CCSprite::create();
    bgPause->setTextureRect(CCRectMake(0, 0, winSize.width, winSize.height));
    bgPause->setPosition(ccp(winSize.width/2, winSize.height/2));
    bgPause->setColor(ccBLACK);
    bgPause->setOpacity(180);
    this->addChild(bgPause);
    
    CCLabelTTF* continueLabel = CCLabelTTF::create("Continue", "Thonburi", 48);
    CCMenuItemLabel *continueItem = CCMenuItemLabel::create(continueLabel, this, menu_selector(MainScene::continueGame));
    continueItem->setPosition( ccp(winSize.width / 2, winSize.height/2) );
    
    // create menu, it's an autorelease object
    pPauseMenu = CCMenu::create(continueItem, NULL);
    pPauseMenu->setPosition( CCPointZero );
    this->addChild(pPauseMenu, 1);
}

void MainScene::continueGame(cocos2d::CCObject *pSender)
{
    gems->UnPause();
    player->UnPause();
    Block->UnPause();
    for (int j=0; j<arrayObstacle.size(); j++)
    {
        ObstacleType* ob = arrayObstacle.at(j);
        ob->UnPause();
    }
    pMainPauseMenu->setEnabled(true);
    this->removeChild(bgPause, true);
    this->removeChild(pPauseMenu, true);
    this->scheduleUpdate();
}

int MainScene::GetMilliCount()
{
    // Something like GetTickCount but portable
    // It rolls over every ~ 12.1 days (0x100000/24/60/60)
    // Use GetMilliSpan to correct for rollover
    timeb tb;
    ftime( &tb );
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}

int MainScene::GetMilliSpan( int nTimeStart )
{
    int nSpan = GetMilliCount() - nTimeStart;
    if ( nSpan < 0 )
        nSpan += 0x100000 * 1000;
    return nSpan;
}

void MainScene::keyBackClicked()
{
	CCLog("UFFA");
	pauseGame(NULL);
}

void MainScene::onExit()
{
	this->removeAllChildrenWithCleanup(true);
	this->setKeypadEnabled(false);
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("world_texture.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("world_texture2.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("main.plist");
    CCLog("Si esce");
}

