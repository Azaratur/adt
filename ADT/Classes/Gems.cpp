//
//  Gems.cpp
//  AdventureTime
//
//  Created by Johnny Picciafuochi on 19/09/2013.
//
//

#include "Gems.h"
#include "Gems.h"
#include <iostream>
#include <fstream>
#include <string>
#include "SimpleAudioEngine.h"

using namespace std;

Gems::Gems()
{
    CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
    statusAudio = userDefault->getBoolForKey("audio_disable");
    Initialized = false;
    Time = -1;
}

void Gems::Destroy()
{
    for (int i=0; i<stars.size(); i++)
    {
        CCSprite* sprite = stars.at(i);
        node->removeChild(sprite, true);
    }
    stars.clear();
}

void Gems::Pause()
{
    for (int i=0; i<stars.size(); i++)
    {
        stars.at(i)->pauseSchedulerAndActions();
    }
}


void Gems::UnPause()
{
    for (int i=0; i<stars.size(); i++)
    {
        stars.at(i)->resumeSchedulerAndActions();
    }
}

void Gems::Init(const char f [], CCParallaxNodeExtras* n, float startx, float y, char c [])
{
    this->Destroy();
    string line;
    node = n;
    //width = 0;
    CCPoint bgSpeed = ccp(0.4, 0.4);
    CCSprite* sprite = CCSprite::createWithSpriteFrameName(c);
    //CCSprite* sprite;
    float time = 0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    std::string writablePath = CCFileUtils::sharedFileUtils()->fullPathForFilename(f);
    unsigned long size = 0;
    unsigned char* pData = 0;
    pData = CCFileUtils::sharedFileUtils()->getFileData(writablePath.c_str(), "rb", &size);
    std::string str;
    str.append(reinterpret_cast<const char*>(pData));
    std::istringstream iss(str);

    while (std::getline(iss, line))
    {
        string str = line.c_str();
        float x = startx;
        for (int i=0; i<str.size(); i++)
        {
            if (str.at(i) == '*')
            {
                sprite = CCSprite::createWithSpriteFrameName(c);
                sprite->setScale(0.7);
                stars.push_back(sprite);
                node->addChild(sprite, -1, bgSpeed, ccp(-1500, 0));
                node->setRealOffset(ccp(x, y), sprite);
                
                CCSpriteFrame *spriteAnim1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame1.png");
                CCSpriteFrame *spriteAnim2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame2.png");
                CCSpriteFrame *spriteAnim3 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame3.png");
                CCSpriteFrame *spriteAnim4 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame4.png");
                CCSpriteFrame *spriteAnim5 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame5.png");
                CCSpriteFrame *spriteAnim6 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Idle.png");
                
                CCArray *animFrames = CCArray::create();
                animFrames->addObject(spriteAnim1);
                animFrames->addObject(spriteAnim2);
                animFrames->addObject(spriteAnim3);
                animFrames->addObject(spriteAnim4);
                animFrames->addObject(spriteAnim5);
                animFrames->addObject(spriteAnim6);
                
                CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.15f);
                CCAnimate *animate = CCAnimate::create(animation);
                CCFiniteTimeAction *delayAction = CCDelayTime::create(time);
                time+=0.1;
                CCFiniteTimeAction *anim = CCRepeat::create(animate, 10);
                CCFiniteTimeAction *readySequence =  CCSequence::create(delayAction, anim, NULL);
                sprite->runAction(readySequence);
            }
            x+=sprite->getContentSize().width;
        }
        if (x>width)
            width = x;
        y-=sprite->getContentSize().height;
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    ifstream myfile;
    std::string fStr = CCFileUtils::sharedFileUtils()->fullPathForFilename(f);
    
    myfile.open(fStr.c_str());
    width = 0;

    if (myfile.is_open())
    {
        while (! myfile.eof() )
        {
            getline (myfile,line);
            string str = line.c_str();
            float x = startx;
            for (int i=0; i<str.size(); i++)
            {
                if (str.at(i) == '*')
                {
                    sprite = CCSprite::createWithSpriteFrameName(c);
                    sprite->setScale(0.7);
                    stars.push_back(sprite);
                    node->addChild(sprite, -1, bgSpeed, ccp(-1500, 0));
                    node->setRealOffset(ccp(x, y), sprite);
                    
                    CCSpriteFrame *spriteAnim1 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame1.png");
                    CCSpriteFrame *spriteAnim2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame2.png");
                    CCSpriteFrame *spriteAnim3 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame3.png");
                    CCSpriteFrame *spriteAnim4 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame4.png");
                    CCSpriteFrame *spriteAnim5 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Light_Frame5.png");
                    CCSpriteFrame *spriteAnim6 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("Gem_Idle.png");
                    
                    CCArray *animFrames = CCArray::create();
                    animFrames->addObject(spriteAnim1);
                    animFrames->addObject(spriteAnim2);
                    animFrames->addObject(spriteAnim3);
                    animFrames->addObject(spriteAnim4);
                    animFrames->addObject(spriteAnim5);
                    animFrames->addObject(spriteAnim6);
                    
                    CCAnimation *animation = CCAnimation::createWithSpriteFrames(animFrames, 0.15f);
                    CCAnimate *animate = CCAnimate::create(animation);
                    CCFiniteTimeAction *delayAction = CCDelayTime::create(time);
                    time+=0.1;
                    CCFiniteTimeAction *anim = CCRepeat::create(animate, 10);
                    CCFiniteTimeAction *readySequence =  CCSequence::create(delayAction, anim, NULL);
                    sprite->runAction(readySequence);
                }
                x+=((sprite->getContentSize().width)*70.0f)/100.0f;
            }
            if (x>width)
                width = x;
            y-=((sprite->getContentSize().height)*70.0f)/100.0f;
        }
        myfile.close();
    }
#endif

    width -= startx;
    Initialized = true;
}

void Gems::Update(CCPoint sP, float radius, Player* ship, bool Update)
{
    if (!Initialized)
        return;
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    for (int i=0; i<stars.size(); i++)
    {
        CCSprite* sprite = stars.at(i);
        float radius = sprite->getContentSize().height/2;
        radius=(radius*70.0f)/100.0f;
        CCPoint Pos = node->convertToWorldSpace(sprite->getPosition());
        if (isIntersectingCircle (sP, Pos, radius, radius) && ship->isVisible() && sprite->isVisible())
        {
            if (Time == -1 || GetMilliCount()-Time>20)
            {
                if (!statusAudio)
                    CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("gem_pickup.mp3");
                Time = GetMilliCount();
            }
            sprite->setVisible(false);
            /*CCScaleTo *scale = CCScaleTo::create(0.05, 2.0);
            CCRotateTo *rotate = CCRotateTo::create(0.1, 180);
            CCFadeOut *fadeOut = CCFadeOut::create(0.3);*/
            //sprite->stopAllActions();
            //sprite->setScale(1.0);
            //sprite->runAction(scale);
            ship->AddGems(1);
        }
    }
}

void Gems::Draw()
{
    if (!Initialized)
        return;
    ccDrawColor4B(255, 255, 255, 255);
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();
    for (int i=0; i<stars.size(); i++)
    {
        CCSprite* sprite = stars.at(i);
        float radius = sprite->getContentSize().height/2;
        radius-=14;
        radius=(radius*70.0f)/100.0f;
        CCPoint Pos = node->convertToWorldSpace(sprite->getPosition());
        ccDrawCircle(ccp(Pos.x, Pos.y), radius, 360, 50, false);
    }
}

bool Gems::isIntersectingCircle(CCPoint p1, CCPoint p2, float r1, float r2)
{
    float distance = ((MAX(p2.x, p1.x) - MIN(p2.x, p1.x))*(MAX(p2.x, p1.x) - MIN(p2.x, p1.x))) + (((MAX(p2.y, p1.y) - MIN(p2.y, p1.y))) *((MAX(p2.y, p1.y) - MIN(p2.y, p1.y))));
    return distance<= (r1+r2)*(r1+r2);
}

int Gems::GetMilliCount()
{
    timeb tb;
    ftime( &tb );
    int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
    return nCount;
}
