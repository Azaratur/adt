//
//  BlockReader.cpp
//  AdventureTime
//
//  Created by Artificium on 17/09/13.
//
//

#include "BlockReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

using namespace std;

BlockReader::BlockReader() {
}

void BlockReader::Init(char level [])
{
   
    /*
     Legenda
     nome|x|y|level|rotation|isMoving|isBonus
     */
    //JA
    blockNumber = 0;
    
    string line;
    ifstream myfile;
    blockNumber = 0;
    int row = 0;
    arrayBlock.clear();
    ContainerBlock.clear();
    arrayCircle.clear();
    ContainerSprite.clear();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    //char path[300];
    unsigned long fileSize = 0;
    std::string writablePath = CCFileUtils::sharedFileUtils()->fullPathForFilename(level);
    
   // unsigned char *path = CCFileUtils::sharedFileUtils()->getFileData(writablePath.c_str(), "r", &fileSize);+
    unsigned long size = 0;
    unsigned char* pData = 0;
    pData = CCFileUtils::sharedFileUtils()->getFileData(writablePath.c_str(), "rb", &size);
    CCLog("BlockLevel: %s", pData);
    std::string str;
    str.append(reinterpret_cast<const char*>(pData));
    std::istringstream iss(str);
    
    int ProcessigType = -1;
    structBlock *block;
    structSpriteCollision *circle;
    while (std::getline(iss, line))
    {
        if (line.compare("<block>")==0)
        {
            if (ProcessigType == 1)
            {
                block->Number = blockNumber;
                block->obstacle = arrayBlock;
                block->Width = Width;
                ContainerBlock.push_back(block);
                blockNumber++;
            }
            else if (ProcessigType == 2)
            {
                circle->Name = SpriteName;
                circle->circle = arrayCircle;
                circle->Width = Width;
                ContainerSprite.push_back(circle);
            }
            arrayBlock.clear();
            ProcessigType = 1;
            block = new structBlock ();
            row = 0;
            continue;
        }
        
        if (line.compare("<sprite>")==0)
        {
            if (ProcessigType == 1)
            {
                block->Number = blockNumber;
                block->obstacle = arrayBlock;
                block->Width = Width;
                ContainerBlock.push_back(block);
                blockNumber++;
            }
            else if (ProcessigType == 2)
            {
                circle->Name = SpriteName;
                circle->circle = arrayCircle;
                circle->Width = Width;
                ContainerSprite.push_back(circle);
            }
            arrayCircle.clear();
            ProcessigType = 2;
            circle = new structSpriteCollision();
            row = 0;
            continue;
        }
        
        
        if (ProcessigType == 1)
        {
            vector<string> v = this->explode("|", line);
            if(row==0)
            {
                //CCLog("-> %i: %s", row, v[0].c_str());
                Width = atoi(v[1].c_str());
            }
            else
            {
                //CCLog("-> %i: %s", row, v[0].c_str());
                structObstacle bl;
                bl.name = v[0].c_str();
                bl.posX = atof(v[1].c_str());
                bl.posY = atof(v[2].c_str());
                bl.level = atoi(v[3].c_str());
                bl.rotation = atoi(v[4].c_str());
                bl.isMoving = atoi(v[5].c_str());
                bl.bonus = atoi(v[6].c_str());
                arrayBlock.push_back(bl);
            }
            CCLog("BlockLevel -> %i: %s", row, line.c_str());
            row++;
        }
        
        if (ProcessigType == 2)
        {
            vector<string> v = this->explode("|", line);
            if(row==0)
            {
                //CCLog("-> %i: %s", row, v[0].c_str());
                SpriteName = v[0].c_str();
                Width = atoi(v[1].c_str());
            }
            else
            {
                //CCLog("-> %i: %s", row, v[0].c_str());
                structCircle bl;
                bl.posX = atof(v[0].c_str());
                bl.posY = atof(v[1].c_str());
                bl.range = atoi(v[2].c_str())/2;
                arrayCircle.push_back(bl);
            }
            CCLog("BlockLevel -> %i: %s", row, line.c_str());
            row++;
        }
    }
    if (ProcessigType == 1)
    {
        block->Number = blockNumber;
        block->obstacle = arrayBlock;
        block->Width = Width;
        ContainerBlock.push_back(block);
        blockNumber++;
    }
    else if (ProcessigType == 2)
    {
        circle->Name = SpriteName;
        circle->circle = arrayCircle;
        circle->Width = Width;
        ContainerSprite.push_back(circle);
    }
    //myfile.open(pData);
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
    std::string lStr = CCFileUtils::sharedFileUtils()->fullPathForFilename(level);
    
    myfile.open(lStr.c_str());
    structBlock *block;
    structSpriteCollision *circle;
    //CCLog("BlockLevel: %s", path.c_str());
    int ProcessigType = -1;
    if (myfile.is_open())
    {
        while (! myfile.eof() )
        {
            getline (myfile,line);
            
            if (line.compare("<block>")==0)
            {
                if (ProcessigType == 1)
                {
                    block->Number = blockNumber;
                    block->obstacle = arrayBlock;
                    block->Width = Width;
                    ContainerBlock.push_back(block);
                    blockNumber++;
                }
                else if (ProcessigType == 2)
                {
                    circle->Name = SpriteName;
                    circle->circle = arrayCircle;
                    circle->Width = Width;
                    ContainerSprite.push_back(circle);
                }
                arrayBlock.clear();
                ProcessigType = 1;
                block = new structBlock ();
                row = 0;
                continue;
            }
            
            if (line.compare("<sprite>")==0)
            {
                if (ProcessigType == 1)
                {
                    block->Number = blockNumber;
                    block->obstacle = arrayBlock;
                    block->Width = Width;
                    ContainerBlock.push_back(block);
                    blockNumber++;
                }
                else if (ProcessigType == 2)
                {
                    circle->Name = SpriteName;
                    circle->circle = arrayCircle;
                    circle->Width = Width;
                    ContainerSprite.push_back(circle);
                }
                arrayCircle.clear();
                ProcessigType = 2;
                circle = new structSpriteCollision();
                row = 0;
                continue;
            }
            
            
            if (ProcessigType == 1)
            {
                vector<string> v = this->explode("|", line);
                if(row==0)
                {
                    //CCLog("-> %i: %s", row, v[0].c_str());
                    Width = atoi(v[1].c_str());
                }
                else
                {
                    //CCLog("-> %i: %s", row, v[0].c_str());
                    structObstacle bl;
                    bl.name = v[0].c_str();
                    bl.posX = atof(v[1].c_str());
                    bl.posY = atof(v[2].c_str());
                    bl.level = atoi(v[3].c_str());
                    bl.rotation = atoi(v[4].c_str());
                    bl.isMoving = atoi(v[5].c_str());
                    bl.bonus = atoi(v[6].c_str());
                    arrayBlock.push_back(bl);
                }
                CCLog("BlockLevel -> %i: %s", row, line.c_str());
                row++;
            }
            
            if (ProcessigType == 2)
            {
                vector<string> v = this->explode("|", line);
                if(row==0)
                {
                    //CCLog("-> %i: %s", row, v[0].c_str());
                    SpriteName = v[0].c_str();
                    Width = atoi(v[1].c_str());
                }
                else
                {
                    //CCLog("-> %i: %s", row, v[0].c_str());
                    structCircle bl;
                    bl.posX = atof(v[0].c_str());
                    bl.posY = atof(v[1].c_str());
                    bl.range = atoi(v[2].c_str())/2;
                    arrayCircle.push_back(bl);
                }
                CCLog("BlockLevel -> %i: %s", row, line.c_str());
                row++;
            }
        }
        if (ProcessigType == 1)
        {
            block->Number = blockNumber;
            block->obstacle = arrayBlock;
            block->Width = Width;
            ContainerBlock.push_back(block);
            blockNumber++;
        }
        else if (ProcessigType == 2)
        {
            circle->Name = SpriteName;
            circle->circle = arrayCircle;
            circle->Width = Width;
            ContainerSprite.push_back(circle);
        }
        myfile.close();
    }
    
    else CCLog("BlockLevel: Unable to open file");
#endif
}

vector<string> BlockReader::explode( const string &delimiter, const string &str)
{
    vector<string> arr;
    
    int strleng = str.length();
    int delleng = delimiter.length();
    if (delleng==0)
        return arr;//no change
    
    int i=0;
    int k=0;
    while( i<strleng )
    {
        int j=0;
        while (i+j<strleng && j<delleng && str[i+j]==delimiter[j])
            j++;
        if (j==delleng)//found delimiter
        {
            arr.push_back(  str.substr(k, i-k) );
            i+=delleng;
            k=i;
        }
        else
        {
            i++;
        }
    }
    arr.push_back(  str.substr(k, i-k) );
    return arr;
}
