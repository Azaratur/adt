//
//  Frame.cpp
//  ADT
//
//  Created by Johnny Picciafuochi on 26/11/2013.
//
//

#include "FrameADT.h"
using namespace cocos2d;

FrameADT::FrameADT (BlockReader::structSpriteCollision* s)
{
    Name = s->Name;
    OriginalWidth = s->Width;
    arrayCollision.clear();
    vector<BlockReader::structCircle> vec = s->circle;
    for (int i=0; i<vec.size(); i++)
    {
        BlockReader::structCircle c = vec.at(i);
        dimStruct ds;
        ds.x = c.posX;
        ds.y = c.posY;
        ds.radius = c.range;
        arrayCollision.push_back(ds);
    }
}
