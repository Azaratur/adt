#ifndef __Gumball2__ObstacleType__
#define __Gumball2__ObstacleType__


#include "cocos2d.h"
#include "CCParallaxNodeExtras.h"
#include <sys/timeb.h>
#include "Player.h"
#include "BlockReader.h"
#include "Sprite.h"

USING_NS_CC;
class ObstacleType
{
    public:
    ObstacleType();
    void Init(BlockReader::structObstacle, CCParallaxNodeExtras* n, float posx, vector<Sprite*> sp, int world);

    void InitMissile(char c [], char a [], char s [], CCSpriteBatchNode* n, bool updown, vector<Sprite*> sp);
    void InitHugeLaser(char c [], CCSpriteBatchNode* n, float y);
    void InitFlyingEnemy (char c[], CCSpriteBatchNode* n, vector<Sprite*> sp);
    void InitFlyingOnlyDownEnemy (char c[], CCSpriteBatchNode* n, vector<Sprite*> sp);
    void cleanAnimation(cocos2d::CCSprite *pSender);
    void Pause();
    void UnPause();
    int randomValueBetween(int low, int high);
    ~ObstacleType();
    void Destroy();
       
    static const int LASER              = 0;
    static const int MISSILE            = 1;
    static const int HUGELASER          = 2;
    static const int FLYINGENEMY        = 3;
    static const int MISSILEUPDOWN      = 4;
    static const int BONUS              = 5;
    static const int FLYINGENEMYONLYDOWN= 6;
    static const int FIREWOLF           = 7;
    static const int WOOD               = 8;
    static const int BUTTERFLY          = 9;
    
    static const int MISSILETIME        = 1500;
    static const int MISSILEWARNING     = 1000;
    static const int MISSILESPEED       = 800;
    static const int MISSILESPEEDUPDOWN = 500;
    static const int FLYINGENEMYSPEED   = 333;
    static const int LASERTIME          = 100;
    
    CCSprite* sprite;
    CCSprite* spriteRot;  
    CCSprite* spriteSmokeSmall;
    CCSprite* spriteSmokeMedium;
    CCSprite* spriteSmokeBig;
    
    CCSpriteFrame* sprAnim;
    bool Paused;
    bool Hidden;
    bool Splashed;
    bool HaveActions;
    bool AudioStarted;
    int Audionum;
    //int fade = 100;
    static const int fade = 100;
    int type;
    float posx;
    float posy;
    float ButterFlyX;
    float ButterFlyY;
    //float SRot = 0.0f;
    float SRot;
    float MissileY;
    bool Collided;
    bool LaserRed;
    bool LaserRot;
    bool MissileLocking;
    bool statusAudio;
    float height;
    float centerRadius;
    int startTime;
    int pauseTime;
    bool Initialized;
    float range;
    int ToogleMissile;
    
    CCParallaxNodeExtras* node;
    CCSpriteBatchNode* BatchNode;
    
    void setVisible (bool b);
    int GetMilliCount();
    int GetMilliSpan( int nTimeStart );
    bool MissileIsOut ();
    bool Update(CCPoint s, float radius, Player* ship, bool Update, float dt);
   // void setDimension (float x, float y, float h, float center);
    bool isIntersectingCircle(CCPoint p1, CCPoint p2, float r1, float r2);
    bool isIntersectingRect(CCPoint p1, float r1, CCPoint pTL, CCPoint pBR);
    vector<vector<FrameADT::dimStruct> > Collision;
    vector<CCSpriteFrame*>spriteAnim;
    
    BonusType* bt;
};
#endif /* defined(__Gumball2__CircleBody__) */


