#ifndef __STORE_SCENE_H__
#define __STORE_SCENE_H__

#include "cocos2d.h"
#include "CCLocalizedString.h"

using namespace cocos2d;
using namespace std;

//class StoreScene : public cocos2d::CCLayerColor
class StoreScene : public cocos2d::CCLayer
{
public:
    // Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
    virtual bool init();
    
    // there's no 'id' in cpp, so we recommend to return the class instance pointer
    static cocos2d::CCScene* scene();
    
    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(StoreScene);
    
    void onExit();
    
    void backToMenu(CCObject* pSender);
    CCMenu* menuBack;
    CCMenu* menuStore;
    CCLayer *layerItems;
    
    static void checkPointRelease(int x, int y);
    static int isTouchOnSprite(CCPoint touch);
    void requestConfirmBuy(int index);
    void buyElement(CCObject* pSender);
    void rejectElement(CCObject* pSender);
    CCMenu *menuConfirmBuy, *menuBuy;
    CCLabelTTF *labelConfirmBuy;
    
    int money;
    CCLabelBMFont *labelMoney;
    
    struct structStore
    {
        string name;
        CCSprite *bgSpr;
        CCSprite *spr;
        int money;
        CCSprite *sprMoney;
    };
    
    vector<structStore> arrayStore;
    
    CCSprite *bgLoading;
};

#endif