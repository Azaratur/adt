//
//  IntroScene.cpp
//  ADT
//
//  Created by Johnny Picciafuochi on 06/02/2014.
//
//

#include "IntroScene.h"
#include "MenuScene.h"
#include "MainScene.h"

CCScene* IntroScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    IntroScene *layer = IntroScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool IntroScene::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }

    CCSize size = CCDirector::sharedDirector()->getWinSize();
    CCSprite* bg = CCSprite::create();
    int w = size.width;
    int h = size.height;
    CCTexture2D *tex = new CCTexture2D();
    char _raw[4];
    _raw[0] = 255;
    _raw[1] = 255;
    _raw[2] = 255;
    _raw[3] = 255;

    tex->initWithData(_raw, kCCTexture2DPixelFormat_RGBA8888, 1, 1, CCSize(1, 1));
    bg->initWithTexture(tex, CCRectMake(0, 0, w, h));
    bg->setPosition(ccp(size.width/2, size.height/2));
    ccColor3B color;
    color.r = 255;
    color.g = 187;
    color.b = 28;
    bg->setColor(color);
    this->addChild(bg,0);
    
    
    
    this->setTouchEnabled(true);
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("intro_texture.plist", "intro_texture.png");
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("tutorial_texture.plist", "tutorial_texture.png");
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("main.plist", "main.png");
    Intro1 = CCSprite::createWithSpriteFrameName("Intro_Scene1.jpg");
    Intro2 = CCSprite::createWithSpriteFrameName("Intro_Scene2.jpg");
    Intro3 = CCSprite::createWithSpriteFrameName("Intro_Scene3.jpg");
    Tutorial1 = CCSprite::createWithSpriteFrameName("Tutorial1.jpg");
    Tutorial2 = CCSprite::createWithSpriteFrameName("Tutorial2.jpg");
    Tutorial3 = CCSprite::createWithSpriteFrameName("Tutorial3.jpg");
    
    Card1 = CCSprite::createWithSpriteFrameName("Card_Reborn.png");
    Card2 = CCSprite::createWithSpriteFrameName("Card_Shield.png");
    Card3 = CCSprite::createWithSpriteFrameName("Card_Stabilizer.png");
    Card4 = CCSprite::createWithSpriteFrameName("Card_Wings.png");
    Card5 = CCSprite::createWithSpriteFrameName("Card_Allbonus.png");
    Card6 = CCSprite::createWithSpriteFrameName("Card_RedGem.png");
    Card7 = CCSprite::createWithSpriteFrameName("Card_YellowGem.png");
    Card1->setPosition(ccp(size.width/2 - 50, size.height/2 +60));
    Card2->setPosition(ccp(size.width/2+60, size.height/2+30));
    Card3->setPosition(ccp(size.width/2-60, size.height/2-40));
    Card4->setPosition(ccp(size.width/2, size.height/2-20));
    Card5->setPosition(ccp(size.width/2+30, size.height/2-60));
    Card6->setPosition(ccp(size.width/2, size.height/2+55));
    Card7->setPosition(ccp(size.width/2+30, size.height/2));
    
    
    card1l = CCLabelBMFont::create(CCLocalizedString("t1027", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card1l->setPosition(ccp(size.width/2, card1l->getContentSize().height/2));
    card2l = CCLabelBMFont::create(CCLocalizedString("t1028", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card2l->setPosition(ccp(size.width/2, card2l->getContentSize().height/2));
    card3l = CCLabelBMFont::create(CCLocalizedString("t1029", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card3l->setPosition(ccp(size.width/2, card3l->getContentSize().height/2));
    
    card4l = CCLabelBMFont::create(CCLocalizedString("t1030", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card4l->setPosition(ccp(size.width/2, card4l->getContentSize().height/2));
    card5l = CCLabelBMFont::create(CCLocalizedString("t1031", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card5l->setPosition(ccp(size.width/2, card5l->getContentSize().height/2));
    card6l = CCLabelBMFont::create(CCLocalizedString("t1032", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card6l->setPosition(ccp(size.width/2, card6l->getContentSize().height/2));
    card7l = CCLabelBMFont::create(CCLocalizedString("t1033", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    card7l->setPosition(ccp(size.width/2, card7l->getContentSize().height/2));
    
    
    Intro1->setPosition(ccp(size.width/2, size.height/2));
    
    
    intro1l = CCLabelBMFont::create(CCLocalizedString("t1002", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    intro1l->setPosition(ccp(size.width/2, intro1l->getContentSize().height/2));
    intro2l = CCLabelBMFont::create(CCLocalizedString("t1003", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    intro2l->setPosition(ccp(size.width/2, intro2l->getContentSize().height/2));
    intro3l = CCLabelBMFont::create(CCLocalizedString("t1004", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    intro3l->setPosition(ccp(size.width/2, intro3l->getContentSize().height/2));
    
    tut1l = CCLabelBMFont::create(CCLocalizedString("t1005", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    tut1l->setPosition(ccp(size.width/2, tut1l->getContentSize().height/2));
    tut2l = CCLabelBMFont::create(CCLocalizedString("t1006", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    tut2l->setPosition(ccp(size.width/2, tut2l->getContentSize().height/2));
    tut3l = CCLabelBMFont::create(CCLocalizedString("t1007", "dito qui"), "stats_font.fnt", 1024, kCCTextAlignmentCenter);
    tut3l->setPosition(ccp(size.width/2, tut3l->getContentSize().height/2));
    
    Intro2->setPosition(ccp(size.width/2, size.height/2));
    Intro2->setVisible(false);
    intro2l->setVisible(false);
    Intro3->setPosition(ccp(size.width/2, size.height/2));
    Intro3->setVisible(false);
    intro3l->setVisible(false);
    
    tut1l->setVisible(false);
    tut2l->setVisible(false);
    tut3l->setVisible(false);
    
    Tutorial1->setPosition(ccp(size.width/2, size.height/2));
    Tutorial2->setPosition(ccp(size.width/2, size.height/2));
    Tutorial3->setPosition(ccp(size.width/2, size.height/2));
    this->addChild(Intro1, 4);
    this->addChild(intro1l, 4);
    this->addChild(Intro2, 5);
    this->addChild(intro2l, 5);
    this->addChild(Intro3, 6);
    this->addChild(intro3l, 6);
    this->addChild(Tutorial1, 3);
    this->addChild(tut1l, 3);
    this->addChild(Tutorial2, 2);
    this->addChild(tut2l, 3);
    this->addChild(Tutorial3, 1);
    this->addChild(tut3l, 3);
    
    this->addChild(Card1, 1);
    this->addChild(card1l, 1);
    Card1->setVisible(false);
    card1l->setVisible(false);
    this->addChild(Card2, 1);
    this->addChild(card2l, 1);
    Card2->setVisible(false);
    card2l->setVisible(false);
    this->addChild(Card3, 1);
    this->addChild(card3l, 1);
    Card3->setVisible(false);
    card3l->setVisible(false);
    this->addChild(Card4, 1);
    this->addChild(card4l, 1);
    Card4->setVisible(false);
    card4l->setVisible(false);
    this->addChild(Card5, 1);
    this->addChild(card5l, 1);
    Card5->setVisible(false);
    card5l->setVisible(false);
    this->addChild(Card6, 1);
    this->addChild(card6l, 1);
    Card6->setVisible(false);
    card6l->setVisible(false);
    this->addChild(Card7, 1);
    this->addChild(card7l, 1);
    Card7->setVisible(false);
    card7l->setVisible(false);
    return true;
}

void IntroScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    
}

void IntroScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    /*if (!Intro2->isVisible())
    {
        CCScene *pScene = MenuScene::scene();
        CCDirector::sharedDirector()->replaceScene(pScene);
    }
    if (!Intro1->isVisible())
        Intro2->setVisible(false);
    Intro1->setVisible(false);*/
    nTouch++;
    
    switch (nTouch)
    {
        case 1:
            Intro2->setVisible(true);
            intro2l->setVisible(true);
            break;
        case 2:
            Intro3->setVisible(true);
            intro3l->setVisible(true);
            break;
        case 3:
            this->removeChild(Intro1, true);
            this->removeChild(Intro2, true);
            this->removeChild(Intro3, true);
            this->removeChild(intro1l, true);
            this->removeChild(intro2l, true);
            this->removeChild(intro3l, true);
            tut1l->setVisible(true);
            break;
        case 4:
            this->removeChild(Tutorial1);
            this->removeChild(tut1l);
            tut2l->setVisible(true);
            break;
        case 5:
            this->removeChild(Tutorial2);
            this->removeChild(tut2l);
            tut3l->setVisible(true);
            break;
        case 6:
            this->removeChild(Tutorial3);
            this->removeChild(tut3l);
            Card1->setVisible(true);
            card1l->setVisible(true);
            break;
        case 7:
           // this->removeChild(Card1);
            this->removeChild(card1l);
            Card2->setVisible(true);
            card2l->setVisible(true);
            break;
        case 8:
           // this->removeChild(Card2);
            this->removeChild(card2l);
            Card3->setVisible(true);
            card3l->setVisible(true);
            break;
        case 9:
           // this->removeChild(Card3);
            this->removeChild(card3l);
            Card4->setVisible(true);
            card4l->setVisible(true);
            break;
        case 10:
          //  this->removeChild(Card4);
            this->removeChild(card4l);
            Card5->setVisible(true);
            card5l->setVisible(true);
            break;
        case 11:
           // this->removeChild(Card5);
            this->removeChild(card5l);
            Card6->setVisible(true);
            card6l->setVisible(true);
            break;
        case 12:
           // this->removeChild(Card6);
            this->removeChild(card6l);
            Card7->setVisible(true);
            card7l->setVisible(true);
            break;
        case 13:
            //this->removeChild(Tutorial3);
            CCLog("Eccolo");
            CCUserDefault *userDefault = CCUserDefault::sharedUserDefault();
            CCScene *pScene;
            pScene = MenuScene::scene();
            //userDefault->setBoolForKey("skip_intro", true);
            //userDefault->flush();
            CCDirector::sharedDirector()->replaceScene(pScene);
            break;
    }
}

void IntroScene::onExit()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("intro_texture.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("tutorial_texture.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile("main.plist");
    this->removeAllChildrenWithCleanup(true);
    this->setKeypadEnabled(false);
    this->setTouchEnabled(false);
}



