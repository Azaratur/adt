//
//  Sprite.h
//  ADT
//
//  Created by Johnny Picciafuochi on 26/11/2013.
//
//

#ifndef __ADT__Sprite__
#define __ADT__Sprite__

#include <iostream>
#include <sys/timeb.h>
#include "cocos2d.h"
#include "BlockReader.h"
#include "FrameADT.h"
using namespace std;
USING_NS_CC;
class Sprite
{
public:
    vector<FrameADT> Frame;
    string Name;
    Sprite(string c );
    void addFrame(FrameADT s);
    void Sort ();
    //bool compareSort( Frame::Frame f1, Frame::Frame f2 );
};

#endif /* defined(__ADT__Sprite__) */
