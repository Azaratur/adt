/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package com.lklab.adventuretime;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHelper;

import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.lklab.adventuretime.UtilsAndroid.FBListener;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
//import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

public class adta extends Cocos2dxActivity
{
	private static Cocos2dxActivity act;
    private static adta adt;
    private static String _TAG = "ADVENTURETIME";
        
    public native void testCallback(boolean param);
    public native void FbData(String id, String nome, String cognome, String email, String sesso, String loc, String token);

	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		try {
	        PackageInfo info = getPackageManager().getPackageInfo(
	                "com.lklab.adventuretime", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {
	    } catch (NoSuchAlgorithmException e) {
	    }
		UtilsAndroid.onCreateCallFB(this, savedInstanceState);
		//setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		adt = this;
		act = this;
		Log.d(_TAG, "JACOPO ON CREATE");
	}

	/** Called before the activity is destroyed. */
	  @Override
	  public void onDestroy()
	  {
		  adt.testCallback(true);
		  super.onDestroy();
	  }
	  
	  @Override
	  protected void onStart()
	  {
		  super.onStart();
		  UtilsAndroid.onStartCallFB ();
	  }

	  @Override
	  protected void onStop()
	  {
		  super.onStop();
		  UtilsAndroid.onStopCallFB ();
		  Log.d(_TAG, "ACTIVITY ON PAUSE");
	  }
	  
	  @Override
	  public void onActivityResult(final int requestCode,final int resultCode, Intent data) 
	  {
		  super.onActivityResult(requestCode, resultCode, data);
		  UtilsAndroid.onActivityResultCallFB(this, requestCode, resultCode, data);
	  }
	  
	  @Override
	  public void onBackPressed()
	  {
		  Log.d(_TAG, "Aza On Back");
		  System.exit(0);
		  finish();
		  super.onBackPressed();
	  }
	  
	  private void resumeGame() {
	    mIsRunning = true;
	    Cocos2dxHelper.onResume();
	   // this.mGLSurfaceView.onResume();
	    ActivityManager actvityManager = (ActivityManager) act.getSystemService( Activity.ACTIVITY_SERVICE );
	    ActivityManager.MemoryInfo mInfo = new ActivityManager.MemoryInfo ();
	    actvityManager.getMemoryInfo( mInfo );
	    Log.v("neom","mema " + mInfo.availMem/1024/1024);
	    Log.d(_TAG, "RESUME COCOS2D");
	}

	private void pauseGame() {
	    mIsRunning = false;
	    Cocos2dxHelper.onPause();
	   // this.mGLSurfaceView.onPause();
	    Log.d(_TAG, "PAUSE COCOS2D");
	}

	private boolean mIsRunning = false;
	private boolean mIsOnPause = false;

	@Override
	protected void onResume() {
	    super.onResume();
	    Log.d(_TAG, "ACTIVITY ON RESUME");
	    if (mIsOnPause) {
	        resumeGame();
	    }
	    mIsOnPause = false;
	}

	@Override
	protected void onPause() {
	    super.onPause();
	    Log.d(_TAG, "ACTIVITY ON PAUSE");
	    mIsOnPause = true;
	    if (mIsRunning) {
	        pauseGame();
	    }
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		UtilsAndroid.onSaveInstanceStateCallFB(outState);
	}

	/*@Override
	public void onWindowFocusChanged(final boolean hasWindowFocus) {
	    super.onWindowFocusChanged(hasWindowFocus);
	    Log.d(_TAG, "ACTIVITY ON WINDOW FOCUS CHANGED " + (hasWindowFocus ? "true" : "false"));
	    if (hasWindowFocus && !mIsOnPause) {
	        resumeGame();
	    }
	}*/
	
    static {
         System.loadLibrary("game");
    }
    
    static void LoginFb ()
    {
    	UtilsAndroid.onClickLoginFB();
    	UtilsAndroid.setListenerFB(new FBListener ()
    	{
    		@Override
    		public void getUser(GraphUser user, Response response, String token) {
    			adt.FbData(user.getId(), user.getFirstName(), user.getLastName(), (String) response.getGraphObject().getProperty("email"), (String) user.getProperty("gender"), (String) user.getProperty("locale"), token);
    		}
    	});
    }
    
    static boolean checkInternet ()
    {
    	if (adt == null)
			return false;
		ConnectivityManager cm = (ConnectivityManager) adt.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null) 
			return false;
		NetworkInfo nt = cm.getActiveNetworkInfo();
		if (nt == null)
			return false;
		return nt.isConnectedOrConnecting();
    }
    
    
    
    public static void test()
    {
    	adt.testCallback(true);
    }
    
}
