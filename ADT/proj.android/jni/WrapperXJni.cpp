#include "WrapperX.h"
#include "WrapperXJni.h"
#include "JacGameCenter.h"

#define  LOG_TAG    "libSimpleAudioEngine"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  CLASS_NAME "com/lklab/adventuretime/adta"

using namespace cocos2d;

extern "C"
{
	void fbLoginJNI()
	{
		JniMethodInfo methodInfo;

		if (! JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "LoginFb", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

	/*void inviteFbJNI()
	{
		JniMethodInfo methodInfo;

		if (! JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "inviteFb", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}*/

	void shareFbJNI()
	{
		JniMethodInfo methodInfo;

		if (! JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "shareFb", "()V"))
		{
			return;
		}

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    void testJNI()
	{
		JniMethodInfo methodInfo;
        
		if (! JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "test", "()V"))
		{
			return;
		}
        
		methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
        
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}

    bool checkInternetJNI()
	{
		JniMethodInfo methodInfo;

		if (! JniHelper::getStaticMethodInfo(methodInfo, CLASS_NAME, "checkInternet", "()Z"))
		{
			return false;
		}

		jboolean b = methodInfo.env->CallStaticBooleanMethod(methodInfo.classID, methodInfo.methodID);

		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		return b;
	}

	//This last function is called directly from the Activity inside the java file
	void  Java_com_lklab_adventuretime_adta_testCallback(JNIEnv *env, jobject obj, jboolean param)
	{
		if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate())
        {
            bool b = param;
			/*const char *id = env->GetStringUTFChars(idFb, 0);
			const char *nome = env->GetStringUTFChars(nameFb, 0);
			const char *cognome = env->GetStringUTFChars(surnameFb, 0);
			const char *mail = env->GetStringUTFChars(emailFb, 0);
			const char *sesso = env->GetStringUTFChars(genderFb, 0);
			const char *loc = env->GetStringUTFChars(localeFb, 0);*/

			delegate->testCallback(b);
        }
	}

	void  Java_com_lklab_adventuretime_adta_FbData(JNIEnv *env, jobject obj, jstring idFb, jstring nameFb, jstring surnameFb, jstring emailFb, jstring genderFb, jstring localeFb, jstring token)
		{
			if (WrapperXDelegate* delegate = WrapperX::sharedWrapperX()->getDelegate()) {

				const char *id = env->GetStringUTFChars(idFb, 0);
				const char *nome = env->GetStringUTFChars(nameFb, 0);
				const char *cognome = env->GetStringUTFChars(surnameFb, 0);
				const char *mail = env->GetStringUTFChars(emailFb, 0);
				const char *sesso = env->GetStringUTFChars(genderFb, 0);
				const char *loc = env->GetStringUTFChars(localeFb, 0);
				const char *tok = env->GetStringUTFChars(token, 0);
				JacGameCenter::handleFb(id, nome, cognome, mail, sesso, loc, tok);
			}
		}

}

