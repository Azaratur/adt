LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := hellocpp/main.cpp \
					WrapperX_android.cpp \
					WrapperXjni.cpp \
					../../Classes/Achievements.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/BlockReader.cpp \
                   ../../Classes/BlockType.cpp \
                   ../../Classes/CCLocalizedString.cpp \
                   ../../Classes/CCParallaxNodeExtras.cpp \
                   ../../Classes/CCSlidingLayer.cpp \
                   ../../Classes/CreditsScene.cpp \
                   ../../Classes/FrameADT.cpp \
                   ../../Classes/Gems.cpp \
                   ../../Classes/IntroScene.cpp \
                   ../../Classes/BonusType.cpp \
                   ../../Classes/MainScene.cpp \
                   ../../Classes/MenuScene.cpp \
                   ../../Classes/ObstacleType.cpp \
                   ../../Classes/OptionsScene.cpp \
                   ../../Classes/Player.cpp \
                   ../../Classes/Sprite.cpp \
                   ../../Classes/StoreScene.cpp \
                   ../../Classes/HelloWorldScene.cpp \
                   ../../Classes/LogoScene.cpp \
                   ../../Classes/JacGameCenter.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes                   
LOCAL_CPPFLAGS += -std=gnu++0x
LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)
