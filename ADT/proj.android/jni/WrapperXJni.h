#ifndef WrapperX_WrapperXJni_h
#define WrapperX_WrapperXJni_h

#include <jni.h>
#include "platform/android/jni/JniHelper.h"
#include <android/log.h>

extern "C"
{
    extern void fbLoginJNI();
    /*extern void inviteFbJNI();*/
    extern void shareFbJNI();
    extern bool checkInternetJNI();
    extern void testJNI();
}

#endif // __FF_UTILS_JNI__

