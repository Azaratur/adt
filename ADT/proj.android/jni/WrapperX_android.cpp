#include "WrapperX.h"
#include "WrapperXJni.h"

static WrapperX* s_pWrapperX = NULL;

/*
 * These methods are declared in WrapperrX.h, inside "Classes" dir
 * They call the functions declared in WrapperXJni.h, which will call, inside its cpp implementation, the Java code
 */

WrapperX* WrapperX::sharedWrapperX()
{
    if (s_pWrapperX == NULL)
    {
    	s_pWrapperX = new WrapperX();
    }
    return s_pWrapperX;
}

void WrapperX::purgeWrapperX()
{
    CC_SAFE_DELETE(s_pWrapperX);
}

bool WrapperX::checkInternet()
{
	return checkInternetJNI();
}

/*void WrapperX::inviteFb()
{
	inviteFbJNI();
}*/

void WrapperX::shareFb()
{
	shareFbJNI();
}

void WrapperX::fbLogin()
{
	fbLoginJNI();
}
void WrapperX::test()
{
	testJNI();
}
